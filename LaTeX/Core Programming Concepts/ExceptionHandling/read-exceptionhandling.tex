

\begin{figure}[h]
    \begin{center}
        \includegraphics[width=12cm]{images/exceptions.png}
    \end{center}
\end{figure}

    \section{Writing robust software}
    As a software developer, you will often be writing software that other
    developers will be using. This could be other developers at the company
    working on the same software product, or perhaps you might write a library
    that gets licensed out to other companies, or anything else.
    Your code will need to have checks in place for errors and be able to manage
    those errors gracefully, allowing the software to continue running
    instead of letting the program crash and restart.
    
    A long time ago, lots of developers used numeric error codes to
    track errors. If you've ever seen something like ``Error 12943''
    with no other useful information, that is an example of these error
    codes - useless for the end-user, but meant so that the programmer
    can search the code for that number and find where it broke. This is
    also why we use \textbf{return 0} at the end of our C++ programs -
    technically, you could return anything else, but returning 0 is meant
    to show that there were no errors. If you ran into an error, you \textit{could}
    return 1 or 2 or 3 instead to mark errors.
    
    Modern languages have \textbf{exception handling} built in, usually
    working with a \textbf{try/catch} style. You write in logic to check
    for errors, and when you find a problem you \textbf{throw} an exception,
    and that exception can be \textbf{caught} elseware in the code.
    
    \newpage
    \paragraph{What kind of errors can we listen for?}
    \begin{itemize}
		\item	Trying to open a file that doesn't exist
		\item	Memory access violations
		\item	Invalid math (dividing by 0)
		\item	Not enough memory
		\item	Receiving unexpected inputs
		\item	Trying to delete from an empty data structure
		\item	Problem converting one data type to another
    \end{itemize}
    
    \newpage
    \section{The C++ Exception object}
    C++ has an \textbf{exception} family of objects that we can use
    when trying to classify what kind of exception has happened.
    If none of the existing exception objects is appropriate, you can
    also create your own exception type.
    
    You can find documentation for the exception object here: ~\\
    \texttt{http://www.cplusplus.com/reference/exception/exception/}
    ~\\
    
    \paragraph{Exceptions:} \footnote{From http://www.cplusplus.com/reference/exception/exception/} ~\\
    \begin{tabular}{l l}
		bad\_alloc & Exception thrown on failure allocating memory \\
		bad\_cast & Exception thrown on failure to dynamic cast \\
		bad\_exception & Exception thrown by unexpected handler \\
		bad\_function\_call & Exception thrown on bad call \\
		bad\_typeid & Exception thrown on typeid of null pointer \\
		bad\_weak\_ptr & Bad weak pointer \\
		ios\_base::failure & Base class for stream exceptions \\
		logic\_error & Logic error exception \\
		runtime\_error & Runtime error exception \\ \hline
		
		domain\_error & Domain error exception \\
		future\_error & Future error exception \\
		invalid\_argument & Invalid argument exception \\
		length\_error & Length error exception \\
		out\_of\_range & Out-of-range exception \\ \hline
		
		overflow\_error & Overflow error exception \\
		range\_error & Range error exception \\
		system\_error & System error exception \\
		underflow\_error & Underflow error exception \\ \hline
		
		bad\_array\_new\_length & Exception on bad array length
    \end{tabular}
    
    \newpage
    \section{Detecting errors and throwing exceptions}
    The first step of dealing with exceptions is identifying a place
    where an error may occur - such as a place where we might end
    up dividing by zero. We write an \textbf{if statement} to check
    for the error case, and then \textbf{throw} an exception.
    We choose an exception type and we can also pass an error message
    as a string.

\begin{lstlisting}[style=code]
int ShareCookies( int cookies, int kids )
{
	if ( kids == 0 )
	{
		throw runtime_error( "Cannot divide by zero!" );
	}
	
	return cookies / kids;
}
\end{lstlisting}

	\section{Listening for exceptions with try}
	Now we know that the function \texttt{ShareCookies} could possibly
	throw an exception. Any time we call that function, we need to
	listen for any thrown exceptions by using \textbf{try}.

\begin{lstlisting}[style=code]
try
{
	// Calling the function
	cookiesPerKid = ShareCookies( c, k );
}
\end{lstlisting}
	
	\newpage
	\section{Dealing with exceptions with catch}
	Immediately following the \textbf{try}, we can write one or more
	\textbf{catch} blocks for different types of exceptions and then
	decide how we want to handle it.

\begin{lstlisting}[style=code]
try
{
	// Calling the function
	cookiesPerKid = ShareCookies( c, k );
}
catch( runtime_error ex )
{
	// Display the error message
	cout << "Error: " << ex.what() << endl;
	
	// Handling it by setting a default value
	cookiesPerKid = 0;
}

cout << "The kids get " << cookiesPerKid 
	<< " each" << endl;
\end{lstlisting}

	A function could possibly throw different types of exceptions
	for different errors, and you can have multiple \textbf{catch}
	blocks for each type.
	
	\paragraph{Handling the error:} 
	Once you catch an error, it's up to you to decide what to do with it.
	For example, you could...
	
	\begin{itemize}
		\item	Ignore the error and just keep going
		\item	Write some different logic for a ``plan B'' backup
		\item	End the program because now the data is invalid
	\end{itemize}
	
	\paragraph{Coding with others' code:}
	While writing software and utilizing others' code, you will want
	to pay attention to which functions you're calling that could throw
	exceptions. Often code libraries will contain documentation that
	specify possible exceptions thrown.
