\section{Searching}

  We're going to keep this section short for now because
  we're mostly going to be focusing on sorting algorithms.
  
  \paragraph{When we're searching for items in an \underline{unsorted} linear structure}
  there's not much we can do to speed up the process. We can basically
  either start at the beginning and move forward, or start and the end and
  move backward, checking each item in the structure for what you're looking for.

\begin{lstlisting}[style=code]
template <typename T>
int LinearSearch( const vector<T>& arr, T findme )
{
    int size = arr.size();
    for ( int i = 0; i < size; i++ )
    {
        if ( arr[i] == findme )
        {
            return i;
        }
    }

    return -1; // not found
}
\end{lstlisting}

  We begin at the first index 0 and iterate until we hit the last
  index. Within the loop, if the element at index \texttt{i}
  matches what we're looking for, we return this index.
  
  If the loop completes and we haven't returned an index yet
  that means we've searched the entire structure and have not
  found the item. In this case, it is not in the structure
  and we can throw an exception to be dealt with elseware
  or return something like -1 to symbolize "no valid index".
  ~\\
  
  This search algorithm's growth rate is $O(n)$ -- the more items in
  the structure, the time linearly increases to search through it.
  Not much we can do about that, which is why we have different types
  of data structures that sort data as it is inserted - more on those
  later on.
  
  \paragraph{OK, but what if the structure \textit{is} sorted?}
  
  We're going to be learning about sorting algorithms, so what if we happen
  to have a structure that \textit{is} sorted? How can we more intelligently
  look for some \textit{value} in the structure?
  ~\\
  
  Let's say we have a simple array like this:
  
  \begin{center}
    \begin{tabular}{ | c | c | c | c | c | c | } \hline
      0 & 1 & 2 & 3 & 4 & 5  \\ \hline
      \texttt{"aardvark"} & \texttt{"bat"} & \texttt{"cat"}
      & \texttt{"dog"} & \texttt{"elephant"} & \texttt{"fox"}
      \\ \hline
    \end{tabular}
  \end{center}
  
  And we want to see if \texttt{"dog"} is in the array. We could investigate
  what the first item is (Hm, starts with an \texttt{"a"}) and the last item
  (\texttt{"f"}), and realize that \texttt{"d"} is about halfish way between
  both values. Maybe we should start in the middle and move left or right?
  
  \begin{itemize}
    \item Index 0 is \texttt{"aardvark"}. Index 5 is \texttt{"fox"}. ~\\
          Middle value $\frac{0+5}{2}$ is 2.5 (or 2, for integer division). ~\\
          What is at position 2? -- \texttt{"cat"}. ~\\
          If arr[2] $<$ findme, move left (investigate arr[1] next) ~\\
          Or if arr[2] $>$ findme, move right (investigate arr[3] next).
    \item \texttt{"d"} is greater than \texttt{"c"} so we'll move right...~\\
          Index 3 gives us \texttt{"dog"} - we've found the item! Return 3.
  \end{itemize}
  
  In this case, we basically have two iterations of a loop to find "dog" and
  return its index. If we were searching linearly, we would have to go
  from 0 to 1 to 2 to 3, so four iterations.
  ~\\
  
  This still isn't the most efficient way to search this array - 
  just starting at the midpoint and moving left or moving right each time.
  However, we can build a better search that imitates that first step:
  Checking the mid-way point each time.
  
  ~\\
  
  Here is the \textbf{Binary Search}.
  
\begin{lstlisting}[style=code]
template <typename T>
int BinarySearch( vector<T> arr, T findme )
{
    int size = arr.size();
    int left = 0;
    int right = size - 1;

    while ( left <= right )
    {
        int mid = ( left + right ) / 2;

        if ( arr[mid] < findme )
        {
            left = mid + 1;
        }
        else if ( arr[mid] > findme )
        {
            right = mid - 1;
        }
        else if ( arr[mid] == findme )
        {
            return mid;
        }
    }

    return -1; // not found
}
\end{lstlisting}
  
  With the binary search we look at the left-most index,
  right-most index, and mid-point. Each iteration of the loop,
  we look at our search value \texttt{findme} -- is its value
  greater than the middle or less than the middle?

  \newpage
  \paragraph{Example}
  Let's say we have this array, and we are searching for \texttt{'p'}.
  
  \begin{center}
    \begin{tabular}{ | c | c | c | c | c | c | c | c | c | c |} \hline
      0 & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 \\ \hline
      \texttt{'a'} & 
      \texttt{'c'} &
      \texttt{'e'} &
      \texttt{'h'} &
      \texttt{'i'} &
      \texttt{'k'} &
      \texttt{'m'} &
      \texttt{'o'} &
      \texttt{'p'} &
      \texttt{'r'}
      \\ \hline
    \end{tabular}
  \end{center}
  
  \paragraph{Step 1:} \texttt{left} is at 0, \texttt{right} is at 9, \texttt{mid} is $\frac{0+9}{2}$ = 4 (integer division).
  \begin{center}
    \begin{tabular}{ | c | c | c | c | c | c | c | c | c | c |} \hline
      0 & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 \\ \hline
      \cellcolor{colorblind_light_orange} \texttt{'a'} & 
      \texttt{'c'} &
      \texttt{'e'} &
      \texttt{'h'} &
      \cellcolor{colorblind_light_orange} \texttt{'i'} &
      \texttt{'k'} &
      \texttt{'m'} &
      \texttt{'o'} &
      \texttt{'p'} &
      \cellcolor{colorblind_light_orange} \texttt{'r'}
      \\ \hline
    \end{tabular}
  \end{center}

  Next we compare \texttt{i} to \texttt{'p'}. \texttt{'p'} comes
  later in the alphabet (so \texttt{p > i}), so next we're going
  to change the \texttt{left} value to look at \texttt{mid+1}
  and keep \texttt{right} as it is.
  \vspace{1cm}

  \paragraph{Step 2:} \texttt{left} is at 5, \texttt{right} is at 9, \texttt{mid} is $\frac{5+9}{2} = \frac{14}{2}$ = 7.
  \begin{center}
    \begin{tabular}{ | c | c | c | c | c | c | c | c | c | c |} \hline
      0 & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 \\ \hline
      \cellcolor{colorblind_medium_gray} \texttt{'a'} & 
      \cellcolor{colorblind_medium_gray} \texttt{'c'} &
      \cellcolor{colorblind_medium_gray} \texttt{'e'} &
      \cellcolor{colorblind_medium_gray} \texttt{'h'} &
      \cellcolor{colorblind_medium_gray} \texttt{'i'} &
      \cellcolor{colorblind_light_orange} \texttt{'k'} &
      \texttt{'m'} &
      \cellcolor{colorblind_light_orange} \texttt{'o'} &
      \texttt{'p'} &
      \cellcolor{colorblind_light_orange} \texttt{'r'}
      \\ \hline
    \end{tabular}
  \end{center}
  
  Now we compare the item at \texttt{arr[mid]} \texttt{'o'} 
  to what we're searching for (\texttt{'p'}).
  \texttt{p > o} so we adjust
  our left point again to our current midpoint.
  \vspace{1cm}

  \paragraph{Step 3:} \texttt{left} is at 7, \texttt{right} is at 9, \texttt{mid} is $\frac{7+9}{2} = \frac{16}{2}$ = 8.
  \begin{center}
    \begin{tabular}{ | c | c | c | c | c | c | c | c | c | c |} \hline
      0 & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 \\ \hline
      \cellcolor{colorblind_medium_gray} \texttt{'a'} & 
      \cellcolor{colorblind_medium_gray} \texttt{'c'} &
      \cellcolor{colorblind_medium_gray} \texttt{'e'} &
      \cellcolor{colorblind_medium_gray} \texttt{'h'} &
      \cellcolor{colorblind_medium_gray} \texttt{'i'} &
      \cellcolor{colorblind_medium_gray} \texttt{'k'} &
      \cellcolor{colorblind_medium_gray} \texttt{'m'} &
      \cellcolor{colorblind_light_orange} \texttt{'o'} &
      \cellcolor{colorblind_light_orange} \texttt{'p'} &
      \cellcolor{colorblind_light_orange} \texttt{'r'}
      \\ \hline
    \end{tabular}
  \end{center}
  
  Now we compare the item at \texttt{arr[mid]} (\texttt{'p'}) 
  to what we're searching for (\texttt{'p'}).
  The values match! So the result is \texttt{mid} as the index where we found our item.
  ~\\
  
  Each step through the process we \textbf{cut out half the search area} by investigating
  mid and deciding to ignore everything either \textit{before it} (like our example)
  or \textit{after it}. We do this every iteration, cutting out half the search region
  each time, effectively giving us an efficiency of $O(log(n))$ - the inverse
  of an exponential increase.








