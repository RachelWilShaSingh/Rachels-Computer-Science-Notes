\documentclass[a4paper,12pt,oneside]{book}
\usepackage[utf8]{inputenc}

\newcommand{\laTopic}       {Searching and Sorting}
\newcommand{\laTitle}       {Rachel's Computer Science Notes}
\newcounter{question}

\renewcommand{\chaptername}{Topic}

\usepackage{../../rachwidgets}
\usepackage{../../rachdiagrams}

\title{Introduction to C++}
\author{Rachel Singh}
\date{\today}

\pagestyle{fancy}
\fancyhf{}

\lhead{\laTopic \ / \laTitle}

\chead{}

\rhead{\thepage}

\rfoot{\tiny \thepage\ of \pageref{LastPage}}

\lfoot{\tiny Rachel Singh, last updated \today}

\renewcommand{\headrulewidth}{2pt}
\renewcommand{\footrulewidth}{1pt}

\begin{document}

    \begin{titlepage}
        \centering{}

        \sffamily{
            \textbf{
                {\fontsize{2cm}{3cm}\selectfont Core Computer Science Notes:}
                {\fontsize{2cm}{3cm}\selectfont \laTopic}
            }
        }

        \begin{figure}[h]
            \begin{center}
                \includegraphics[width=12cm]{../images/searching.png}
            \end{center}
        \end{figure}

        \sffamily{
            \textbf{
                An overview compiled by Rachel Singh
            }
        }
        
        \vspace{1cm} \small
        This work is licensed under a \\ Creative Commons Attribution 4.0 International License. ~\\~\\
        \includegraphics{../images/cc-by-88x31.png}
        ~\\~\\
        
        Last updated \today

    \end{titlepage}
    
	\tableofcontents
	
	\chapter{Intro to algorithm complexity} %---------------------------%
	
	\section{Thinking about algorithm efficiency}
	
	Although our computers are much more powerful than they were at
	the beginning, we still need to pay attention to the complexity
	and efficiency of our programs to some extent. Every operation takes
	a little bit of processing time, and as you loop in a program, that
	processing time is multiplied by the amount of times you go through a loop.
	Our programs don't work with a large set of data, but as programs scale
	and we work with thousands or millions of records, the efficiency of
	our algorithms matter. ~\\
	
	Let's say we have a sorting algorithm that, for every $n$ records,
	it takes $n^2$ program units to find an object.
	If we had 100 records, then it would take $100^2 = 10,000$ units to
	sort the set of data.
	
	Now, what the units are could vary - an older
	machine might take longer to execute one instruction, and a newer
	computer might process an instruction much more quickly, but we think of
	algorithm complexity in this sort of generic form.
	
	\newpage
	\section{Counting operations}
	
	Let's look at operations count for different functions.
	
	\paragraph{Single operations:}
	A math operation, an assignment statement, variable declarations,
	and other single-line instructions are small and quickly executed.
	These are considered \textbf{constant time}, they always take the
	same amount of time to execute.
	~\\
	
	We use the notation $O(1)$ (pronounced ``Big-O of 1'') to specify
	that this is a constant time operation, the smallest unit of complexity.
	
	\paragraph{Looping:}
	Once we add looping into the mix, those single operations within it
	get executed repeatedly, which is where algorithm complexity comes in. ~\\
	
	\subparagraph{Single loops:} 
	This code has a loop that executes a command 10 times,
	so we would write this as $O(10)$ (for now). 

\begin{lstlisting}[style=code]
for ( int i = 0; i < 10; i++ )
{
	cout << i << endl;
}
\end{lstlisting}

	\subparagraph{Neighbor loops:}
	If we have two loops back-to-back, one loop completes executing
	and then the next one executes. We add together the iterations from each.

\begin{lstlisting}[style=code]
for ( int i = 0; i < 10; i++ )
{
	cout << i << endl;
}

for ( int i = 0; i < 20; i++ )
{
	cout << i << endl;
}
\end{lstlisting}

	The first loop runs 10 times, and the second loop runs 20 times,
	so that's a total of 30 executions, or $O(30)$.
	
	\subparagraph{Nested loops:}
	When we encounter nested loops, the amount of times the internal
	operation is executed increases - the total number of loops is the
	amount of time the external loop runs \textit{times} the amount of
	times the internal loop runs.
	
\begin{lstlisting}[style=code]
for ( int i = 0; i < 5; i++ )
{
	for ( int j = 0; j < 4; j++ )
	{
		cout << i << "," << j << "\t";
	}
	cout << endl;
}
\end{lstlisting}

	The external loop runs 5 times and the internal loop runs 4 times,
	which ends up being 20 total operations. (In general, we don't care
	about counting the separate \texttt{cout} statements, we are more
	concerned with counting the iterations of the loops.)
	
	If we wrote out the result of this loop, it would look like this:
	
	\begin{center}
		\begin{tabular}{| c | c | c | c |} \hline
			0,0 & 0,1 & 0,2 & 0,3 \\ \hline
			1,0 & 1,1 & 1,2 & 1,3 \\ \hline
			2,0 & 2,1 & 2,2 & 2,3 \\ \hline
			3,0 & 3,1 & 3,2 & 3,3 \\ \hline
			4,0 & 4,1 & 4,2 & 4,3 \\ \hline
		\end{tabular}
	\end{center}
	
	\newpage
	\section{Thinking about operations generically}
	We generally don't know the exact amount of times any given loop
	is going to run, since we're often loading up data from some source
	and processing that data. We don't \textit{need to know} the exact
	amount of times something runs. But, we are still concerned with
	the algorithm's complexity, so we generalize.
	
	\paragraph{$O(1)$:} ``Big-O of 1'' time complexity is going to be for
	any single operation or set of single operations together - assignment
	statements, math operations, output statements, if statements, etc.
	
	
	\paragraph{$O(n)$:} ``Big-O of $n$'' time complexity would be for
	a single loop that runs $n$ times. Again, we don't know exactly how
	many times it runs, so we generalize it to $n$. We generally think 
	in terms of processing $n$ pieces of data (such as in an array), and
	for an array of size $n$, it takes $n$ iterations of a loop to get through it
	- such as if you wanted to display all the elements of the array.
	
\begin{lstlisting}[style=code]
void DisplayList( string arr[], int n )
{
	for ( int i = 0; i < n; i++ )
	{
		cout << i << " = " << arr[i] << endl;
	}
}
\end{lstlisting}

	\begin{center}
		\begin{tikzpicture}
		  \begin{axis}[grid=major,xmin=0,ymin=0,xmax=20,ymax=20,xlabel={Amount of data $n$},ylabel={Time complexity $O(n)$}]
			\addplot[red] coordinates
			  {(0,0) (20,20)};
		  \end{axis}
		\end{tikzpicture} ~\\ ~\\
		Linear complexity: ~\\ 
		As we have more data, the amount of operations increases linearly.
	\end{center}
	
	\paragraph{$O(n^2)$:} ``Big-O of $n^2$'' time complexity is when you
	have nested loops. If we generalize our thinking of the loops to say
	the outer loop runs $n$ times, we also say the inner loop runs $n$ times itself,
	so with one inside the other, it runs a total of $n \times n$ times - or $n^2$.

\begin{lstlisting}[style=code]
void NestedLoop( string arr[], int n )
{
	for ( int i = 0; i < n; i++ )
	{
		for ( int j = 0; j < n; j++ )
		{
			cout << i << ", " << j << endl;
		}
	}
}
\end{lstlisting}


	\begin{center}
		\begin{tikzpicture}
		  \begin{axis}[grid=major,xmin=0,xmax=5,ymax=25,xlabel={Amount of data $n$},ylabel={Time complexity $O(n^2)$}]
			\addplot[red] {x^2};
		  \end{axis}
		\end{tikzpicture} ~\\ ~\\
		Quadratic complexity: ~\\
		As the amount of data $n$ rises, the time complexity increases quadratically.
	\end{center}

	\newpage
	\paragraph{$O(log(n))$:} ``Logarithmic'' time complexity is more efficient
	than even a $O(n)$ time complexity because for logarithmic algorithms,
	they tend to cut out a chunk of the work to do each iteration.

	\begin{center}
		\begin{tikzpicture}
		  \begin{axis}[grid=major,xmin=0,xmax=5,ymax=5,xlabel={Amount of data $n$},ylabel={Time complexity $O(log(n))$}]
			\addplot[red] {log10(x)};
		  \end{axis}
		\end{tikzpicture} ~\\ ~\\
		Logarithmic complexity: ~\\
		Notice how as the amount of data $n$ increases, the execution
		time rises only slowly.
	\end{center}
	
	\subsection{Best, Average, and Worst case times}
	When talking about Big-O notation, we are mostly concerned with
	the complexity of the algorithm in the \textbf{average-case}.
	We can also talk about the \textbf{best-case} complexity - such as
	if we're trying to sort data that's already sorted, how long does it
	take for the algorithm to go through the entire thing to check? -
	and we might care about \textbf{worst-case} complexity - what if
	the data is perhaps sorted descending, and the algorithm is going to
	sort it ascending, so it has to do the maximum amount of work?
	
	\chapter{Intro to Sorting} %-------------------------------------------------% 
	
	Having data in some sort of order is important in most applications,
	and there are lots of different sorting algorithms to choose from
	that approach sorting differently.
	
	\section{Insertion sort}
	
	\begin{center}
		\begin{tabular}{c c c}
			\textbf{Best} & \textbf{Average} & \textbf{Worst}  \\
			$O(n)$ & $O(n^2)$ & $O(n^2)$ 
		\end{tabular}
	\end{center}
	
	Insertion sort is a pretty simple algorithm and it works alright
	with small data sets but it \textit{does not} scale well to larger
	data sets.
	
	~\\ Implementation based on https://en.wikipedia.org/wiki/Insertion\_sort
\begin{lstlisting}[style=code]
void InsertionSort( int arr[], int size )
{
    int i = 1;
    while ( i < size )
    {
        int j = i;
        while ( j > 0 && arr[j-1] > arr[j] )
        {
            Swap( arr[j], arr[j-1] );
            j = j-1;
        }

        i = i + 1;
    }
}
\end{lstlisting}

	\newpage
	Let's step through the algorithm to see how it works.

	\begin{center}
		\begin{tabular}{c | c | p{8cm} p{4cm}}
			& & Let's start with this unsorted array.
			&	
			\begin{tabular}{| c | c | c | c |} \hline
				5 & 2 & 3 & 1
				\\ \hline
			\end{tabular}
			\\ \texttt{i} & \texttt{j} & \\
			1 & 1 & First, we set \texttt{i=1} (line 3) and \texttt{j=1} (line 6).
			&
			\begin{tabular}{| c | c | c | c |} \hline
				5 & \cellcolor{colorblind_light_blue}2 & 3 & 1
				\\ \hline
			\end{tabular}
			\\ & & \\
			& & Compare \texttt{arr[j-1]} and \texttt{arr[j]}...
			&
			\begin{tabular}{| c | c | c | c |} \hline
				\cellcolor{colorblind_light_orange}5 & \cellcolor{colorblind_light_blue}2 & 3 & 1
				\\ \hline
			\end{tabular}
			\\ & & \\
			& & Swap...
			&
			\begin{tabular}{| c | c | c | c |} \hline
				\cellcolor{colorblind_light_orange}2 & \cellcolor{colorblind_light_blue}5 & 3 & 1
				\\ \hline
			\end{tabular}
			\\ & & \\
			2 & 2 & Increment \texttt{i}; \texttt{i=2} and \texttt{j = 2} now.
			&
			\begin{tabular}{| c | c | c | c |} \hline
				2 & 5 & \cellcolor{colorblind_light_blue}3 & 1
				\\ \hline
			\end{tabular}
			\\ & & \\
			& & Compare \texttt{arr[j-1]} and \texttt{arr[j]}...
			&
			\begin{tabular}{| c | c | c | c |} \hline
				2 & \cellcolor{colorblind_light_orange}5 & \cellcolor{colorblind_light_blue}3 & 1
				\\ \hline
			\end{tabular}
			\\ & & \\
			& & Swap...			
			&
			\begin{tabular}{| c | c | c | c |} \hline
				2 & \cellcolor{colorblind_light_orange}3 & \cellcolor{colorblind_light_blue}5 & 1
				\\ \hline
			\end{tabular}
			\\ & & \\
			2 & 1 & Decrement \texttt{j}, so \texttt{j = 1}.
			
			Compare \texttt{arr[j-1]} and \texttt{arr[j]}... it's fine.
			&
			\begin{tabular}{| c | c | c | c |} \hline
				\cellcolor{colorblind_light_orange}2 & \cellcolor{colorblind_light_blue}3 & 5 & 1
				\\ \hline
			\end{tabular}
			\\ & & \\
			3 & 3 & Increment \texttt{i}; \texttt{i = 3}
			and \texttt{j = 3}.
			&
			\begin{tabular}{| c | c | c | c |} \hline
				2 & 3 & 5 & \cellcolor{colorblind_light_blue}1
				\\ \hline
			\end{tabular}
			\\ & & \\
			& & Compare \texttt{arr[j-1]} and \texttt{arr[j]}...
			&
			\begin{tabular}{| c | c | c | c |} \hline
				2 & 3 & \cellcolor{colorblind_light_orange}5 & \cellcolor{colorblind_light_blue}1
				\\ \hline
			\end{tabular}
			\\ & & \\
			& & Swap... &
			\begin{tabular}{| c | c | c | c |} \hline
				2 & 3 & \cellcolor{colorblind_light_orange}1 & \cellcolor{colorblind_light_blue}5
				\\ \hline
			\end{tabular}
			\\ & & \\
			3 & 2 & Decrement \texttt{j}... (\texttt{j=2}) &
			\begin{tabular}{| c | c | c | c |} \hline
				2 & \cellcolor{colorblind_light_orange}3 & \cellcolor{colorblind_light_blue}1 & 5
				\\ \hline
			\end{tabular}
			\\ & & \\
			& & Swap... &
			\begin{tabular}{| c | c | c | c |} \hline
				2 & \cellcolor{colorblind_light_orange}1 & \cellcolor{colorblind_light_blue}3 & 5
				\\ \hline
			\end{tabular}
			\\ & & \\
			3 & 1 & Decrement \texttt{j}... (\texttt{j=1}) & 
			\begin{tabular}{| c | c | c | c |} \hline
				\cellcolor{colorblind_light_orange}2 & \cellcolor{colorblind_light_blue}1 & 3 & 5
				\\ \hline
			\end{tabular}
			\\ & & \\
			& & Swap... &
			\begin{tabular}{| c | c | c | c |} \hline
				\cellcolor{colorblind_light_orange}1 & \cellcolor{colorblind_light_blue}2 & 3 & 5
				\\ \hline
			\end{tabular}
			\\ & & \\
			3 & 0 & Decrement \texttt{j}... (\texttt{j=0})
			
			End of inner loop.
			\\ & & \\
			4 & & Increment \texttt{i}... (\texttt{i=4})
			
			End of outer loop.
		\end{tabular}
	\end{center}
	
	
	
	
	
	
	\newpage
	\section{Selection sort}
	
	\begin{center}
		\begin{tabular}{c c c}
			\textbf{Best} & \textbf{Average} & \textbf{Worst}  \\
			$O(n^2)$ & $O(n^2)$ & $O(n^2)$ 
		\end{tabular}
	\end{center}
	
	Also an inefficient sorting algorithm but is also relatively simple.
	
	~\\ Implementation based on https://en.wikipedia.org/wiki/Selection\_sort
\begin{lstlisting}[style=code]
void SelectionSort( int arr[], int size )
{
    for ( int i = 0; i < size-1; i++ )
    {
        int minIndex = i;

        for ( int j = i+1; j < size; j++ )
        {
            if ( arr[j] < arr[minIndex] )
            {
                minIndex = j;
            }
        }

        if ( minIndex != i )
        {
            Swap( arr[i], arr[minIndex] );
        }
    }
}
\end{lstlisting}

	\newpage
	\begin{center}
		\begin{tabular}{c | c | c | p{8cm} p{4cm}}
			& & & Let's start with this unsorted array.
			&
			\begin{tabular}{| c | c | c | c |} \hline
				9 & 5 & 4 & 7
				\\ \hline
			\end{tabular}
			
			\\
			\texttt{i} & \texttt{j} & \texttt{min} \\
			0 & 1 & 0 
			& \texttt{i=0}, \texttt{minIndex = i} (0),
			and \texttt{j = i+1} (1).
			&	
			\begin{tabular}{| c | c | c | c |} \hline
				\cellcolor{colorblind_light_blue}9 & \cellcolor{colorblind_light_orange}5 & 4 & 7
				\\ \hline
			\end{tabular}
			\\ & & & \texttt{arr[j] < arr[minIndex]}? (true)
			\\
			0 & 1 & 1 &
			Set \texttt{minIndex = j} (1).
			\\ & & &
			\\
			0 & 2 & 1 & \texttt{j++} (2).
			&	
			\begin{tabular}{| c | c | c | c |} \hline
				\cellcolor{colorblind_light_blue}9 & \cellcolor{colorblind_light_gray}5 & \cellcolor{colorblind_light_orange}4 & 7
				\\ \hline
			\end{tabular}
			\\ & & & \texttt{arr[j] < arr[minIndex]}? (true)
			\\ 0 & 2 & 2 &
			Set \texttt{minIndex = j} (2).
			\\ & & &
			\\ 
			0 & 3 & 2 & \texttt{j++} (3).
			&	
			\begin{tabular}{| c | c | c | c |} \hline
				\cellcolor{colorblind_light_blue}9 & 5 &  \cellcolor{colorblind_light_gray}4 & \cellcolor{colorblind_light_orange}7
				\\ \hline
			\end{tabular}
			\\ & & & \texttt{arr[j] < arr[minIndex]}? (false)
			
			\\ & & &
			\texttt{j++} (4)
			
			End of inner for loop
			&	
			\begin{tabular}{| c | c | c | c |} \hline
				\cellcolor{colorblind_light_blue}9 & 5 &  \cellcolor{colorblind_light_gray}4 & 7
				\\ \hline
			\end{tabular}
			
			\\ & & & \\ & & & 
			\texttt{minIndex != i}? (true)
			\\ & & & Swap \texttt{arr[i]} and \texttt{arr[minIndex]}.
			&	
			\begin{tabular}{| c | c | c | c |} \hline
				\cellcolor{colorblind_light_blue}4 & 5 &  \cellcolor{colorblind_light_gray}9 & 7
				\\ \hline
			\end{tabular}
			\\ & & & \\ 1 & 2 & 1 &
			\texttt{i++} (1), \texttt{minIndex = i} (1),
			\texttt{j = i+1} (2).
			&	
			\begin{tabular}{| c | c | c | c |} \hline
				4 & \cellcolor{colorblind_light_blue}5 & \cellcolor{colorblind_light_orange}9 & 7
				\\ \hline
			\end{tabular}
			\\ & & & \texttt{arr[j] < arr[minIndex]}? (false)
			\\ & & & \\ 1 & 3 & 1 & \texttt{j++} (3).
			&	
			\begin{tabular}{| c | c | c | c |} \hline
				4 & \cellcolor{colorblind_light_blue}5 & 9 & \cellcolor{colorblind_light_orange}7
				\\ \hline
			\end{tabular}
			\\ & & & \texttt{arr[j] < arr[minIndex]}? (false)
			
			\\ & & &
			\texttt{j++} (4)
			
			End of inner for loop
			&	
			\begin{tabular}{| c | c | c | c |} \hline
				4 & \cellcolor{colorblind_light_blue}5 & 9 & 7
				\\ \hline
			\end{tabular}
			
			\\ & & & \\ & & & 
			\texttt{minIndex != i}? (false)
			\\ & & & \\ 2 & 3 & 2 &
			\texttt{i++} (2), \texttt{minIndex = i} (2),
			\texttt{j = i+1} (3).
			&	
			\begin{tabular}{| c | c | c | c |} \hline
				4 & 5 & \cellcolor{colorblind_light_blue}9 & \cellcolor{colorblind_light_orange}7
				\\ \hline
			\end{tabular}
			\\ & & & \texttt{arr[j] < arr[minIndex]}? (true)
			\\ 2 & 3 & 3 &
			Set \texttt{minIndex = j} (3).
			
			\\ & & &
			\texttt{j++} (4)
			
			End of inner for loop
			&	
			\begin{tabular}{| c | c | c | c |} \hline
				4 & 5 & \cellcolor{colorblind_light_blue}9 & \cellcolor{colorblind_light_gray}7
				\\ \hline
			\end{tabular}
		\end{tabular}
	\end{center}
	
	\newpage
			
	\begin{center}
		\begin{tabular}{c | c | c | p{8cm} p{4cm}}
			\texttt{i} & \texttt{j} & \texttt{min} \\
			2 & 3 & 3 & 
			\texttt{minIndex != i}? (true)
			\\ & & & Swap \texttt{arr[i]} and \texttt{arr[minIndex]}.
			&	
			\begin{tabular}{| c | c | c | c |} \hline
				4 & 5 & \cellcolor{colorblind_light_blue}7 & \cellcolor{colorblind_light_gray}9
				\\ \hline
			\end{tabular}
			
			\\ & & &
			\texttt{i++} (3)
			
			End of outer for loop
		\end{tabular}
	\end{center}
			
			
			
			
			
			
	\newpage
	\section{More sorting algorithms}
	
	Insertion sort and selection sort are two sorting algorithms with
	relatively simple implementations. There are also more advanced
	sorting algorithms that have better complexity, such as...
	
	\begin{center}
		\begin{tabular}{ l | c | c | c }
			\textbf{Algorithm} 	& \textbf{Best} 		& \textbf{Average} 		& \textbf{Worst} \\ \hline
			Quicksort 			& $O(n \cdot log(n))$	& $O(n \cdot log(n))$	& $O(n^2)$
			\\
			Mergesort			& $O(n \cdot log(n))$	& $O(n \cdot log(n))$	& $O(n \cdot log(n))$
			\\
			Bubble sort			& $O(n)$				& $O(n^2)$				& $O(n^2)$
			\\
			Shell sort			& $O(n \cdot log(n))$	& $O(n(log(n))^2)$		& $O(n(log(n))^2)$	
		\end{tabular}
	\end{center}
	
	If you search for sorting algorithms on YouTube, you can also find some
	cool visualizations of each sorting algorithm running over time.
	
	
	
	\chapter{Searching} %-----------------------------------------------% 
	
	Searching is also something that happens quite a bit no matter what kind
	of software is being written. We want to be able to search for data and
	not have to wait too long for the results.

	\section{Linear search}
	
	A linear search is a very basic search, but it works on \textbf{unsorted data}.
	You basically start at the beginning, look at each element moving forward by 1
	each time, and if you find the element you return that index number.
	
	Its time complexity is $O(n)$ because as the data increases, the time
	to process increases linearly.
	
\begin{lstlisting}[style=code]
int LinearSearch( int findme, int arr[], int size )
{
    for ( int i = 0; i < size; i++ )
    {
        if ( arr[i] == findme )
        {
            return i;
        }
    }

    return -1; // not found
}
\end{lstlisting}

	\newpage
	\section{Binary search}
	
	When we have sorted data, we can make more intelligent decisions
	on how to search through our set of data. A binary search has
	an average complexity of $O(log(n))$ because each iteration
	we cut out half of the data set, decreasing the total amount we
	have to search each time.
   
\begin{lstlisting}[style=code]
int BinarySearch( int findme, int arr[], int size )
{
    int left = 0;
    int right = size - 1;

    while ( left <= right )
    {
        int mid = ( left + right ) / 2;

        if ( arr[mid] < findme )
        {
            left = mid + 1;
        }
        else if ( arr[mid] > findme )
        {
            right = mid - 1;
        }
        else if ( arr[mid] == findme )
        {
            return mid;
        }
    }

    return -1; // not found
}
\end{lstlisting}

	Let's step through the searching algorithm on the next page.
	
	\newpage
	
	\begin{center}
		\begin{tabular}{c | c | c | p{11cm}}
			\texttt{L} & \texttt{R} & \texttt{M} \\
			0 & 11 & 5 & 
			Let's say we're searching for 24. Our array size is 12.
			
			Set \texttt{left=0}, \texttt{right=size-1},
			and \texttt{mid = (left+right)/2} 
			
			(throw out remainder).
			
			\begin{tabular}{| c | c | c | c | c | c | c | c | c | c | c | c |} \hline
				\cellcolor{colorblind_light_blue}10 & 13 & 15 & 17 & 18 & \cellcolor{colorblind_light_gray}20 & 22 & 24 & 25 & 27 & 29 & \cellcolor{colorblind_light_orange}30
				\\ \hline
				0 & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 & 10 & 11 \\ \hline
			\end{tabular}
			
			\\ & & &
			\texttt{arr[mid] < findme}? (true). ($20 < 24$)
			\\ 
			6 & 11 & 5 & Set \texttt{left = mid + 1} (6).
			\\ & & &
			\begin{tabular}{| c | c | c | c | c | c | c | c | c | c | c | c |} \hline
				10 & 13 & 15 & 17 & 18 & \cellcolor{colorblind_light_gray}20 & \cellcolor{colorblind_light_blue}22 & 24 & 25 & 27 & 29 & \cellcolor{colorblind_light_orange}30
				\\ \hline
				0 & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 & 10 & 11 \\ \hline
			\end{tabular}
			\\ & & & \\
			6 & 11 & 8 &
			Set \texttt{mid = (left + right)/2} (8).
			\\ & & &
			\begin{tabular}{| c | c | c | c | c | c | c | c | c | c | c | c |} \hline
				10 & 13 & 15 & 17 & 18 & 20 & \cellcolor{colorblind_light_blue}22 & 24 & \cellcolor{colorblind_light_gray}25 & 27 & 29 & \cellcolor{colorblind_light_orange}30
				\\ \hline
				0 & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 & 10 & 11 \\ \hline
			\end{tabular}
			
			\\ & & &
			\texttt{arr[mid] < findme}? (false). ($25 \not< 24$)
			
			\\ & & &
			\texttt{arr[mid] > findme}? (true). ($25 > 24$)
			\\ 
			6 & 7 & 8 & Set \texttt{right = mid - 1} (7).
			\\ & & &
			\begin{tabular}{| c | c | c | c | c | c | c | c | c | c | c | c |} \hline
				10 & 13 & 15 & 17 & 18 & 20 & \cellcolor{colorblind_light_blue}22 & \cellcolor{colorblind_light_orange}24 & \cellcolor{colorblind_light_gray}25 & 27 & 29 & 30
				\\ \hline
				0 & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 & 10 & 11 \\ \hline
			\end{tabular}
			\\ & & & \\
			6 & 7 & 6 &
			Set \texttt{mid = (left + right)/2} (6).
			\\ & & &
			\begin{tabular}{| c | c | c | c | c | c | c | c | c | c | c | c |} \hline
				10 & 13 & 15 & 17 & 18 & 20 & \cellcolor{colorblind_light_blue}22 & \cellcolor{colorblind_light_orange}24 & 25 & 27 & 29 & 30
				\\ \hline
				0 & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 & 10 & 11 \\ \hline
			\end{tabular}
			
			\\ & & &
			\texttt{arr[mid] < findme}? (true). ($22 < 24$)
			\\ 7 & 7 & 6 &
			Set \texttt{left = mid + 1} (7).
			
			\\ 7 & 7 & 7 &
			Set \texttt{mid = (left+right)/2} (7).
			\\ & & &
			\begin{tabular}{| c | c | c | c | c | c | c | c | c | c | c | c |} \hline
				10 & 13 & 15 & 17 & 18 & 20 & 22 & \cellcolor{colorblind_light_orange}24 & 25 & 27 & 29 & 30
				\\ \hline
				0 & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 & 10 & 11 \\ \hline
			\end{tabular}
			\\ & & & 
			\\ & & & \texttt{arr[mid] < findme}? (false). ($24 \not<24$)
			\\ & & & \texttt{arr[mid] > findme}? (false). ($24 \not>24$)
			\\ & & & \texttt{arr[mid] == findme}? (true). ($24 == 24$)
			\\ & & & return \texttt{mid} (7).
		\end{tabular}
	\end{center}
	
	At the end, this algorithm returns the index 
	of the element that has the value we were searching for.
			

   
   
   
\end{document}

