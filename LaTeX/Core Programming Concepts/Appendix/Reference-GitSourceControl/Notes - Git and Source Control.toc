\contentsline {chapter}{\numberline {1}What is source control?}{2}
\contentsline {chapter}{\numberline {2}Installing Git}{3}
\contentsline {chapter}{\numberline {3}Using Git and GitLab}{4}
\contentsline {section}{\numberline {3.1}Creating a repository in GitLab}{4}
\contentsline {section}{\numberline {3.2}Cloning the repository}{4}
\contentsline {section}{\numberline {3.3}Committing changes}{4}
\contentsline {section}{\numberline {3.4}Pulling latest changes and merging}{4}
\contentsline {section}{\numberline {3.5}Pushing your changes}{4}
