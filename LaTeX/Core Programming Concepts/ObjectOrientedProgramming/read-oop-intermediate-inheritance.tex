\newpage
\section{Inheritance}
Inheritance is where one class inherits some or all member variables
and functions from some parent class. Certain ``traits'' are passed on,
and the child class can have additional member variables and functions
defined, making it a more \textit{specialized} version of the parent class.

\subsection{Design ideas}

    \subsubsection{Reusing functionality}

    Let's say we're designing an operating system that will store files.
    We can think about what \textbf{all files} have in common, and we
    can figure out what is different between different types of files.
    Any commonality could go in a \textbf{base class} (aka \textbf{parent class}).

    ~\\ So, a generic \textbf{File} class might have:
    \begin{center}
        filename
        \tab	extension
        \tab	locationPath
        \tab	fileSize
        \tab	creationDate \\
        \tab	CreateFile()
        \tab	RenameFile()
        \tab	GetSize()
        \tab	GetPath()
    \end{center}

    Then we could have specialized file classes that inherit from the original File,
    such as TextFile, ImageFile, SoundFile, and so on. These would \textbf{inherit}
    all the common attributes of its parent (File), but each would have additional
    variables and methods that are suited specifically to its own specialization.

    \begin{center}
        \begin{tabular}{l p{10cm}}
            \textbf{TextFile} & textContent, Print()
            \\
            \textbf{ImageFile} & pixels[ ][ ], Draw(), EditPixel( x, y )
            \\
            \textbf{SoundFile} & audioSample, length, Play(), Pause(), Stop()
        \end{tabular}
    \end{center}

    \newpage
    \subsubsection{The iostream and fstream}

    \begin{center}
        \begin{tikzpicture}
            \coordinate (A) at (0, 10);
            \coordinate (B) at (0, 8.5);
            \coordinate (Ca) at (-3, 7);
            \coordinate (Cb) at (3, 7);
            \coordinate (Da) at (-5, 5.5);
            \coordinate (Db) at (-2, 5.5);
            \coordinate (Dc) at (2, 5.5);
            \coordinate (Dd) at (5, 5.5);

            \draw (A) -- (B) -- (Ca) -- (B) -- (Cb);
            \draw (Da) -- (Ca) -- (Db);
            \draw (Dc) -- (Cb) -- (Dd);

            \filldraw[fill=white] (A) ellipse (1cm and 0.5cm) node  {ios\_base};
            \filldraw[fill=white] (B) ellipse (1cm and 0.5cm) node 	{ios};
            \filldraw[fill=white] (Ca) ellipse (1cm and 0.5cm) node 	{istream};
            \filldraw[fill=white] (Cb) ellipse (1cm and 0.5cm) node 	{ostream};

            \filldraw[fill=white] (Da) ellipse (1cm and 0.5cm) node 	{ifstream};
            \filldraw[fill=white] (Db) ellipse (1cm and 0.5cm) node 	{cin};
            \filldraw[fill=white] (Dc) ellipse (1cm and 0.5cm) node 	{ofstream};
            \filldraw[fill=white] (Dd) ellipse (1cm and 0.5cm) node 	{cout};
        \end{tikzpicture}
        ~\\~\\
        \footnotesize
        Just part of the ios family. You can see the full library's family
        tree at http://www.cplusplus.com/reference/ios/
    \end{center}

    One family we've already been using are our console input/output streams,
    \texttt{cin} and \texttt{cout}, and our file input/output streams,
    \texttt{ifstream} and \texttt{ofstream}. Working with each of these works
    largely the same way.

    When we overload the \texttt{<<} and \texttt{>>} stream operators,
    we use a base type of \texttt{istream} and \texttt{ostream} so that
    our class can handle \texttt{cin} / \texttt{cout} AND \texttt{ifstream} / \texttt{ofstream},
    as well as any other streams in that family tree.

    \subsection{Inheritance (Is-A) vs. Composition (Has-A)}
    Another way we think about inheritance is that one class ``is-a'' type of other class.
    ~\\
    \textbf{Car \textit{is-a} vehicle:}
\begin{lstlisting}[style=code]
class Car : public Vehicle
{
// etc.
};
\end{lstlisting}
    Whereas Composition, where one class \textit{contains} another class
    as a member variable object, is known as a ``has-a'' relationship.
    ~\\
    \textbf{Car \textit{has-an} engine:}
\begin{lstlisting}[style=code]
class Car
{
private:
Engine engine;
};
\end{lstlisting}

    When is composition more appropriate than inheritance?
    This is a design question and it can really depend on how
    the designer feels about each one.

    \subsubsection{Example: Game objects}

    For example, when writing a game, I find \textit{inheritance}
    handy for creating game objects. A \textbf{BaseObject} can
    contain everything that's in common between all objects (characters,
    items, tiles, etc.), such as:

\begin{figure}[h]
\centering
\begin{subfigure}{.55\textwidth}

\begin{center}
\includegraphics{ObjectOrientedProgramming/images/apple.png}
\end{center}

Any items that don't animate, don't move, and just sit there
in the level could be instantiated as a simple BaseObject and it would be fine.

\end{subfigure}%
\begin{subfigure}{.45\textwidth}
\centering

    \begin{tabular}{ | l | }
        \hline
        \multicolumn{1}{|c|}{\textbf{BaseObject}}
        \\ \hline
        \# m\_position : Coordinate \\
        \# m\_sprite : Sprite \\
        \# m\_name : string \\
        \hline
        + Setup( ... ) \\
        + SetTexture( ... ) \\
        + Update() \\
        + SetPosition( ... ) \\
        + Draw( ... ) \\
        \hline
    \end{tabular}

\end{subfigure}
\end{figure}

Then, let's say we want to build a 2D game level and each level is
made up of a 2D array of tiles. Tiles will be very similar to objects,
but maybe we want more information stored in a tile. We could then
\textit{inherit} from the BaseObject...

\begin{figure}[h]
\centering
\begin{subfigure}{.55\textwidth}

\begin{center}
\includegraphics{ObjectOrientedProgramming/images/bricks.png}
\end{center}

The \textbf{Tile} class would have all the \textbf{public}
and \textbf{protected} members from its parent, plus any
new member variables and methods we declare within this class.

\end{subfigure}%
\begin{subfigure}{.45\textwidth}
\centering

    \begin{tabular}{ | l | }
        \hline
        \multicolumn{1}{|c|}{\textbf{Tile : BaseObject}}
        \\ \hline
        - m\_isSolid : boolean \\
        - m\_doesDamage : boolean \\
        \hline
    \end{tabular}

\end{subfigure}
\end{figure}

For the game characters, there would be two different types usually:
characters that the players control and characters that the computer controls.
\textbf{Character} could be its own base-class as well, implementing
anything a character can do, with a \textbf{Player} and \textbf{NPC} (non-player-character)
classes inheriting from \textbf{Character}.

\begin{center}

    \begin{tabular}{ | l | }
        \hline
        \multicolumn{1}{|c|}{\textbf{Character : BaseObject}}
        \\ \hline
        \# m\_speed : int \\
        \# m\_direction : Direction \\
        \# m\_animationFrame : float \\
        \# m\_maxFrames : float \\
        \hline
        + Move( ... ) \\
        + Animate( ... ) \\
        \hline
    \end{tabular}

\end{center}

\begin{figure}[h]
\centering
\begin{subfigure}{.55\textwidth}
\centering

    \begin{tabular}{ | l | }
        \hline
        \multicolumn{1}{|c|}{\textbf{Player : Character}}
        \\ \hline
        - score : int \\
        - experience : int \\
        - level : int
        \\ \hline
        + HandleKeyboard( ... ) \\
        + LevelUp()
        \\ \hline
    \end{tabular}

    \begin{center}
    \includegraphics{ObjectOrientedProgramming/images/ayda.png}
    \end{center}
\end{subfigure}%
\begin{subfigure}{.45\textwidth}
\centering

    \begin{tabular}{ | l | }
        \hline
        \multicolumn{1}{|c|}{\textbf{NPC : Character}}
        \\ \hline
        - difficulty : int \\
        - isFriendly : boolean
        \\ \hline
        + SetGoal( ... ) \\
        + DecideNextAction()
        \\ \hline
    \end{tabular}
    \begin{center}
    \includegraphics{ObjectOrientedProgramming/images/enemy.png}
    \end{center}
\end{subfigure}
\end{figure}




The \textbf{Player} class would be a specialized type of
Character that can be controled via the keyboard (or some other
input device), as well as have information that a non-player-character
generally has, such as a score or a level.

Likewise, the \textbf{NPC} class is a specialization of Character
as well, but with special functionality and attributes of a character
in the game controlled by the computer itself. This could include
some form of AI to help the NPC decide how to behave. ~\\

\newpage

    \begin{center}
        \textbf{Game object family:} ~\\~\\

        \begin{tikzpicture}
            \coordinate (A) at (0, 10);
            \coordinate (B) at (0, 8.5);
            \coordinate (Ca) at (-3, 8.5);
            \coordinate (Cb) at (3, 8.5);
            \coordinate (Dc) at (1, 7);
            \coordinate (Dd) at (5, 7);

            \draw (Ca) -- (A) -- (Cb);
            \draw (Dc) -- (Cb) -- (Dd);

            \filldraw[fill=white] (A) ellipse (1.5cm and 0.5cm) node  {BaseObject};
            \filldraw[fill=white] (Ca) ellipse (1cm and 0.5cm) node 	{Tile};
            \filldraw[fill=white] (Cb) ellipse (1cm and 0.5cm) node 	{Character};

            \filldraw[fill=white] (Dc) ellipse (1cm and 0.5cm) node 	{Player};
            \filldraw[fill=white] (Dd) ellipse (1cm and 0.5cm) node 	{NPC};
        \end{tikzpicture}
    \end{center}

With this family of game objects, we could also write functions
that work on \textit{all} types of game objects, such as
checking if there's a collision between items, because they all
have a common parent.

\begin{center}
    \includegraphics[width=10cm]{ObjectOrientedProgramming/images/screenshot.png}
\end{center}


%\newpage
%\subsection{Warning: Diamond problem}
%In C++ (but not Java and C\#) it's possible to inherit from
%more than one parent class.

    %\begin{center}
        %\begin{tikzpicture}
            %\coordinate (A) at (0, 10);
            %\coordinate (B) at (-3, 8.5);
            %\coordinate (C) at (3, 8.5);
            %\coordinate (D) at (0, 7);

            %\draw[-stealth,ultra thick,blue] (A) -- (-2.5, 9);
            %\draw[-stealth,ultra thick,blue] (A) -- (2.5, 9);
            %\draw[-stealth,ultra thick,blue] (B) -- (-0.5, 7.5);
            %\draw[-stealth,ultra thick,blue] (C) -- (0.5, 7.5);

            %\filldraw[fill=white] (A) ellipse (1.5cm and 0.5cm) node  	{BaseObject};
            %\filldraw[fill=white] (B) ellipse (1cm and 0.5cm) node 		{Tile};
            %\filldraw[fill=white] (C) ellipse (1cm and 0.5cm) node 		{Character};
            %\filldraw[fill=white] (D) ellipse (1cm and 0.5cm) node 		{Player};
        %\end{tikzpicture}
    %\end{center}



\newpage
\subsection{Implementing inheritance}

    In C++ a class can inherit from one or more other classes.
    Note that Java and C\# do not support multiple inheritance directly,
    if you end up switching to those languages. Design-wise, however,
    it can be messy to inherit from more than one parent and as an
    alternative, you might find a \textit{composition (has-a)} approach
    a better design.



    \subsection{Inheriting from one parent}

    To inherit from one other class in C++, all you need to do
    is add ``\texttt{: public OTHERCLASS}'' after the ``\texttt{class CLASSNAME}''
    at the beginning of your class declaration.
    With a \textbf{public inheritance} (the most common kind),
    the child class will inherit all \textbf{public and protected members}
    of the parent class.

\begin{lstlisting}[style=code]
// File is the parent
class File
{
protected:
string name;
string extension;
size_t filesize;
};

// TextDocument is the child
class TextDocument : public File
{
protected:
string textContents;
/* Also inherits these members from File:
string name;
string extension;
size_t filesize;
*/
};
\end{lstlisting}

    \begin{hint}{Also Known As...}
        The \textbf{Parent Class} is often known as the \textbf{superclass}
        and the \textbf{Child Class} is often known as the \textbf{subclass},
        particularly from the Java perspective.
    \end{hint}


    \subsection{Inheriting from multiple parents}

    You can also have a class inherit from multiple parents,
    bringing in public/protected members from all parent classes,
    but again this can muddle your program's design somewhat and
    is generally pretty uncommon.

\begin{lstlisting}[style=code]
class AnimationObject	// Parent A
{
public:
void Animate();

protected:
int totalFrames;
int currentFrame;
};

class PhysicsObject		// Parent B
{
public:
void Fall();
void Accelerate();

protected:
float velocity;
float acceleration;
};

class PlayerCharacter : public AnimationObject,
                public PhysicsObject
{
public:
void HandleKeyboard();

protected:
string name;
};
\end{lstlisting}

    \newpage
    \subsubsection{Private, Public, and Protected}

        \paragraph{Member access} ~\\

        Remember that we can declare our member variables and methods
        as \textbf{private} and \textbf{public}, but we can also
        make items \textbf{protected}. Protected members
        are similar to private, except that protected members
        will be inherited by child classes - private members \textit{do not}
        get inherited.

        \begin{center}
            \begin{tabular}{| l | c | c | c |}	\hline
                \textbf{Access} & \textbf{Class' methods} 	& \textbf{Childrens' methods} & \textbf{Outside class} \\ \hline
                public
                    & \cellcolor{colorblind_light_blue}Accessible
                    & \cellcolor{colorblind_light_blue}Accessible
                    & \cellcolor{colorblind_light_blue}Accessible
                \\ \hline
                protected
                    & \cellcolor{colorblind_light_blue}Accessible
                    & \cellcolor{colorblind_light_blue}Accessible
                    & \cellcolor{colorblind_light_orange}Not accessible
                \\ \hline
                private
                    & \cellcolor{colorblind_light_blue}Accessible
                    & \cellcolor{colorblind_light_orange}Not accessible
                    & \cellcolor{colorblind_light_orange}Not accessible
                \\ \hline
            \end{tabular}
        \end{center}

        \paragraph{Inheritance style} ~\\
        You can also use public, protected, and private when inheriting other classes.
        You probably won't need to ever do a protected or private inheritance.
        
        \begin{itemize}
            \item	\textbf{public inheritance:} 	Child class inherits \textbf{public and protected members} from the parent.
            \item	\textbf{protected inheritance:} Child class inheirits \textbf{public and protected members} and turns them into protected members.
            \item	\textbf{private inheritance:}	Child class inherits \textbf{public and protected members} and turns them into private members.
        \end{itemize}
        
        This could be used if, for some reason, you want a child class to inherit certain members
        but don't want ``grandchildren'' to have access to those members as well.
    
    \newpage
    \subsubsection{Function Overriding}
    
        When a child class inherits from a parent class, the parent
        class' public and protected methods are passed down to the child
        and these functions are still callable for objects of the child type.
    
        If we would like, the child class can also
        \textbf{override} any methods from the parent. 
        This means that we write a new version of the function
        with the same return type and parameter list, and when
        that function is called from the child class, the child class'
        version will be called instead of the parents'.
        
        ~\\
        Let's say we're writing a quizzer program that will support
        differnet question types. We can write a base ``Question'' class
        with whatever would be in common between all Questions:
                        
\begin{lstlisting}[style=code]
class Question
{
public:
Question( string q, string a ) {
question = q;
answer = a;
}

void AskQuestion() {
cout << question << endl;
string playerAnswer;
cin >> playerAnswer;

if ( playerAnswer == answer ) {
    cout << "Right!" << endl;
} else {
    cout << "Wrong." << endl;
}
}

protected:
string question;
string answer;
};

\end{lstlisting}

    We can then inherit from our Question class to make
    different types of quiz questions: Multiple choice, true/false,
    and so on. Adding additional functionality means that we might
    want to rewrite how \texttt{AskQuestion()} works - so we can.
    
    This also means that everything in our Question family has a
    similar interface - create a question, set it up, and then
    call \texttt{AskQuestion()} no matter what kind of question it is.


\begin{lstlisting}[style=code]
class MultipleChoiceQuestion : public Question
{
public:
void DisplayQuestion()
{
cout << question << endl;
for ( int i = 0; i < 4; i++ )
{
    cout << i << ". " 
         << answerChoices[i] << endl;
}
int playerAnswer;
cin >> playerAnswer;

if ( answerChoices[ playerAnswer ] == answer ) {
    cout << "Right!" << endl;
} else {
    cout << "Wrong." << endl;
}
}

protected:
string answerChoices[4];
};
\end{lstlisting}

        \paragraph{Calling the parents' version of a method} ~\\
        
        In some cases, perhaps the parent version of a method has
        some common functionality we will need no matter what
        version in the family we're using, but the child versions
        of the methods add on to that functionality. We can
        directly call the parent's version of some method from
        within a child's method by explicitly calling the method
        with the parent class name and the \textbf{scope resolution operator ::}.

        \newpage
\begin{lstlisting}[style=code]
class Lion
{
public:
void Activities()
{
cout << "Hunt" << endl;
cout << "Eat" << endl;
cout << "Sleep" << endl;
}
};

class HouseCat : public Lion
{
public:
void Activities()
{
Lion::Activities();
cout << "Talk to human" << endl;
cout << "Use catbox" << endl;
}
};
\end{lstlisting}
    
    For this example, the HouseCat does everything the Lion does
    and adds a few activities to the list. We don't have to re-write
    the same Activities from Lion - we can just call the parent's
    version of the method directly with \texttt{Lion::Activities();}.

    \subsubsection{Constructors and Destructors in the family}
    
    We often use constructors to initialize member variables for a class,
    and when a child is inheriting its parents' member variables,
    we want to make sure we're not forgetting to still initialize those
    variables.
    
    We can call the parent's constructor from the child's constructor
    as well, but we do it via the \textbf{initializer list} of a
    constructor - a little bit of code after the constructor's
    function header that can be used to initialize variables
    and call the parent constructor.
    
    \newpage
\begin{lstlisting}[style=code]
class Person
{
public:
Person( string name )
{
m_name = name;
}

protected:
string m_name;
};

class Student : public Person
{
public:
Student( string name, string major )
: Person( name ) // Calling the parent ctor
{
m_major = major;
}

protected:
string m_major;
};
\end{lstlisting}
    \begin{center}
        \footnotesize (ctor is short for constructor)
    \end{center}
    
    We can also initialize variables through the initializer
    list as well:
\begin{lstlisting}[style=code]
class Student : public Person
{
public:
Student( string name, string major )
: Person( name ), m_major( major )
{
// nothing else to do now!
}

protected:
string m_major;
};
\end{lstlisting}
