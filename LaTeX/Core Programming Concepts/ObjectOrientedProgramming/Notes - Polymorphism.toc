\contentsline {section}{\numberline {0.1}Polymorphism}{2}%
\contentsline {subsection}{\numberline {0.1.1}Design and polymorphism}{2}%
\contentsline {subsubsection}{Example: Quizzer and multiple question types}{3}%
\contentsline {subsubsection}{Other design considerations}{4}%
\contentsline {subsection}{\numberline {0.1.2}Review: Class inheritance and function overriding}{5}%
\contentsline {subsection}{\numberline {0.1.3}Review: Pointers to class objects}{5}%
\contentsline {subsection}{\numberline {0.1.4}Which version of the method is called?}{6}%
\contentsline {paragraph}{No virtual methods - Which \texttt {AskQuestion()} is called?}{7}%
\contentsline {paragraph}{Virtual methods - Which \texttt {AskQuestion()} is called?}{8}%
\contentsline {subsection}{\numberline {0.1.5}Virtual methods, late binding, and the Virtual Table}{9}%
\contentsline {paragraph}{The \texttt {virtual} keyword}{9}%
\contentsline {subsection}{\numberline {0.1.6}When should we use \texttt {virtual}?}{10}%
\contentsline {paragraph}{Destructors should always be virtual.}{10}%
\contentsline {paragraph}{Constructors cannot be marked \texttt {virtual}.}{10}%
\contentsline {paragraph}{Not every function needs to be virtual.}{10}%
\contentsline {subsection}{\numberline {0.1.7}Designing interfaces with pure virtual functions and abstract classes}{11}%
\contentsline {paragraph}{What is an Interface?}{11}%
\contentsline {paragraph}{Declarations:}{13}%
\contentsline {paragraph}{Definitions:}{14}%
\contentsline {paragraph}{Function calls:}{15}%
