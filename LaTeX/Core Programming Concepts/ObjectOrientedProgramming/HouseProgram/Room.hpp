#ifndef _ROOM_HPP
#define _ROOM_HPP

#include <string>
using namespace std;

class Room
{
    public:
    void Setup();

    string GetName();
    int GetWidth();
    int GetLength();
    int GetArea();

    private:
    string name;
    int width;
    int length;
};

#endif

