\section{Enumerations}

  \subsection{Traditional enums}
  
  When designing a program we sometimes need a list of states
  or categories or attributes, such as:
  
  \begin{center}
    \begin{tabular}{p{4cm} p{4cm} p{4cm} }
      \textbf{Directions}
       
      NORTH,
      
      SOUTH, 
      
      EAST, 
      
      WEST
      &
      \textbf{BookType}
      
      PRINT, 
      
      EBOOK, 
      
      AUDIOBOOK, 
      
      MAGAZINE 
      &
      \textbf{PrinterStatus}
      
      OFFLINE, 
      
      ONLINE, 
      
      ERROR\_NO\_PAPER, 
      
      ERROR\_NO\_INK       
    \end{tabular}
  \end{center}
  
  What is a good way to store this information?
  
  \paragraph{Storing categories with strings:}
  Maybe the easiest way seems to be to store these states with
  \texttt{string} variables. After all, when you read this in the code:
  
\begin{lstlisting}[style=code]
if ( printerA.status == "ONLINE" )
\end{lstlisting}

  that's pretty clear, right?
  ~\\
  
  The problem with using strings for this job is that \textbf{it is easy
  to mistype something}, which will lead to logic errors.
  Someone could end up writing \texttt{"online"} somewhere, which won't
  work because the status is all caps, or they might
  misspell it like \texttt{"nline"} on accident. That's a problem,
  and it's hard to guard against (are you going to put a spellchecker
  in your IDE?)
  
  \paragraph{Storing categories with numbers:}
  Instead of strings we \textit{could} use number codes instead. For example,
  we could write this:
    
\begin{lstlisting}[style=code]
if      ( printerA.status == 0 )  // Offline
//...
else if ( printerA.status == 1 )  // Online
//...
else if ( printerA.status == 2 )  // No paper
//...
else if ( printerA.status == 3 )  // No ink
//...
\end{lstlisting}

  The plus side here is that it's harder to typo a number, and we can make
  sure that the printer status is an integer, not any arbitrary string.
  ~\\
  
  However, throughout an entire program, seeing numeric literals everywhere
  is confusing (``What is 0? What is 1??'') - these are called ``magic numbers'',
  where you have to know what they represent to understand the code.
  
  \hrulefill
  \paragraph{OK, so what is better, then??} Let's take the best of both worlds,
  AND use a feature that the compiler will ensure is being used properly.
  ~\\
  
  With an \textbf{enumeration} (enum), we can specify a discrete (finite)
  set of states which can be represented as integers but also as 
  text within the code:
  
\begin{lstlisting}[style=code]
enum PrinterStates {
  OFFLINE = 0,
  ONLINE = 1,
  NO_PAPER = 2,
  NO_INK = 3
};
\end{lstlisting}

  With this, we are still representing the printer states with simple numbers,
  but not the entire range of integers -- just 0, 1, 2, and 3 (though these values are optional).
  
  \newpage
  Now we can write our code like this:  

\begin{lstlisting}[style=code]
if      ( printerA.status == OFFLINE )  
//...
else if ( printerA.status == ONLINE )  
//...
else if ( printerA.status == NO_PAPER )  
//...
else if ( printerA.status == NO_INK )  
//...
\end{lstlisting}

  And the best part is, if we typo one of these codes, the compiler will detect it
  if it's not part of the \texttt{PrinterStates} enum!
  ~\\
  
  So now we have self-documenting code: we can infer what these if statements
  are for based on just reading the line of code.
  
  \hrulefill
  \subsubsection{More info about enums}
  
    \paragraph{Declaring an enum variable:}
    
      Within our Printer class, we would declare our printer's status
      as a variable whose type is the enum:
  
\begin{lstlisting}[style=code]
PrinterStates status;
\end{lstlisting}

    \paragraph{Numbers can be assigned in any order:}
    You don't have to start the numbering at 0 and go up by 1 each time,
    you could set specific values to specific numbers however you'd like.
      
\begin{lstlisting}[style=code]
enum HttpStatusCode {
  OK = 200,
  ACCEPTED = 20,
  TEMP_REDIRECT = 307,
  PERM_REDIRECT = 308,
  BAD_REQUEST = 400,
  UNAUTHORIZED = 401,
  FORBIDDEN = 403,
  NOT_FOUND = 404
};
\end{lstlisting}

\newpage
    \paragraph{Numbers not required:}
    When creating an enum, we aren't required to specify integer values
    for each item, or any items! (But sometimes the numbers can be useful.)

\begin{lstlisting}[style=code]
enum BookType { PRINT, EBOOK, AUDIOBOOK, MAGAZINE };
\end{lstlisting}
    
    \paragraph{Outputting an enum gives you a number:}
    If you were writing an enum value to a text file or the console
    it would just show up as its assigned integer code.

\begin{lstlisting}[style=code]
HttpStatusCode returnCode = NOT_FOUND;

cout << "Code: " << returnCode << endl;
\end{lstlisting}


\begin{lstlisting}[style=output]
Code: 404
\end{lstlisting}


  \hrulefill
  \subsection{Enum classes (Since C++11)}
  
  As of C++11 (C++ from 2011) enums can now be written as \textbf{enum classes}.
  This has better type safety since it doesn't readily convert to an integer,
  which can help avoid issues and errors.
  
  When declaring a enum class, it will look like this:
  
\begin{lstlisting}[style=code]
enum class Light {
    STOP = 0,
    SLOW = 1,
    GO = 2
};
\end{lstlisting}
  
  And using the enum's values will look like this:
  
\begin{lstlisting}[style=code]
Light light = Light::STOP;

if ( light == Light::GO ) { light = Light::SLOW; }
\end{lstlisting}
  
  Now instead of just using labels like STOP, SLOW, and GO,
  they must be prefixed with the type of enum they are - so
  \texttt{Light::STOP}, \texttt{Light::SLOW}, and \texttt{Light::GO}.
  
  Another perk of this new form of enum is that now these enum values
  can be used for different enums. For example, maybe you used
  \texttt{STOP} for the state of a traffic light,
  but also for a state for the program itself, with STOP meaning
  ``close the program''. Now there's no ambiguity because you would
  have \texttt{Light::STOP} and \texttt{Program::STOP}.
