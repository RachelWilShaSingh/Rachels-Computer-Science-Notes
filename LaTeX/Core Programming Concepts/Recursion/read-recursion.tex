

\begin{figure}[h]
    \begin{center}
        \includegraphics[width=12cm]{images/recursionB.png}
    \end{center}
\end{figure}

\section{Examples of recursion in math} %------------------------------------%
    Recursion is a manner of solving a problem by breaking it down into
    smaller pieces - and those smaller pieces are solved in the same way
    as the big-picture version.
    
    
    \paragraph{Summation:} A summation can be broken down into smaller chunks
    but using the same structure as the original. ~\\
    
    Let's say we have
    $$ \sum_{i=1}^{6} { i } = 1 + 2 + 3 + 4 + 5 + 6 $$
    
    The summation can be redefined recursively:
    
    $$ \sum_{i=1}^{6} { i } = 6 + \sum_{i=1}^{5} { i } $$
    
    The summation from $i = 1$ to $6$ is equivalent to whatever the sum is
    at $i=6$, plus the sum from $i = 1$ to $5$.
    We can continue this way until we get to a case that we know (e.g., $\sum_{i=1}^{1} { i }$).
    
    \begin{itemize}
        \item   $ \sum_{i=1}^{6} { i } = 6 + \sum_{i=1}^{5} { i } $
        \item   $ \sum_{i=1}^{5} { i } = 5 + \sum_{i=1}^{4} { i } $
        \item   $ \sum_{i=1}^{4} { i } = 4 + \sum_{i=1}^{3} { i } $
        \item   $ \sum_{i=1}^{3} { i } = 3 + \sum_{i=1}^{2} { i } $
        \item   $ \sum_{i=1}^{2} { i } = 2 + \sum_{i=1}^{1} { i } $ - We know that $\sum_{i=1}^{1} { i } = 1$, then we move back up to sub out this value.
    \end{itemize}
    
    \newpage
    
    \begin{tabular}{p{6cm} | p{6cm}}
        \textbf{Recursive problem} & \textbf{Finding the solution} \\ 
        1. $$ \sum_{i=1}^{6} { i } = 6 + \sum_{i=1}^{5} { i } $$
        
        But what is $\sum_{i=1}^{5} { i }$?
        &
        11. $$ \sum_{i=1}^{6} { i } = 6 + \sum_{i=1}^{5} { i } = 6 + 15 = 21 $$
        \\ \multicolumn{1}{c |}{ $\downarrow$ } & \multicolumn{1}{c}{ $\uparrow$ } \\
        2. $$ \sum_{i=1}^{5} { i } = 5 + \sum_{i=1}^{4} { i } $$
        
        But what is $\sum_{i=1}^{4} { i }$?
        &
        10. $$ \sum_{i=1}^{5} { i } = 5 + \sum_{i=1}^{4} { i } = 5 + 10 = 15 $$
        \\ \multicolumn{1}{c |}{ $\downarrow$ } & \multicolumn{1}{c}{ $\uparrow$ } \\
        3. $$ \sum_{i=1}^{4} { i } = 4 + \sum_{i=1}^{3} { i } $$
        
        But what is $\sum_{i=1}^{3} { i }$?
        &
        9. $$ \sum_{i=1}^{4} { i } = 4 + \sum_{i=1}^{3} { i } = 4 + 6 = 10 $$
        \\ \multicolumn{1}{c |}{ $\downarrow$ } & \multicolumn{1}{c}{ $\uparrow$ } \\
        4. $$ \sum_{i=1}^{3} { i } = 3 + \sum_{i=1}^{2} { i } $$
        
        But what is $\sum_{i=1}^{2} { i }$?
        &
        8. $$ \sum_{i=1}^{3} { i } = 3 + \sum_{i=1}^{2} { i } = 3 + 3 = 6 $$
        \\ \multicolumn{1}{c |}{ $\downarrow$ } & \multicolumn{1}{c}{ $\uparrow$ } \\
        5. $$ \sum_{i=1}^{2} { i } = 2 + \sum_{i=1}^{1} { i } $$
        
        But what is $\sum_{i=1}^{1} { i }$?
        &
        7. $$ \sum_{i=1}^{2} { i } = 2 + \sum_{i=1}^{1} { i } = 2 + 1 = 3 $$
        \\ \multicolumn{1}{c |}{ $\downarrow$ } & \multicolumn{1}{c}{ $\uparrow$ } \\
        6. $\sum_{i=1}^{1} { i } = 1$
        \\ \multicolumn{1}{c |}{ $\downarrow$ } &  \\
        Now we start substituting this value, from bottom-up... $\rightarrow$
    \end{tabular}
    
    
    \newpage
    \paragraph{Factorials:} With a factorial of $n$, written $n!$ the
    formula to solve this is:
    
    $$ n! = n \cdot (n-1) \cdot ... \cdot 3 \cdot 2 \cdot 1 $$
    
    ~\\ So, $2!$ is $2 \cdot 1$, \tab $3!$ is $3 \cdot 2 \cdot 1$, \tab $4!$ is $4 \cdot 3 \cdot 2 \cdot 1$, and so on.
    
    ~\\ Additionally, we can break down each of these equations:
    ~\\ $3!$ is equivalent to $3 \cdot 2!$
    ~\\ $4!$ is equivalent to $4 \cdot 3!$
    ~\\ ...
    ~\\ $n!$ is equivalent to $n \cdot (n-1)!$
    ~\\
    
    Thinking of breaking down the problem here in this way is looking at it recursively.
    
    \begin{center}
        \includegraphics[width=14cm]{Recursion/images/recursivefactorial.png}
    \end{center}
    
    \newpage

\section{Recursion in programming} %------------------------------------%

    In programming, we usually approach problems \textbf{iteratively},
    using a for-loop or a while-loop:
        
\begin{lstlisting}[style=code]
// Iterative solution
int Sum( int n )
{
    int result = 0;
    for ( int i = 1; i <= n; i++ )
    {
        result += i;
    }
    return result;
}
\end{lstlisting}

    Which solves this summation: 
    
    $$ \sum_{i=1}^n {i} $$
    
    But some types of problems lend themselves better to a \textbf{recursive}
    solution. To be fair, though, many problems are \textit{better solved} \textbf{iteratively}.
    So how do we know which method is better?
    
    %-------------------------------------------------------------------%
    %-------------------------------------------------------------------%
    \newpage
    \subsection{Recursion basics}
    
    When defining a problem recursively in programming, we need two things:
    
    \begin{enumerate}
        \item   A \textbf{terminating case:} A case that ends our recursing.
                Often, this is some known data, something hard-coded.
                For example with our summation, the terminating case would be
                that $$ \sum_{i=1}^1 {i} = 1 $$
                or for a factorial, $ 1! = 1 $ and $ 0! = 1 $.
                
        \item   A \textbf{recursive case:} A recursive case is what happens
                otherwise - if we're not to a solution yet (via the terminating case),
                we call the same function again, but with updated arguments. For example:
                
                \begin{itemize}
                    \item   \texttt{ Factorial( 4 ) = 4 * Factorial( 3 ) }
                    \item   \texttt{ Factorial( 3 ) = 3 * Factorial( 2 ) }
                    \item   \texttt{ Factorial( 2 ) = 2 * Factorial( 1 ) }
                    \item   \texttt{ Factorial( 1 ) = 1 }
                \end{itemize}
    \end{enumerate}
    
    We can solve these basic math operations both iteratively and recursively:
    
\begin{lstlisting}[style=code]
// Iterative solution
int FactorialI( int n )
{
    int result = 1;
    for ( int i = 1; i <= n; i++ )
    {
        result *= i;
    }
    return result;
}
\end{lstlisting}

\begin{lstlisting}[style=code]
// Recursive solution
int FactorialR( int n )
{
    if ( n == 1 || n == 0 ) { return 1; }        
    return n * FactorialR( n-1 );
}
\end{lstlisting}

    %-------------------------------------------------------------------%
    %-------------------------------------------------------------------%
    \newpage
    \subsection{Breaking down problems into recursive solutions}
    
    \begin{center}
        \includegraphics[width=10cm]{Recursion/images/recursionstress.png}
    \end{center}
    
    One of the most challenging parts of recursion, at least for me,
    is trying to break away from thinking of something in terms of ``looping''
    and figuring out how to think of it ``recursively''. It's not as natural-feeling,
    so don't worry if it's confusing at first.
    
    Let's tackle some basic design problems to practice.
    
    \paragraph{Summation:} Try to convert the Summation function to
    be recursive. Think about what the \textbf{terminating case} would be
    and the \textbf{recursive case}. Use the Factorial function for reference.
    
\begin{lstlisting}[style=code]
int SumI( int n )
{
    int result = 0;
    for ( int i = 1; i <= n; i++ )
    {
        result += i;
    }
    return result;
}
\end{lstlisting}
    
\begin{lstlisting}[style=code]
int SumR( int n )
{
    // Terminating case?
    // Recursive case?
}
\end{lstlisting}

    \newpage
    \subparagraph{Solution for recursive summation:} ~\\
    
\begin{lstlisting}[style=code]
int SumR( int n )
{
    if ( n == 1 ) { return 1; }  // Terminating case
    return n + SumR( n-1 );      // Recursive case
}
\end{lstlisting}

    \hrulefill
    \paragraph{Example - Draw a line:} Now let's make a function that
    will draw a line of symbols, with a parameter being the length.
    Iteratively, it could look like this:
    
\begin{lstlisting}[style=code]
void DrawLineI( int amount )
{
    for ( int i = 0; i < amount; i++ )
    {
        cout << "-";
    }
}
\end{lstlisting}

    How would we repeat this behavior recursively? How do we have a
    ``count up'' sort of functionality? What would be the terminating case?
    ~\\
    
    We're going to think of it a little differently:
    The recursive function will only output one ``-'' before it recurses.
    Each time it recurses, it draws one more dash...

\begin{lstlisting}[style=code]
void DrawLine_Recursive( int amount )
{
    cout << "-";
    // Recursive case
    DrawLineR( amount );
}
\end{lstlisting}

    However, we don't have a \textbf{terminating case}... it will continue
    looping, but it won't go \textit{forever} like a bad while loop. We
    will eventually run out of \textbf{stack space} and the program will
    encounter a \textbf{stack overflow} and end.
    ~\\
    
    \newpage
    So what would the terminating case be? How do we adjust the \texttt{amount}
    each time? Since \texttt{amount} is the one parameter we have, let's
    have the recursion stop once it is 0. Each time we recurse, we can
    pass in \texttt{amount-1} to the next call...

\begin{lstlisting}[style=code]
void DrawLine_Recursive( int amount )
{
    cout << "-";
    
    // Terminating case
    if ( amount == 0 ) { return; }
    
    // Recursive case
    DrawLineR( amount - 1 );
}
\end{lstlisting}

    \hrulefill
    \paragraph{Counting Up:} How can we write a function that takes a
    \texttt{start} and \texttt{end} integer, and outputs each number
    between them (including the start and end)?
    
    ~\\
    Iteratively, it could look like this:
    
\begin{lstlisting}[style=code]
void CountUpI( int start, int end )
{
    for ( int i = start; i <= end; i++ )
    {
        cout << i << "\t";
    }
}
\end{lstlisting}

    ~\\ Try to fill in this function to build a recursive solution:

\begin{lstlisting}[style=code]
void CountUpR( int start, int end )
{
}

\end{lstlisting}

    \newpage
    \subparagraph{Solution for recursive count up:} ~\\
    
\begin{lstlisting}[style=code]
void CountUpR( int start, int end )
{
    cout << start << "\t";

    // Terminating case
    if ( start == end ) { return; }
    
    // recursive case
    CountUp_Recursive( start+1, end ); 
}
\end{lstlisting}

    \hrulefill

    %-------------------------------------------------------------------%
    %-------------------------------------------------------------------%
    \subsection{A case for recursion}
    
        Although there are a lot of problems we could convert from
        an iterative solution to a recursive solution, there are some
        types of problems that really are better suited to recursion.
        
        \subsubsection{Searching a File System}
        
            \begin{center}
                \includegraphics[width=10cm]{Recursion/images/filesystem.png}
            \end{center}
            
            On a harddrive, we generally have files and folders.
            Folders can contain files, but they will also contain subfolders
            as well. And subfolders can each contain \textit{their own} subfolders.
            
            When you don't know the exact layout of the filesystem, how would you
            even begin to iteratively search for a specific file?
            
            \newpage
            Instead, it is good to think of it recursively. For example,
            say we're searching for a folder where you store your Recursion homework.
            We will begin searching at the top-most folder of the computer.
            The algorithm would then run like...
            
            \begin{center}
                \begin{tabular}{ | p{4cm} | p{4cm} | p{4cm} | } \hline
                    1. & 2. & 3. \\
                    \includegraphics[width=4cm]{Recursion/images/traverse1.png} &
                    \includegraphics[width=4cm]{Recursion/images/traverse2.png} &
                    \includegraphics[width=4cm]{Recursion/images/traverse1.png} 
                    \\
                    folder = ``C:'', 
                    
                    Is this ``Recursion''? No.
                    
                    Are there subfolders? Yes.
                    
                    Then, search first subfolder.
                    &
                    folder = ``work'',
                    
                    Is this ``Recursion''? No.
                    
                    Are there subfolders? No.
                    
                    Return.
                    &
                    folder = ``C:''
                    
                    Are there subfolders? Yes.
                    
                    Then, search next subfolder.
                    \\ \hline
                    4. & 5. & 6. \\
                    \includegraphics[width=4cm]{Recursion/images/traverse3.png} &
                    \includegraphics[width=4cm]{Recursion/images/traverse4.png} &
                    \includegraphics[width=4cm]{Recursion/images/traverse5.png} 
                    \\
                    4. folder = ``school''
                    
                    Is this ``Recursion''? No.
                    
                    Are there subfolders? Yes.
                    
                    Then, search next subfolder.
                    &
                    5. folder = ``cs210''
                    
                    Is this ``Recursion''? No.
                    
                    Are there subfolders? No.
                    
                    Return.
                    &
                    6. folder = ``school''
                    
                    Are there subfolders? Yes.
                    
                    Then, search next subfolder.
                    
                    \\ \hline
                    %\\ \hline
                    
                    %7. folder = ``cs235''
                    
                    %Is this ``Recursion''? No.
                    
                    %Are there subfolders? Yes.
                    
                    %Then, search the first subfolder.
                    
                    %&
                    
                    %8. folder = ``Recursion''
                    
                    %Is this ``Recursion''? Yes.
                    
                    %Return this.
                    
                    %\\ \hline
                \end{tabular}
            \end{center}
            
            (Continued)
            
            \newpage
            
            \begin{center}
                \begin{tabular}{ | p{4cm} | p{4cm} | p{4cm} | } \hline
                    7. & 8. & 9. \\
                    \includegraphics[width=4cm]{Recursion/images/traverse6.png} &
                    \includegraphics[width=4cm]{Recursion/images/traverse8.png} &
                    \includegraphics[width=4cm]{Recursion/images/traverse9.png} 
                    \\
                    folder = ``cs235''
                    
                    Is this ``Recursion''? No.
                    
                    Are there subfolders? Yes.
                    
                    Then, search the first subfolder.
                    
                    &
                    
                    folder = ``Recursion''
                    
                    Is this ``Recursion''? Yes.
                    
                    Return \textbf{Recursion}.
                    
                    &
                    
                    folder = ``cs235''
                    
                    Return \textbf{Recursion}.
                    \\ \hline
                    10. & 11. &  \\
                    \includegraphics[width=4cm]{Recursion/images/traverse10.png} &
                    \includegraphics[width=4cm]{Recursion/images/traverse11.png} &
                    
                    \\
                    folder = ``school''
                    
                    Return \textbf{Recursion}.
                    
                    &
                    
                    folder = ``C:''
                    
                    Return \textbf{Recursion}.
                    
                    &
                    \\ \hline
                \end{tabular}
            \end{center}
            
            ~\\ For \textbf{find} functionality, terminating cases would be:
            
            \begin{enumerate}
                \item   Have we found the item? Return it.
                \item   Are we out of places to search? Return nothing.
            \end{enumerate}
            
            ~\\ And the recursive case would be:
            
            \begin{enumerate}
                \item   Has a subfolder? Call Find() on that folder.
            \end{enumerate}
            
        \newpage
        \subsubsection{Solving a maze}
        
            Solving a maze can be approached from a recursive standpoint
            much easier than an iterative one.
            
            ~\\ \textbf{Terminating case:} If we hit a dead-end, or if
                    we find the end of the maze.
                    
            ~\\ \textbf{Recursive case:} Explore each available direction.
        
            \begin{center}
                \begin{tabular}{ | p{4cm} | p{4cm} | p{4cm} | } \hline
                    1. & 2. & 3. \\
                    \includegraphics[width=4cm]{Recursion/images/maze.png} &
                    \includegraphics[width=4cm]{Recursion/images/maze1.png} &
                    \includegraphics[width=4cm]{Recursion/images/maze2.png} 
                    \\
                    Starting point. We will iterate through all directions
                    we can go. In some cases, there is only one valid direction.
                    & 
                    Once we hit multiple options, we will recurse into a direction.
                    If we end up returning unsuccessfully, we will recurse into
                    the \textit{other} direction.
                    &
                    If we hit a dead-end, this is a terminating case and
                    we begin returning.
                    \\ \hline
                    4. & 5. & 6. \\
                    \includegraphics[width=4cm]{Recursion/images/maze3.png} &
                    \includegraphics[width=4cm]{Recursion/images/maze4.png} &
                    \includegraphics[width=4cm]{Recursion/images/maze5.png} 
                    \\
                    We get back to a previous function call where we can
                    continue recursing in a different direction.
                    &
                    Hitting a terminating case and returning backwards
                    essentially ``undoes'' the path that takes us to
                    a dead-end.
                    &
                    The recursion ends once we either run out of all potential
                    paths to take, or we achieve the goal.
                    \\ \hline
                \end{tabular}
                \\
            \end{center}
            
        %\newpage
        %\subsection{Building a tree}
        %asfd
        
