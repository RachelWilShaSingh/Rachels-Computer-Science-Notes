    
    
        \begin{figure}[h]
            \begin{center}
                \includegraphics[width=12cm]{images/functions.png}
            \end{center}
        \end{figure}
        
    \section{Program Structure} %------------------------------------%    

\begin{figure}[h]
    \centering
    \begin{subfigure}{.5\textwidth}
        \centering
           \includegraphics[width=6cm]{images/errorinmain.png}
    \end{subfigure}%
    \begin{subfigure}{.5\textwidth}
       
           As programs become more sophisticated and offer more features,
           the size of the program increases. At some point, it becomes
           too complicated to keep your entire program just within \texttt{main()}.
           
           ~\\
           (You could certainly do it, but maintaining it would be a nightmare!)
           
           ~\\           
           One of the tools we use to build \textbf{modular}, \textbf{easier-to-read}
           code is \textbf{functions}. By using functions, we can delegate tasks
           out to other portions of the program, passing \textbf{inputs} as data
           to the function, and receiving some kind of \textbf{output} as a result.
           
    \end{subfigure}
\end{figure}

       As a basic example, let's say we need to calculate square footage of a room
       in multiple places in a program. By making a
       \begin{center}
       \texttt{float GetArea( float width, float length ) ~\\
            return width * length;
        }
       \end{center}
       function, we only have to implement the formula once and can use the
       function in every part of the program. Then, we wouldn't be copy-and-pasting
       the same formula over and over again in the code - which also means
       less likelihood of errors, and easier to update later as needed.
       
       \paragraph{Program flow and functions} ~\\
       
       Whenever a function is \textbf{called}, the program flow is redirected
       into that function, running from top-to-bottom as normal (with branching
       and loops changing that flow). Once the function has completed, it can
       \textbf{return} some data, which is received by the \textbf{call} location.
       
        \begin{center}
            \includegraphics[width=10cm]{images/functioncall.png}
        \end{center}
       
        
        From a design standpoint, this means we can break out different
        parts of programs into their own sections. Each function has
        a name, which should be used to label what the purpose of its
        code is.
        
        \begin{center}
            \includegraphics[width=12cm]{images/programbreakout.png}
        \end{center}
        

    \section{Function Basics} %------------------------------------%
    
        \subsection{Uses of functions}
    
        \paragraph{Functions for formulas} ~\\
        In algebra, you've seen functions like this:
        
        $$ f(x) = 2x + 3 $$
        
        If you were like me in algebra, you might have thought to yourself,
        ``That's the same as $y = 2x + 3$, why did we replace $y$ with $f(x)$?''
        
        The reason is that we want to write a function in terms of its \textbf{input}, $x$,
        and its \textbf{output} $f(x)$. The equation $2x + 3$ is the \textbf{function body},
        which specifies how we get some output given some input.
        ~\\
        
        We can use functions in programming like with math as well - defining
        formulas that we might need a lot - but there's more to functions than
        just making computations.
    
        Let's say we want to make our ``GetArea'' function in math. We would need
        two inputs: Width and Length, and the output would end up being the area,
        so maybe we would name the function $A$. It might look like this:
        
        $$ A( w, l ) = w \cdot l $$
        
        In C++, it would look like this:
\begin{lstlisting}[style=code]
float Area( float width, float length )
{
    return width * length;
}
\end{lstlisting}
        \footnotesize
        (Luckily we don't have to restrict ourselves to single-letter variable
        and function names in C++ :)
        
        \normalsize
        
        \paragraph{Functions to validate} ~\\       
        Functions can also be handy in validating data and user input,
        putting the validation in one place instead of having to re-implement
        the same checks over and over.
        
        For example, let's say we want to keep some variable percentage
        in our program betwee 0\% and 100\% - no negative values, nothing over 100\%.
        We could implement a function that takes the percentage as an \textbf{input},
        and either returns the same percentage, or 0\%, or 100\% as \textbf{output}...
        
\begin{lstlisting}[style=code]
int BoundPercent( int originalPercent )
{
    if ( originalPercent < 0 )
    {
        return 0;
    }
    else if ( originalPercent > 100 )
    {
        return 100;
    }
    else
    {
        return originalPercent;
    }
}
\end{lstlisting}

    Then, \textbf{anywhere in the program}, we could use this function
    to make sure our percentages are in the right range...
        
\begin{lstlisting}[style=code]
// in main()
hungerPercent       = BoundPercent( hungerPercent );
healthPercent       = BoundPercent( healthPercent );
happinessPercent    = BoundPercent( happinessPercent );
\end{lstlisting}

    \begin{center}
        \includegraphics[width=5cm]{images/nocomp.png}
    \end{center}
        
    \newpage
    \paragraph{Functions to get input} ~\\
    Getting user input is a common part of writing software,
    and we will usually need to validate what the user is entering
    \textit{prior} to doing any operations on it. Let's say your
    program is full of numbered menus and you want to validate the user's
    menu choices easily. You can use a function with a while loop in it:
    
\begin{lstlisting}[style=code]
int GetUserInput( int min, int max )
{
    int choice;
    cout << "Choice: ";
    cin >> choice;
    
    while ( choice < min || choice > max )
    {
        cout << "Invalid choice, try again: ";
        cin >> choice;
    }

    return choice;
}
\end{lstlisting}

    You write this function once and then you can reuse it in your entire
    program for all menus...

\begin{lstlisting}[style=code]
cout << "1. Deposit money" << endl 
     << "2. Withdraw money" << endl
     << "3. View balance" << endl;
     
choice = GetUserInput( 1, 3 ); // 1, 2, or 3

if ( choice == 1 )
{
    cout << "1. Checking account" << endl
         << "2. Savings account" << endl;
         
    choice = GetUserInput( 1, 2 ); // 1 or 2
}
\end{lstlisting}

        \newpage
        \paragraph{Functions to output} ~\\
        Functions aren't required to return data. Sometimes, you just
        want a function that is responsible for formatting \textbf{output}
        or displaying a menu. In this case, a function's return type
        can be \texttt{void}.
        
\begin{lstlisting}[style=code]
void DisplayStudentInfo( string name, float gpa )
{
    cout << "Name:   " << name
         << "GPA:    " << gpa << endl;
}
\end{lstlisting}

    You also aren't required to pass \textbf{input} to functions.
    In this case, the parameter list between ( ) remains empty, but the
    () is always required for functions:

\begin{lstlisting}[style=code]
void DisplayMainMenu()
{
    cout << "1. Deposit" << endl;
    cout << "2. Withdraw" << endl;
    cout << "3. View Balance" << endl;
    cout << "4. Log Out" << endl;
}
\end{lstlisting}
        
    \paragraph{Other uses} ~\\
    These are just some examples of why you might use functions in your programs.
    Anything you write in \texttt{main()} can go inside of a different function
    as well - it's just another tool for designing \textbf{clean}, \textbf{maintanable},
    and \textbf{readable} code.
    
    \begin{center}
        \includegraphics[width=12cm]{images/badcode.png}
    \end{center}
        
        \newpage
        \subsection{Anatomy of a function}
        
            \subsubsection*{Defining a function}
            
            Before you start using a function, you have to declare
            and define it...
        
            \paragraph{Function Declaration}
            
            A function declaration is similar to a variable declaration -
            we tell the program ``hey, we want to use this function,
            here's its name and some info about it.''
            Declaration statements end with a semi-colon and don't
            contain a code block (the function body) because it's
            \textit{just} a declaration.
            
\begin{lstlisting}[style=code]
float GetArea( float width, float height );
\end{lstlisting}
            
            \paragraph{Function Definition}
            
            The function definition is where we actually write \textit{what the function does}.
            It includes the same information as the function declaration,
            but we include the function body in a code block.

\begin{lstlisting}[style=code]
float GetArea( float width, float height )
{
    return width * height;
}
\end{lstlisting}
            
            \paragraph{Function Header}
            
            The function header is the first line of a function,
            which includes the following information: \textbf{return type},
            \textbf{function name}, and \textbf{parameter list}.
            
            \paragraph{Function Parameters}
            
            The parameters of a function are a list of \textbf{input variables}
            that are expected to be passed into the function from elseware.
            The parameter list is located between the ( and ) in the function header.
            
\begin{lstlisting}[style=code]
( float width, float height )
\end{lstlisting}
            
            \paragraph{Function Return Type}
            
            The \textbf{return type} of a function specifies \textbf{what kind of data}
            is returned from this function. The return type can be \textbf{any data type},
            or it can be \textbf{void} if nothing is going to be returned.
            
            \paragraph{Function Body}
            
            The function body is the code block, written between \{ and \},
            defining what logic the function performs.
            
            \newpage
            \subsubsection*{Calling a function}
            
            Once the function has been defined, you can call it from
            anywhere in your program...
                        
\begin{lstlisting}[style=code]
int main()
{
    float width, height;
    cout << "Enter width and height: ";
    cin >> width >> height;
    
    // Call GetArea
    float area = GetArea( width, height );
    
    cout << "Area: " << area << endl;
}
\end{lstlisting}
            
            \paragraph{Function Call}
            
            Calling a function requires the \textbf{function name},
            and passing in a series of \textbf{inputs} that become
            the function's parameters. In the example above,
            the \texttt{GetArea} function is called, with the values
            from the \texttt{width} and \texttt{height} variables being
            passed in. Once \texttt{GetArea} returns its \textbf{output},
            that output is then stored in the \texttt{area} variable.
            
            \paragraph{Function Arguments}
            
            An \textbf{argument} is the name of the value or variables
            being passed into the function during the \textbf{function call}.
            These arguments become the values that the \textbf{function parameters}
            within the \textbf{function definition} uses.
                
            ~\\ Here in the function call, 10 and 20 are the arguments:
\begin{lstlisting}[style=code]
// Call GetArea
float area = GetArea( 10, 20 );
\end{lstlisting}

            ~\\ So the values of 10 and 20 get used as the width and length parameters' values:
\begin{lstlisting}[style=code]
float GetArea( float width, float height )
{
    return width * height;
}
\end{lstlisting}

    ~\\~\\
    The arguments of a function call can be \textbf{hard-coded values},
    like 10 and 20 above, or you can pass in other variables as arguments.
    Then, whatever is stored in those variables is copied over to the parameters.
    
\begin{lstlisting}[style=code]
// Call GetArea twice
float area1 = GetArea( room1Width, room1Length );
float area2 = GetArea( room2Width, room2Length );
\end{lstlisting}

    The arguments passed in \textbf{do not need to share a name with the parameters};
    these are not the same variables. They're only sharing the data stored within them.
    
    The first time \texttt{GetArea} is called, whatever is stored within \texttt{room1Width}
    is copied from that variable and stored in the parameter variable \texttt{width}
    within the function's definition.
    
    \paragraph{Common syntax errors with function calls} ~\\
    
    When calling a function...
    
    \begin{itemize}
        \item   \underline{Do not} put the parameter \textbf{data types}
                with the arguments. ~\\
                \color{red}
                \xmark \texttt{float area = GetArea(float room1Width, float room1Length);}
                \color{black}
                \checkmark \texttt{float area = GetArea( room1Width, room1Length );}
                
        \item   \underline{Always} put parentheses ( ) after the function name,
                even when the function has no parameters. ~\\
                \color{red}
                \xmark \texttt{DisplayMenu;}
                \color{black} ~\\
                \checkmark \texttt{DisplayMenu();}
                
        \item   Remember that if a function returns data, you need to \underline{store} its return value in a variable.
                ~\\
                \color{red}
                \xmark \texttt{GetArea( 10, 20 );   // The return value is lost}
                \color{black} ~\\
                \checkmark \texttt{area = GetArea( 10, 20 ); // Storing the return value }
    \end{itemize}
    
    
    \section{Variable scope}  %------------------------------------%
        
        \textbf{Scope} refers to \textbf{the location} in the code where
        a variable exists. 
        
        \paragraph{main():} If you declare a variable at the top of the
        \texttt{main()} function, not inside any if statements or loops,
        then that variable is \textbf{in scope} for the entire duration of
        \texttt{main()}, starting with the line it was declared on.
        
\begin{lstlisting}[style=code]
int main()
{
    int a;
    // ...etc...
}
\end{lstlisting}
        
        \paragraph{Variables declared within if statements and loops:}
        Variables declared \textit{within} the code block of an if statement
        or a loop only exists within that code block.
        
\begin{lstlisting}[style=code]
if ( a == 3 )
{
    int b;  // only exists within this block
}

for ( int i = 0; i < 10; i++ )
{
    cout << i; // only exists within this block
}
\end{lstlisting}

        If you try to use these variables somewhere below the code block,
        your compiler will give an error, stating that the variable does not
        exist in that scope. Once the program leaves the code block,
        the variable is \textbf{out of scope}.
        
        \paragraph{Functions:} Remember that \texttt{main()} is a function,
        just like any other functions we write in our program. Any variables
        declared within a function are \textbf{local to that function},
        and accessible anywhere from the variable's declaration until the
        end of the function.
    
\begin{lstlisting}[style=code]
int GetChoice()
{
    int choice; // local variable
    cout << "Choice: ";
    cin >> choice;
    return choice;
}
\end{lstlisting}
        
        \paragraph{Parameters:} Variables declared in the \textbf{parameter list}
        of a function are also local to that function and can be used anywhere
        within the function.
        
\begin{lstlisting}[style=code]
int Sum( int a, int b, int c )
{
    // a, b, and c are local to this function.
    return a + b + c;
}
\end{lstlisting}

        \paragraph{Same names?} ~\\
        The same \textbf{name} can be reused for different variables
        in \textbf{different scopes}. Even if they share the same name,
        they are not related in any way.
        
\begin{lstlisting}[style=code]
int GetChoice()
{
    int choice;     // Variable A
    cout << "Choice: ";
    cin >> choice;
    return choice;
}

int main()
{
    int choice;     // Variable B
    choice = GetChoice();
}
\end{lstlisting}

    \section{Parameters and arguments}  %------------------------------------%
        
        \subsection{Pass-by-value}
        When we have a function declared with a parameter...

\begin{lstlisting}[style=code]
void Example( int someNumber )
{
    cout << someNumber;
}
\end{lstlisting}

        ...and we call that function elseware, passing in another variable as an argument...

\begin{lstlisting}[style=code]
int main()
{
    int myNumber = 2;
    Example( myNumber );
    return 0;
}
\end{lstlisting}
    
        ... What happens is that the \textbf{value} of the variable \texttt{myNumber}
        is copied and passed to the function parameter \texttt{someNumber}.
        This works the same as if you simply pass in \texttt{Example( 10 )},
        passing in a hard-coded value instead of using a variable.
        ~\\
        
        If you wrote the function to change the value of its parameter,
        that change would only be reflected \textbf{within the function}
        and would \textit{not} affect the original argument passed as part of the function call.

\begin{lstlisting}[style=code]
void Example( int someNumber )
{
    cout << "Example begin: " << someNumber << endl;
    someNumber = 100;
    cout << "Example end: " << someNumber << endl;
}

int main()
{
    int myNumber = 2;

    Example( myNumber );
    
    cout << "main end: " << myNumber << endl;
    
    return 0;
}
\end{lstlisting}

        The output of this program would be:
        
\begin{lstlisting}[style=output]
Example begin: 2
Example end: 100
main end: 2
\end{lstlisting}

        Even though the value of \texttt{someNumber} from the \texttt{Example}
        function changes (which is valid), that change doesn't affect \texttt{myNumber}
        within \texttt{main()}, because \textbf{only the value was copied over}.
        
        ~\\
        This is known as \textbf{pass-by-value}.
        
        \newpage
        \subsection{Pass-by-reference}
        
        If we wanted to change the value of an argument variable within
        a function, we'd have to change the parameter to \textbf{pass-by-reference}.
        To do this, we use the symbol \texttt{\&} in the parameter's declaration,
        after the data type and before the variable name.

\begin{lstlisting}[style=code]
void Example( int& someNumber )
{
    cout << "Example begin: " << someNumber << endl;
    someNumber = 100;
    cout << "Example end: " << someNumber << endl;
}
\end{lstlisting}

        The ampersand symbol can go next to the data type (\texttt{int\& blah}),
        next to the variable name (\texttt{int \&blah}), or separate from both
        (\texttt{int \& blah}). ~\\
        
        Once we've made the parameter a \textbf{reference}, then when the
        function is called, the argument is \underline{not copied} - 
        a \textit{reference} to that variable is passed to the function.
        Any changes to the reference parameter in the function also 
        affects the original argument variable.

\begin{lstlisting}[style=code]
int main()
{
    int myNumber = 2;

    // Calling it looks the same as before
    Example( myNumber );
    
    cout << "main end: " << myNumber << endl;
    
    return 0;
}
\end{lstlisting}

        The output of this program would be:
        
\begin{lstlisting}[style=output]
Example begin: 2
Example end: 100
main end: 100
\end{lstlisting}
        
        \newpage
        \paragraph{Pass-by-reference instead of return} ~\\
        In some cases, you may need to return multiple pieces of data
        from a function - however, you can only \textbf{return one item}
        from a function with the \texttt{return} statement (in C++).
        One option is to set the information you want ``returned'' as
        pass-by-reference parameters of the function.

\begin{lstlisting}[style=code]
void DoubleTheseNumbers( int & a, int & b, int & c )
{
    a *= 2;
    b *= 2;
    c *= 2;
}
\end{lstlisting}

        \paragraph{Pass-by-reference of large things} ~\\
        We haven't gone over \textbf{arrays} or \textbf{classes/objects} yet, 
        but another reason we might want to pass something by-reference
        instead of by-value is when the parameter is big (which
        can be the case with arrays and objects). ~\\
        
        If we have a large object we need to pass to a function,
        doing a copy of the entire thing is inefficient - it is much
        simpler and faster to pass the large object by-reference.
        
        \begin{center}
            \includegraphics[width=12cm]{images/copyparameter.png}
        \end{center}

        \newpage
        \subsection{Default parameters}
        
        When declaring a function, you can also set \textbf{default parameters}.
        These are the default values assigned to the parameters if the user
        doesn't pass anything in. The default parameters are
        \textbf{only specified in a function declaration} - NOT the definition!
        
        ~\\ In this example, it could be a function that displays ingredients
        for a recipe, and by default the batch is set to 1.0 (one batch).
\begin{lstlisting}[style=code]
void OutputIngredients( float eggs, float sugar, float flour, float batch = 1.0 );
\end{lstlisting}

        ~\\ The function could be called without passing in a batch:
\begin{lstlisting}[style=code]
cout << "Ingredients:" << endl;
OutputIngredients( 1, 2.0, 3.5 );
\end{lstlisting}

        ~\\ Or they could pass a batch amount explicitly:
\begin{lstlisting}[style=code]
cout << "Ingredients:" << endl;
OutputIngredients( 1, 2.0, 3.5, 0.5 );  // half batch
\end{lstlisting}
        ~\\
        
        You can have multiple \textbf{default parameters}
        specified in your function declaration - but all variables
        with default values must go \textit{after} any variables
        \textit{without default values}.

    \newpage
    \subsection{Summary: Ways to pass data to/from functions}
    
    The following table illustrates different ways we can define our
    parameters, and what the goal is. ``RT'' means ``Return Type'',
    ``T'' is the ``Type'' of the parameter.
    
    \begin{center}
        \begin{tabular}{l c c p{5cm}}
            \textbf{Function} & \textbf{Read} & \textbf{Return} & \textbf{Info}
            \\
            \texttt{RT func( T X )} & \checkmark & \xmark
            & \small \texttt{X} is pass-by-value, which is fine for primitive data types
            like ints, floats, chars, bools.
            \\ \\
            \texttt{RT func( const T\& X )} & \checkmark & \xmark
            & \small \texttt{X} is passed by const-reference because \texttt{X}
            is a more sophisticated data type like a string or other
            class-based object (longer time to copy if passed by-value).
            \\ \\
            \texttt{RT func( T\& X )} & \checkmark & \checkmark
            & \small The function can read from \texttt{X} but also overwrite
            its value and the change will be reflected back to the
            argument being passed to the function call.
        \end{tabular}
    \end{center}




    \section{Function Overloading} %------------------------------------%
   
   In C++, you can also write multiple functions that have the
   \textbf{same name}, but a different \textbf{parameter list}.
   This is known as \textbf{function overloading}.
   ~\\
   
   Let's say you want to be able to sum numbers, and you make a version
   for floats and a version for integers:
   
\begin{lstlisting}[style=code]
int Sum( int a, int b )
{
    return a + b;
}

float Sum( float a, float b )
{
    return a + b;
}
\end{lstlisting}
    
    You can write as many versions of the function as you like, so long
    as the function headers are \textbf{uniquely identifiable} to the compiler,
    which means:
    
    \begin{itemize}
        \item   The functions have a different \textbf{amount} of parameters, or
        \item   The \textbf{data types} of the parameters are different, or
        \item   The parameters are in a \textbf{different order} (when mixing data types).
    \end{itemize}
    
    These will become much more useful once we cover classes and objects.
    
