\contentsline {chapter}{\numberline {1}Introduction to Command Line computing}{3}
\contentsline {paragraph}{What is a Command Line?}{3}
\contentsline {paragraph}{Where is the Command Line used?}{4}
\contentsline {chapter}{\numberline {2}Accessing a UNIX/Linux terminal}{5}
\contentsline {section}{\numberline {2.1}PuTTY on Windows}{5}
\contentsline {chapter}{\numberline {3}Using UNIX/Linux from the command line}{6}
\contentsline {section}{\numberline {3.1}Running programs and program flags}{6}
\contentsline {section}{\numberline {3.2}Navigating the file system}{7}
\contentsline {subsection}{\numberline {3.2.1}ls - List directory contents}{7}
\contentsline {paragraph}{Some flags:}{7}
\contentsline {paragraph}{Manual page:}{7}
\contentsline {subsection}{\numberline {3.2.2}pwd - Present working directory}{7}
\contentsline {paragraph}{Manual page:}{8}
\contentsline {subsection}{\numberline {3.2.3}cd - Change directory}{8}
\contentsline {paragraph}{Manual page:}{8}
\contentsline {subsection}{\numberline {3.2.4}cp - Copy files/directories}{8}
\contentsline {paragraph}{Manual page:}{8}
\contentsline {subsection}{\numberline {3.2.5}mv - Move files/directories}{8}
\contentsline {paragraph}{Manual page:}{8}
\contentsline {subsection}{\numberline {3.2.6}rm - Remove files/directories}{8}
\contentsline {paragraph}{Manual page:}{8}
\contentsline {subsection}{\numberline {3.2.7}mkdir - Make directory}{8}
\contentsline {paragraph}{Manual page:}{8}
\contentsline {subsection}{\numberline {3.2.8}ln - Make link}{8}
\contentsline {paragraph}{Manual page:}{8}
\contentsline {subsection}{\numberline {3.2.9}chmod - Change file mode bits}{8}
\contentsline {paragraph}{Manual page:}{8}
\contentsline {section}{\numberline {3.3}Other handy programs}{9}
\contentsline {subsection}{\numberline {3.3.1}man - Manual}{9}
\contentsline {paragraph}{Manual page:}{9}
\contentsline {subsection}{\numberline {3.3.2}ps - Report a snapshot of current processes}{9}
\contentsline {paragraph}{Some flags:}{9}
\contentsline {paragraph}{Manual page:}{10}
\contentsline {subsection}{\numberline {3.3.3}find - Find files in a directory hierarchy}{10}
\contentsline {paragraph}{Some flags:}{10}
\contentsline {paragraph}{Manual page:}{11}
\contentsline {subsection}{\numberline {3.3.4}grep - Find text/pattern}{11}
\contentsline {paragraph}{Form:}{11}
\contentsline {paragraph}{Some flags:}{11}
\contentsline {paragraph}{Manual page:}{11}
\contentsline {subsection}{\numberline {3.3.5}wc - Print newline, word, and byte counts for each file}{12}
\contentsline {paragraph}{Some flags:}{12}
\contentsline {paragraph}{Manual page:}{12}
\contentsline {subsection}{\numberline {3.3.6}sleep - Delay for an amount of time}{12}
\contentsline {paragraph}{Manual page:}{12}
\contentsline {subsection}{\numberline {3.3.7}date - Print or set system date and tieme}{13}
\contentsline {paragraph}{Manual page:}{13}
\contentsline {subsection}{\numberline {3.3.8}printf - Format and print data}{13}
\contentsline {paragraph}{Some Placeholders:}{13}
\contentsline {paragraph}{Manual page:}{13}
\contentsline {section}{\numberline {3.4}Stringing together commands}{14}
\contentsline {subsection}{\numberline {3.4.1}Pipe program-A's output to program-B's input $|$}{14}
\contentsline {paragraph}{Example 1: Finding all .cpp files}{14}
\contentsline {paragraph}{Example 2: Getting processes that a certain user started}{14}
\contentsline {subsection}{\numberline {3.4.2}Redirecting output $>$}{15}
\contentsline {chapter}{\numberline {4}Command Line Text Editors}{17}
\contentsline {subsection}{\numberline {4.0.1}nano}{17}
\contentsline {subsection}{\numberline {4.0.2}vi/vim}{17}
\contentsline {subsection}{\numberline {4.0.3}emacs}{17}
\contentsline {chapter}{\numberline {5}Writing scripts - basic shell programming}{18}
\contentsline {section}{\numberline {5.1}Input and Output}{18}
\contentsline {chapter}{\numberline {6}Using \texttt {awk} for pattern processing}{19}
