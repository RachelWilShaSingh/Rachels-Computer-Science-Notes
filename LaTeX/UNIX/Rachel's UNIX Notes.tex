\documentclass[a4paper,12pt,oneside]{book}
\usepackage[utf8]{inputenc}

\newcommand{\laTopic}       {}
\newcommand{\laTitle}       {Rachel's Linux/UNIX Notes}
\newcounter{question}

\renewcommand{\chaptername}{Topic}

\usepackage{../rachwidgets}
\usepackage{../rachdiagrams}

\title{}
\author{Rachel Singh}
\date{\today}

\pagestyle{fancy}
\fancyhf{}

\lhead{\laTitle}

\chead{}

\rhead{\thepage}

\rfoot{\tiny \thepage\ of \pageref{LastPage}}

\lfoot{\tiny Rachel Singh, last updated \today}

\renewcommand{\headrulewidth}{2pt}
\renewcommand{\footrulewidth}{1pt}

\begin{document}

    \begin{titlepage}
        \centering{}

        \sffamily{
            \textbf{
                {\fontsize{1cm}{2cm}\selectfont Rachel's}~\\~\\
                {\fontsize{3cm}{3cm}\selectfont Linux/UNIX}~\\~\\
                {\fontsize{2cm}{3cm}\selectfont Notes}
            }
        }

        \begin{figure}[h]
            \begin{center}
                \includegraphics[width=12cm]{images/title-image.png}
            \end{center}
        \end{figure}

        \sffamily{
            \textbf{
                A concise overview of topics by Rachel Singh
            }
        }
        
        \vspace{1cm} \small
        This work is licensed under a \\ Creative Commons Attribution 4.0 International License. ~\\~\\
        \includegraphics{images/cc-by-88x31.png}
        ~\\~\\
        
        Last updated \today

    \end{titlepage}


\tableofcontents

\chapter{Introduction to Command Line computing}

    \paragraph{What is a Command Line?}
    
        \begin{center}
            \includegraphics{images/terminal-window-example.png}
        \end{center}
        
        A \textbf{command line} (aka \textbf{terminal}, \textbf{console}, or \textbf{shell})
        is a text-based interface with your computer.        
        Most average users are used to using \textbf{Windows} or \textbf{MacOS}
        on their computers exclusively from the \textbf{Graphical User Interface (GUI)},
        however, text terminals are available in both of these operating systems.        
        UNIX and Linux machines also have \textbf{GUI}s, but the terminal is maybe
        more ``visible'' as the assumption is that the average Linux/UNIX user would
        use it at least for some things.
        
        ~\\
        Programs that run in the command line and don't have a graphical user interface
        instead have a \textbf{Command Line Interface (CLI)}.
    
    \paragraph{Where is the Command Line used?}
    
        If you're a millennial or older, you may have used the \textbf{DOS prompt}
        on computers in the 90s to launch games and run other programs before
        Windows was as mainstream.
        Even though Windows has moved away from a design where the average user
        needs a terminal, many computer professionals still need to use
        the terminal for various features, or ``power-users'' might write
        terminal scripts to automate jobs.
        
        Examples of when a terminal might be used in the real world:
        
        \begin{itemize}
            \item   An IT admin setting up a lot of computers. They can write
                    an \textbf{automated script} to configure multiple systems at once,
                    instead of having to manually go through the GUI to set options.
            \item   A programmer \textbf{tunnelling} into a UNIX machine at work
                    to kick off a build of their software for UNIX/Linux systems.
                    They might be working on a Windows or Mac computer, but you
                    can access other computers remotely, and it is lower-bandwidth
                    to do so from a CLI than a GUI.
            \item   A web developer uploading website files to a Linux server using
                    \textbf{SCP} (secure copy) to copy files from their machine
                    to the server.
            \item   A gamer setting up a Minecraft server or a server for another
                    multiplayer video game, where multiple players (clients) can connect
                    remotely.
        \end{itemize}
        
    \chapter{Accessing a UNIX/Linux terminal}
    
        \section{PuTTY on Windows}

    \chapter{Using UNIX/Linux from the command line}
    
        \section{Running programs and program flags}
            
            CTRL-X
            
            Home directory is \~, filesystem layout
            
            . is current folder
            
            .. is previous folder
            
            
    
    \newpage
        \section{Navigating the file system}
        
            % -------------------------------------------------------- %
            \subsection{ls - List directory contents}
                The \texttt{ls} command is used to list all the contents of a directory.
                Typing it by itself will give you a simple list:

            ~\\ \texttt{user@user:\~\$ ls}
\begin{lstlisting}[style=output]
backup.cpp  school        work
photos      todo.txt
projects    webserver.sh
\end{lstlisting}
                
                \paragraph{Some flags:} ~\\
                
                \begin{tabular}{l p{10cm}}
                    \texttt{ -l }: & Long list format \\
                    \texttt{ -h }: & Human-readable format (print file sizes in KB, MB, GB instead of bytes) \\
                    \texttt{ -r }: & Recurse through subfolders \\
                    \texttt{ -X }: & Sort alphabetically by extension
                \end{tabular}
                ~\\
                
            ~\\ \texttt{user@user:\~\$ ls -lh}
\begin{lstlisting}[style=output]
total 16K
-rw-rw-r-- 1 wilsha wilsha    0 Mar 12 11:14 backup.cpp
drwxrwxr-x 2 wilsha wilsha 4.0K Mar 12 11:13 photos
drwxrwxr-x 2 wilsha wilsha 4.0K Mar 12 11:13 projects
drwxrwxr-x 2 wilsha wilsha 4.0K Mar 12 11:13 school
-rw-rw-r-- 1 wilsha wilsha    0 Mar 12 11:13 todo.txt
-rw-rw-r-- 1 wilsha wilsha    0 Mar 12 11:14 wserver.sh
drwxrwxr-x 2 wilsha wilsha 4.0K Mar 12 11:13 work
\end{lstlisting}

                
                \paragraph{Manual page:} \texttt{https://linux.die.net/man/1/ls}
            
            % -------------------------------------------------------- %
            \subsection{pwd - Present working directory}
            
                Display your current working directory (what folder you're currently in).
                ~\\ \texttt{user@user:\~\$ pwd}
\begin{lstlisting}[style=output]
/home/wilsha/TEACHING/LaTeX/UNIX     
\end{lstlisting}

                \paragraph{Manual page:} \texttt{https://linux.die.net/man/1/pwd}
            
            % -------------------------------------------------------- %
            \subsection{cd - Change directory}
            
                Move to a different directory. Type in the folder name after \texttt{cd},
                or type \texttt{..} to go back one folder. You can also string folders together.
                
                ~\\
                \begin{tabular}{l p{10cm}}
                    \texttt{cd school} & Navigate into the school folder, within the current folder. \\
                    \texttt{cd school/cs211} & Navigate into the subfolder school, then its subfolder cs211. \\
                    \texttt{cd ..} & Navigate backwards into the directory that contains the folder you're currently in. \\
                    \texttt{cd ../..} & Go back two directories \\
                    \texttt{cd \~} & Go back to the home directory (e.g., /home/YOURNAME/)
                \end{tabular}
                
                \paragraph{Manual page:} \texttt{https://linux.die.net/man/1/cd}
                
            \subsection{cp - Copy files/directories}
            
                \paragraph{Manual page:} \texttt{https://linux.die.net/man/1/cp}
                
            % -------------------------------------------------------- %
            \subsection{mv - Move files/directories}
            
                \paragraph{Manual page:} \texttt{https://linux.die.net/man/1/mv}
            % -------------------------------------------------------- %
            \subsection{rm - Remove files/directories}
            
                \paragraph{Manual page:} \texttt{https://linux.die.net/man/1/rm}
            % -------------------------------------------------------- %
            \subsection{mkdir - Make directory}
            
                \paragraph{Manual page:} \texttt{https://linux.die.net/man/1/mkdir}
            % -------------------------------------------------------- %
            \subsection{ln - Make link}
            
                \paragraph{Manual page:} \texttt{https://linux.die.net/man/1/ln}
            % -------------------------------------------------------- %
            \subsection{chmod - Change file mode bits}
            
                \paragraph{Manual page:} \texttt{https://linux.die.net/man/1/chmod}
        
        \section{Other handy programs}
        
            % -------------------------------------------------------- %
            \subsection{man - Manual}
            
                \paragraph{Manual page:} \texttt{https://linux.die.net/man/1/man}
            % -------------------------------------------------------- %
            \subsection{ps - Report a snapshot of current processes}
            
                Returns a snapshot of processes currently running on the system.
                
                ~\\ \texttt{user@user:\~\$ ps}
\begin{lstlisting}[style=output]    
  PID TTY          TIME CMD
 7040 pts/1    00:00:00 bash
 8206 pts/1    00:00:00 ps  
\end{lstlisting}

                \paragraph{Some flags:} ~\\
                
                \begin{tabular}{l p{10cm}}
                    \texttt{ -e } or \texttt{ -A }: & Select all processes \\
                    \texttt{ -f }: & Full-format listing
                \end{tabular}
                ~\\
                
                Adding the \texttt{-f} flag gives more columns:
                
                ~\\ \texttt{user@user:\~\$ ps -f}
\begin{lstlisting}[style=output]    
UID        PID  PPID  C STIME TTY          TIME CMD
wilsha    8260  8254  0 11:56 pts/2    00:00:00 bash
wilsha    8295  8260  0 11:57 pts/2    00:00:00 ps -f 
\end{lstlisting}

                ~\\ Adding the \texttt{-e} flag will select all processes; not just ones that the current user started:
                
                ~\\ \texttt{user@user:\~\$ ps -ef}
\begin{lstlisting}[style=output]    
UID        PID  PPID  C STIME TTY        TIME CMD
root         1     0  0 10:04 ?      00:00:01 /sbin/init splash
root         2     0  0 10:04 ?      00:00:00 [kthreadd]
(...)
wilsha    8254     1  1 11:56 ?      00:00:01 mate-terminal
wilsha    8260  8254  0 11:56 pts/2  00:00:00 bash
root      8293     2  0 11:57 ?      00:00:00 [kworker/u24:1-e]
wilsha    8343  8260  0 11:58 pts/2  00:00:00 ps -ef
\end{lstlisting}
            
                \paragraph{Manual page:} \texttt{https://linux.die.net/man/1/ps}
            % -------------------------------------------------------- %
            \subsection{find - Find files in a directory hierarchy}
            
                To search for files that match some filename (or other criteria), you can use the \texttt{find} command.
                
                ~\\ Find all files named ``backup.cpp'' in this folder and all sub-folders:
                ~\\ \texttt{user@user:\~\$ find . -name "backup.cpp"}
\begin{lstlisting}[style=output]    
./TEACHING/Computer-Science-Notes/LaTeX/UNIX/backup.cpp
./backup.cpp 
\end{lstlisting}

                ~\\ Find all .java files in this folder and all sub-folders:
                ~\\ \texttt{user@user:\~\$ find . -name "*.java"}
\begin{lstlisting}[style=output]    
./TEACHING/EXAMPLE_CODE/Word.java
./TEACHING/EXAMPLE_CODE/RunningTotal.java
./TEACHING/EXAMPLE_CODE/NumberGuesser.java
./TEACHING/EXAMPLE_CODE/Bank.java
\end{lstlisting}
                
                \paragraph{Some flags:} ~\\
                
                \begin{tabular}{l p{10cm}}
                    \texttt{ -atime \textit{n} }: & Files that were last \textbf{accessed} $n \cdot 24$ hours ago \\
                    \texttt{ -amin \textit{n} }: & Files that were last \textbf{accessed} $n$ minutes ago \\
                    \texttt{ -ctime \textit{n} }: & Files \textbf{created} $n \cdot 24$ hours ago \\
                    \texttt{ -cmin \textit{n} }: & Files \textbf{created} $n$ minutes ago \\
                    \texttt{ -mtime \textit{n} }: & Files \textbf{modified} $n \cdot 24$ hours ago \\
                    \texttt{ -mmin \textit{n} }: & Files \textbf{modified} $n$ minutes ago \\
                    \texttt{ -size \textit{n}[cwbkMG] }: & Files using $n$ units of space (c = bytes, M = Megabytes, G = Gigabytes) \\
                    \texttt{ -user \textit{uname} }: & Files owned by \textit{uname}.
                \end{tabular}
                ~\\
                
                ~\\ Find files modified in the last half hour (30 minutes):
                ~\\ \texttt{user@user:\~\$ find . -mmin -30}
\begin{lstlisting}[style=output]  
./Rachel's UNIX Notes.toc
./Rachel's UNIX Notes.aux
./Rachel's UNIX Notes.tex
./Rachel's UNIX Notes.pdf
./Rachel's UNIX Notes.log
./images
./images/redirectstdout.png
./fs
./fs/test.cpp
./fs/project.cpp
./fs/process.txt

\end{lstlisting}
            
                \paragraph{Manual page:} \texttt{https://linux.die.net/man/1/find}
            % -------------------------------------------------------- %
            \subsection{grep - Find text/pattern}
            
                \paragraph{Form:}   ~\\
                
                \begin{itemize}
                    \item   \texttt{grep [OPTIONS] PATTERN [FILE...]}
                    \item   \texttt{grep [OPTIONS] [-e PATTERN | -f FILE] [FILE...] }
                \end{itemize}
            
                grep can be used to search within files or other inputs
                for a matching string or expression pattern.
                
                
                ~\\ Search for "Rachel" within files in current directory (denoted  by ``.''),
                list line number (\texttt{-n}) and recurse into subfolders (\texttt{-r}):
                ~\\ \texttt{user@user:\~\$ grep -nr "Rachel" .}
\begin{lstlisting}[style=output]    
./webserver.sh:1:# Connect to Rachel's server
./todo.txt:1:Rachel's to do list
./backup.cpp:1:// Rachel
\end{lstlisting}
                
                \paragraph{Some flags:} ~\\
                
                \begin{tabular}{l p{10cm}}
                    \texttt{ -r }: & Search recursively \\
                    \texttt{ -n }: & Show line number in file
                \end{tabular}
                ~\\
            
                \paragraph{Manual page:} \texttt{https://linux.die.net/man/1/grep}
            % -------------------------------------------------------- %
            \subsection{wc - Print newline, word, and byte counts for each file}
            
                You can use this to count the amount of newlines in an output,
                which can be handy to just get the amount of results from a grep or find.
                
                \paragraph{Some flags:} ~\\
                
                \begin{tabular}{l p{10cm}}
                    \texttt{ -c }: & Print byte counts \\
                    \texttt{ -m }: & Print character counts \\
                    \texttt{ -l }: & Print newline counts \\
                    \texttt{ -w }: & Print word counts
                \end{tabular}
                ~\\
                
                ~\\ Just finding files with .cpp extensions:
                ~\\ \texttt{user@user:\~\$ find . -name "*.cpp"}
\begin{lstlisting}[style=output]    
./fs/test.cpp
./fs/project.cpp
./fs/backup.cpp
\end{lstlisting}

                ~\\ Getting the total amount of .cpp files:
                ~\\ \texttt{user@user:\~\$ find . -name "*.cpp" | wc -l}
\begin{lstlisting}[style=output]    
3
\end{lstlisting}
            
                \paragraph{Manual page:} \texttt{https://linux.die.net/man/1/wc}
            % -------------------------------------------------------- %
            \subsection{sleep - Delay for an amount of time}
            
                Perhaps when writing a script you want a delay to occur before continuing
                with the next command. You can do this with the \texttt{sleep} command
                and by passing in amount of seconds, minutes, or hours.
                
                ~\\
                \begin{tabular}{l l}
                    Sleep for 10 seconds: & \texttt{user@user:\~\$ sleep 10s}
                    \\
                    Sleep for 10 minutes: & \texttt{user@user:\~\$ sleep 10m}
                    \\
                    Sleep for 10 hours: & \texttt{user@user:\~\$ sleep 10h}
                \end{tabular}
                

            
                \paragraph{Manual page:} \texttt{https://linux.die.net/man/1/sleep}
            % -------------------------------------------------------- %
            \subsection{date - Print or set system date and tieme}
            
                \paragraph{Manual page:} \texttt{https://linux.die.net/man/1/date}
            % -------------------------------------------------------- %
            \subsection{printf - Format and print data}
            
                You can print information to the screen using \texttt{printf}.
                
\begin{verbatim}
user@user:\~\$ printf "Hi, I'm %s \n" $USER
\end{verbatim}
\begin{lstlisting}[style=output]    
Hi, I'm wilsha 
\end{lstlisting}

                You can add a string literal (e.g., ``Hi, I'm ...'') and insert in
                variables and other data by using placeholders.
                
                \paragraph{Some Placeholders:} ~\\
                
                \begin{tabular}{l p{10cm}}
                    \texttt{ \%d } and \texttt{ \%i }: & Signed integer \\
                    \texttt{ \%c }: & Character \\
                    \texttt{ \%s }: & Null-terminated string \\
                    \texttt{ \%e }: & Double (standard form) \\
                    \texttt{ \%g }: & Double (normal/exponential form) \\
                \end{tabular}
                ~\\

                \paragraph{Manual page:} \texttt{https://linux.die.net/man/1/printf}
            % -------------------------------------------------------- %
            %\subsection{top - Display tasks}
            
                %\paragraph{Manual page:} \texttt{https://linux.die.net/man/1/top}
            %% -------------------------------------------------------- %
            %\subsection{wget - Network downloader}
            
                %\paragraph{Manual page:} \texttt{https://linux.die.net/man/1/wget}
            %% -------------------------------------------------------- %
            %\subsection{lynx - Web browser}
            
                %\paragraph{Manual page:} \texttt{}
            %% -------------------------------------------------------- %
            %\subsection{convert - Convert between image formats}
            
                %\paragraph{Manual page:} \texttt{https://linux.die.net/man/1/convert}
            %% -------------------------------------------------------- %
            %\subsection{ssh - Secure Shell client}
            
                %\paragraph{Manual page:} \texttt{https://linux.die.net/man/1/ssh}
            %% -------------------------------------------------------- %
            %\subsection{scp - Secure file copy}
            
                %\paragraph{Manual page:} \texttt{https://linux.die.net/man/1/scp}
           
        \newpage
        \section{Stringing together commands}
        
            \subsection{Pipe program-A's output to program-B's input $|$}
            
                The Pipe operator allows you to take output from a first program
                (e.g., \texttt{ls} to list all files/folders in the directory)
                and pass that as input into a second program
                (e.g., using \texttt{grep} to see if any files/folders match some certain text.)
                
                \paragraph{Example 1: Finding all .cpp files} ~\\
                
                When using just \texttt{ls}, we can see a list of files:
                
                ~\\ \texttt{user@user:\~\$ ls}
\begin{lstlisting}[style=output]    
backup.cpp  photos      project.cpp  projects  
school      test.cpp    todo.txt     webserver.sh  work
\end{lstlisting}

                But we might not need all that information. Let's say
                we only wanted to see what .cpp files were in our directory.
                We could take this output from \texttt{ls}, use the \textbf{pipe} $|$
                to pass that as input to \texttt{grep} to do a search on a string...
                
                ~\\ \texttt{user@user:\~\$ ls | grep .cpp}
\begin{lstlisting}[style=output]    
backup.cpp
project.cpp
test.cpp
\end{lstlisting}

                \paragraph{Example 2: Getting processes that a certain user started} ~\\
                
                With the \texttt{ps -ef} command, we can see a full list of processes
                running on the machine:
                
                ~\\ \texttt{user@user:\~\$ ps -ef}
\begin{lstlisting}[style=output]    
UID        PID  PPID  C STIME TTY        TIME CMD
root         1     0  0 10:04 ?      00:00:01 /sbin/init splash
root         2     0  0 10:04 ?      00:00:00 [kthreadd]
(...)
wilsha    8254     1  1 11:56 ?      00:00:01 mate-terminal
wilsha    8260  8254  0 11:56 pts/2  00:00:00 bash
root      8293     2  0 11:57 ?      00:00:00 [kworker/u24:1-e]
wilsha    8343  8260  0 11:58 pts/2  00:00:00 ps -ef
\end{lstlisting}

                Let's say we wanted to get a list of all processes created by root.
                We could take our \texttt{ps -ef} output and make it the input of \texttt{grep}...
                
                ~\\ \texttt{user@user:\~\$ ps -ef | grep root}
\begin{lstlisting}[style=output]    
root  1     0  0 10:04 ?  00:00:01 /sbin/init splash
root  2     0  0 10:04 ?  00:00:00 [kthreadd]
root  3     2  0 10:04 ?  00:00:00 [rcu_gp]
root  4     2  0 10:04 ?  00:00:00 [rcu_par_gp]
root  6     2  0 10:04 ?  00:00:00 [kworker/0:0H-kb]
(... etc ...)
\end{lstlisting}

            \subsection{Redirecting output $>$}
            
                We can use the $>$ operator to take output of any program
                that outputs text and redirect it elseware, such as a text file.
                
                Let's say that we have this command to get all processes started by the root:
                \texttt{user@user:\~\$ ps -ef | grep root} \tab
                and now we want to store it in a file for easier reading. We can modify
                our command to something like this:
                
                ~\\ \texttt{user@user:\~\$ ps -ef | grep root > process.txt} ~\\
                
                There won't be any output in the terminal, such as displaying that list,
                because its output has been \textbf{redirected} to a text file. If you
                use \texttt{ls}, you'll be able to see the new process.txt file,
                and you can open it with a text editor.
                
                \begin{center}
                    \includegraphics[width=12cm]{images/redirectstdout.png}
                \end{center}
            
    \chapter{Command Line Text Editors}
    
        \subsection{nano}
        \subsection{vi/vim}
        \subsection{emacs}

    \chapter{Writing scripts - basic shell programming}
            
        \section{Input and Output}


    \chapter{Using \texttt{awk} for pattern processing}
    
        \begin{intro}{\texttt{awk} man page}
        https://linux.die.net/man/1/awk
        \end{intro}
        
        \begin{quote}
            AWK is a domain-specific language designed for text processing and typically used as a data extraction and reporting tool. It is a standard feature of most Unix-like operating systems. 
            \footnote{From https://en.wikipedia.org/wiki/AWK}
        \end{quote}
        
        
















\end{document}
