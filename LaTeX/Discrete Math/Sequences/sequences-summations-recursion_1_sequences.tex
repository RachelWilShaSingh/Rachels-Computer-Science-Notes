

    %---%
    \section{Sequences}

        Here we will be describing sequences of numbers using two different forms.
        When talking about sequences of numbers, we need to acknowledge that each
        number has a position in the sequence. The number itself is the \textbf{element}
        of the sequence, and its position is the \textbf{index}.

        \begin{center}
            Sequence given by $a_n = 2n$ ~\\~\\

            \begin{tabular}{l | l l l l }
                Element ($a_n$)   &   2 & 4 & 6 & 8
                \\ \hline
                Index ($n$)       &   1 & 2 & 3 & 4
            \end{tabular}
        \end{center}

        \paragraph{Closed formula:}
        We can specify the element's value based on its \textit{position in the list} - the \textbf{index}.

        ~\\
        Examples:

        \begin{tabular}{p{4cm} p{4cm} p{4cm}}
            $a_n = 2n + 1$ & $a_n = 2n$ & $a_n = n^2$
        \end{tabular}

        \paragraph{Recursive formula:}
        A recursive formula defines an element of the sequence based on
        one or more previous elements of the list. A recursive sequence must,
        at minimum, give the value of one element, like:

        \begin{center}
            $a_1 = 1$
        \end{center}

        and define subsequent elements based on previous items:

        \begin{center}
            $a_n = a_{n-1} + 2$
        \end{center}

        For a sequence like this:

        \begin{center}
            2, 4, 6, 8, 10
        \end{center}

        we could define the formula in either way...

        \begin{itemize}
            \item   Closed formula: $a_n = 2n$ \\
                    The value is 2 times the position $n$.
            \item   Recursive formula: $a_1 = 2$, $a_n = a_{n-1} + 2$ \\
                    The value is two more than the previous element.
        \end{itemize}

        ~\\
        Note that in most programming languages, the index in an array or list
        tends to start at the number 0 - not 1, so make sure you check the
        starting index in a recursive formula and not just assume it starts at
        $a_0$ or $a_1$.

        \subsection*{Sequences in code}

        You could write functions in your programming language of choice
        to illustrate both a closed and a recursive formula. For example:

        \paragraph{Code version of closed formula $a_n = 2n$} ~\\

\begin{lstlisting}[style=code]
int Closed( int n ) {
    return 2 * n;
}
\end{lstlisting}

        \paragraph{Code version of recursive formula $a_1 = 2$, $a_n = a_{n-1} + 2$} ~\\

\begin{lstlisting}[style=code]
int Recursive( int n ) {
    if ( n == 1 )
        return 2;
    else
        return RecursiveFormula( n-1 ) + 2;
}
\end{lstlisting}

        \newpage
        \subsection{Building closed sequences}

        Before we start guessing formulas from sequences,
        let's generate some sequences ourselves first and look
        at what kind of patterns we find.

        \subsubsection{Slope and Intercept}

        Perhaps you remember graphing in algebra, when you had a linear
        function like $y = x$, and modifying the right-hand-side changed
        the style of the function: Adding or subtracting a number changed
        its position, and putting a number next to $x$ increased or
        decreased the slope.

        ~\\
        \begin{tabular}{l l l}
            \begin{tikzpicture}[framed]
                \draw[gray] (1.5, 0) -- (-1.5, 0);
                \draw[gray] (0, -1.5) -- (0, 1.5);
                \node at (2, 0) {$x$};
                \node at (0, 2) {$y$};

                % Notches on x axis
                \draw[gray] (-0.5, -0.1) -- (-0.5, 0.1);    % (-1, 0)
                \draw[gray] (-1.0, -0.1) -- (-1.0, 0.1);    % (-2, 0)
                \draw[gray] (-1.5, -0.1) -- (-1.5, 0.1);    % (-3, 0)
                \draw[gray] (0.5, -0.1) -- (0.5, 0.1);      % (1, 0)
                \draw[gray] (1.0, -0.1) -- (1.0, 0.1);      % (2, 0)
                \draw[gray] (1.5, -0.1) -- (1.5, 0.1);      % (3, 0)
                % Notches on y axis
                \draw[gray] (-0.1, -0.5) -- (0.1, -0.5);    % (0, -1)
                \draw[gray] (-0.1, -1.0) -- (0.1, -1.0);    % (0, -2)
                \draw[gray] (-0.1, -1.5) -- (0.1, -1.5);    % (0, -3)
                \draw[gray] (-0.1, 0.5) -- (0.1, 0.5);      % (0, 1)
                \draw[gray] (-0.1, 1.0) -- (0.1, 1.0);      % (0, 2)
                \draw[gray] (-0.1, 1.5) -- (0.1, 1.5);      % (0, 3)

                \node[gray] at (1.5, 0.25) { \tiny 3 };
                \node[gray] at (-0.25, 1.5) { \tiny 3 };

                \draw (-1.5, -1.5) -- (1.5,1.5);
            \end{tikzpicture}
            &
            \begin{tikzpicture}[framed]
                \draw[gray] (1.5, 0) -- (-1.5, 0);
                \draw[gray] (0, -1.5) -- (0, 1.5);
                \node at (2, 0) {$x$};
                \node at (0, 2) {$y$};

                % Notches on x axis
                \draw[gray] (-0.5, -0.1) -- (-0.5, 0.1);    % (-1, 0)
                \draw[gray] (-1.0, -0.1) -- (-1.0, 0.1);    % (-2, 0)
                \draw[gray] (-1.5, -0.1) -- (-1.5, 0.1);    % (-3, 0)
                \draw[gray] (0.5, -0.1) -- (0.5, 0.1);      % (1, 0)
                \draw[gray] (1.0, -0.1) -- (1.0, 0.1);      % (2, 0)
                \draw[gray] (1.5, -0.1) -- (1.5, 0.1);      % (3, 0)
                % Notches on y axis
                \draw[gray] (-0.1, -0.5) -- (0.1, -0.5);    % (0, -1)
                \draw[gray] (-0.1, -1.0) -- (0.1, -1.0);    % (0, -2)
                \draw[gray] (-0.1, -1.5) -- (0.1, -1.5);    % (0, -3)
                \draw[gray] (-0.1, 0.5) -- (0.1, 0.5);      % (0, 1)
                \draw[gray] (-0.1, 1.0) -- (0.1, 1.0);      % (0, 2)
                \draw[gray] (-0.1, 1.5) -- (0.1, 1.5);      % (0, 3)

                \node[gray] at (1.5, 0.25) { \tiny 3 };
                \node[gray] at (-0.25, 1.5) { \tiny 3 };
                \node[gray] at (-1.0, 0.25) { \tiny -2 };
                \node[gray] at (-0.25, 1.0) { \tiny 2 };

                \draw (-1.0, 0.0) -- (0, 1.0);
                \draw (-1.0, 0.0) -- (-1.5, -0.5);
                \draw (0, 1.0) -- (0.5, 1.5);
            \end{tikzpicture}
            &
            \begin{tikzpicture}[framed]
                \draw[gray] (1.5, 0) -- (-1.5, 0);
                \draw[gray] (0, -1.5) -- (0, 1.5);
                \node at (2, 0) {$x$};
                \node at (0, 2) {$y$};

                % Notches on x axis
                \draw[gray] (-0.5, -0.1) -- (-0.5, 0.1);    % (-1, 0)
                \draw[gray] (-1.0, -0.1) -- (-1.0, 0.1);    % (-2, 0)
                \draw[gray] (-1.5, -0.1) -- (-1.5, 0.1);    % (-3, 0)
                \draw[gray] (0.5, -0.1) -- (0.5, 0.1);      % (1, 0)
                \draw[gray] (1.0, -0.1) -- (1.0, 0.1);      % (2, 0)
                \draw[gray] (1.5, -0.1) -- (1.5, 0.1);      % (3, 0)
                % Notches on y axis
                \draw[gray] (-0.1, -0.5) -- (0.1, -0.5);    % (0, -1)
                \draw[gray] (-0.1, -1.0) -- (0.1, -1.0);    % (0, -2)
                \draw[gray] (-0.1, -1.5) -- (0.1, -1.5);    % (0, -3)
                \draw[gray] (-0.1, 0.5) -- (0.1, 0.5);      % (0, 1)
                \draw[gray] (-0.1, 1.0) -- (0.1, 1.0);      % (0, 2)
                \draw[gray] (-0.1, 1.5) -- (0.1, 1.5);      % (0, 3)

                \node[gray] at (1.5, 0.25) { \tiny 3 };
                \node[gray] at (-0.25, 1.5) { \tiny 3 };

                \draw (0.75, 1.5) -- (-0.75, -1.5);
            \end{tikzpicture}
            \\
            $y = x$
            &
            $y = x + 2$
            &
            $y = 2x$
            \\
            y-intersection is at $(0, 0)$
            &
            y-intersection is at $(-2, 0)$
            &
            y-intersection is at $(0, 0)$
            \\
            Slope is ``up 1 over 1''
            &
            Slope is ``up 1 over 1''
            &
            Slope is ``up 2 over 1''
        \end{tabular}

        ~\\
        These equations are the same as writing closed formulas
        to find an element at position $n$, where $n \equiv x$ and
        $a_n \equiv y$.

        \newpage


        \begin{tabular}{l l l}
            \begin{tikzpicture}[framed]
                \draw[gray] (0, 3.0) -- (0, 0);
                \draw[gray] (0, 0) -- (3.0, 0);
                \node at (3.5, 0) {$x$};
                \node at (0, 3.5) {$y$};

                % Notches on x axis
                \draw[gray] (0.5, -0.1) -- (0.5, 0.1);      % (1, 0)
                \draw[gray] (1.0, -0.1) -- (1.0, 0.1);      % (2, 0)
                \draw[gray] (1.5, -0.1) -- (1.5, 0.1);      % (3, 0)
                \draw[gray] (2.0, -0.1) -- (2.0, 0.1);    % (4, 0)
                \draw[gray] (2.5, -0.1) -- (2.5, 0.1);    % (5, 0)
                \draw[gray] (3.0, -0.1) -- (3.0, 0.1);    % (6, 0)
                % Notches on y axis
                \draw[gray] (-0.1, 0.5) -- (0.1, 0.5);      % (0, 1)
                \draw[gray] (-0.1, 1.0) -- (0.1, 1.0);      % (0, 2)
                \draw[gray] (-0.1, 1.5) -- (0.1, 1.5);      % (0, 3)
                \draw[gray] (-0.1, 2.0) -- (0.1, 2.0);    % (0, 4)
                \draw[gray] (-0.1, 2.5) -- (0.1, 2.5);    % (0, 5)
                \draw[gray] (-0.1, 3.0) -- (0.1, 3.0);    % (0, 6)

                \node[gray] at (1.5, 0.25) { \tiny 3 };
                \node[gray] at (-0.25, 1.5) { \tiny 3 };
                \node[gray] at (3.0, 0.25) { \tiny 6 };
                \node[gray] at (-0.25, 3.0) { \tiny 6 };

                \draw[fill=black] (0.5,0.5) circle (1pt) node[right] { \tiny (1,1) };
                \draw[fill=black] (1.0,1.0) circle (1pt) node[right] { \tiny (2,2) };
                \draw[fill=black] (1.5,1.5) circle (1pt) node[right] { \tiny (3,3) };
                \draw[fill=black] (2.0,2.0) circle (1pt) node[right] { \tiny (4,4) };
                \draw[fill=black] (2.5,2.5) circle (1pt) node[right] { \tiny (5,5) };
                \draw[fill=black] (3.0,3.0) circle (1pt) node[right] { \tiny (6,6) };

                \draw (0, 0) -- (3, 3);
            \end{tikzpicture}
            &
            \begin{tikzpicture}[framed]
                \draw[gray] (0, 3.0) -- (0, 0);
                \draw[gray] (0, 0) -- (3.0, 0);
                \node at (3.5, 0) {$x$};
                \node at (0, 3.5) {$y$};

                % Notches on x axis
                \draw[gray] (0.5, -0.1) -- (0.5, 0.1);      % (1, 0)
                \draw[gray] (1.0, -0.1) -- (1.0, 0.1);      % (2, 0)
                \draw[gray] (1.5, -0.1) -- (1.5, 0.1);      % (3, 0)
                \draw[gray] (2.0, -0.1) -- (2.0, 0.1);    % (4, 0)
                \draw[gray] (2.5, -0.1) -- (2.5, 0.1);    % (5, 0)
                \draw[gray] (3.0, -0.1) -- (3.0, 0.1);    % (6, 0)
                % Notches on y axis
                \draw[gray] (-0.1, 0.5) -- (0.1, 0.5);      % (0, 1)
                \draw[gray] (-0.1, 1.0) -- (0.1, 1.0);      % (0, 2)
                \draw[gray] (-0.1, 1.5) -- (0.1, 1.5);      % (0, 3)
                \draw[gray] (-0.1, 2.0) -- (0.1, 2.0);    % (0, 4)
                \draw[gray] (-0.1, 2.5) -- (0.1, 2.5);    % (0, 5)
                \draw[gray] (-0.1, 3.0) -- (0.1, 3.0);    % (0, 6)

                \node[gray] at (1.5, 0.25) { \tiny 3 };
                \node[gray] at (-0.25, 1.5) { \tiny 3 };
                \node[gray] at (3.0, 0.25) { \tiny 6 };
                \node[gray] at (-0.25, 3.0) { \tiny 6 };

                \draw[fill=black] (0.5,1.5) circle (1pt) node[right] { \tiny (1,3) };
                \draw[fill=black] (1.0,2.0) circle (1pt) node[right] { \tiny (2,4) };
                \draw[fill=black] (1.5,2.5) circle (1pt) node[right] { \tiny (3,5) };
                \draw[fill=black] (2.0,3.0) circle (1pt) node[right] { \tiny (4,6) };

                \draw (0, 1) -- (2, 3);
            \end{tikzpicture}
            &
            \begin{tikzpicture}[framed]
                \draw[gray] (0, 3.0) -- (0, 0);
                \draw[gray] (0, 0) -- (3.0, 0);
                \node at (3.5, 0) {$x$};
                \node at (0, 3.5) {$y$};

                % Notches on x axis
                \draw[gray] (0.5, -0.1) -- (0.5, 0.1);      % (1, 0)
                \draw[gray] (1.0, -0.1) -- (1.0, 0.1);      % (2, 0)
                \draw[gray] (1.5, -0.1) -- (1.5, 0.1);      % (3, 0)
                \draw[gray] (2.0, -0.1) -- (2.0, 0.1);    % (4, 0)
                \draw[gray] (2.5, -0.1) -- (2.5, 0.1);    % (5, 0)
                \draw[gray] (3.0, -0.1) -- (3.0, 0.1);    % (6, 0)
                % Notches on y axis
                \draw[gray] (-0.1, 0.5) -- (0.1, 0.5);      % (0, 1)
                \draw[gray] (-0.1, 1.0) -- (0.1, 1.0);      % (0, 2)
                \draw[gray] (-0.1, 1.5) -- (0.1, 1.5);      % (0, 3)
                \draw[gray] (-0.1, 2.0) -- (0.1, 2.0);    % (0, 4)
                \draw[gray] (-0.1, 2.5) -- (0.1, 2.5);    % (0, 5)
                \draw[gray] (-0.1, 3.0) -- (0.1, 3.0);    % (0, 6)

                \node[gray] at (1.5, 0.25) { \tiny 3 };
                \node[gray] at (-0.25, 1.5) { \tiny 3 };
                \node[gray] at (3.0, 0.25) { \tiny 6 };
                \node[gray] at (-0.25, 3.0) { \tiny 6 };

                \draw[fill=black] (0.5,1.0) circle (1pt) node[right] { \tiny (1,2) };
                \draw[fill=black] (1.0,2.0) circle (1pt) node[right] { \tiny (2,4) };
                \draw[fill=black] (1.5,3.0) circle (1pt) node[right] { \tiny (3,6) };

                \draw (0, 0) -- (1.5, 3.0);
            \end{tikzpicture}
            \\
            $y = x$
            &
            $y = x + 2$
            &
            $y = 2x$
            \\
            $a_n = n$
            &
            $a_n = n + 2$
            &
            $a_n = 2n$
            \\ \\
            $a_1 = 1$
            &
            $a_1 = 3$
            &
            $a_1 = 2$
            \\
            $a_2 = 3$
            &
            $a_2 = 4$
            &
            $a_2 = 4$
            \\
            $a_3 = 2$
            &
            $a_3 = 5$
            &
            $a_3 = 6$
        \end{tabular}

        ~\\ ~\\
        All of this is to say, multiplying or dividing $n$ is going to
        change the ``speed'' of the increase/decrease between numbers
        in the sequences:
        
        ~\\
        \begin{center}
        \begin{tabular}{p{4cm} p{4cm} p{4cm}}
            Increases by 1
            & Increases by 2
            & Increases by $\frac{1}{2}$
            \\ \hline \\
            $a_n = n$
            & $a_n = 2n$
            & $a_n = \frac{1}{2}n$ \\ \\
            1, 2, 3, 4, ...
            & 2, 4, 6, 8, ...
            & $\frac{1}{2}$, 1, $1 \frac{1}{2}$, 2, ...
        \end{tabular}
        \end{center}

        ~\\~\\
        And adding or subtracting a number from $n$ is going to change
        the ``offset'' of where the numbers in the sequence begin:

        ~\\
        \begin{center}
        \begin{tabular}{p{4cm} p{4cm} p{4cm}}
            Starts at 1
            & Starts at 2
            & Starts at 0
            \\ \hline \\
            $a_n = n$
            & $a_n = n + 1$
            & $a_n = n - 1$ \\ \\
            1, 2, 3, 4, ...
            & 2, 3, 4, 5, ...
            & 0, 1, 2, 3, ...
        \end{tabular}
        \end{center}
                
        \newpage
        
        We aren't going to be doing graphs as in an elementary algebra class,
        but I wanted to remind you of those properties - of moving the intercepts
        and changing the slopes - because they're relevant here. If we're going
        to be \textit{``guessing''} formulas based on numbers for this section,
        it's good to be able to identify the patterns we see.
        
        ~\\~\\
        So now, let's build some basic sequences and look at the patterns
        directly, before getting into the whole guessing game.
        
        
        
        
        
        \subsubsection{Linear formulas}

        How many different linear formulas can you come up with?
        With a linear formula we will just have $n^1$, which
        causes a steady increase/decrease.
        
        ~\\ Let's show an example, and we're going to start with
        \textbf{the first value will be at $n=0$} - many math books
        prefer starting with $n=1$ for a sequence, but we are computer
        scientists, and starting at 0 reveals some extra information.

        \begin{center}
        \begin{tabular}{p{3cm} | c | c | c | c | p{5cm}}
            Formula         & $a_0$ & $a_1$ & $a_2$ & $a_3$ & Notes
            \\ \hline  & & & & \\
            
            $a_n = n$       & 0 & 1 & 2 & 3
            & Starts at 0, goes up by 1
            \\ & & & & \\
            $a_n = 2n$      & 0 & 2 & 4 & 6
            & Starts at 0, goes up by 2
            \\ & & & & \\ 
            $a_n = 2n + 1$  & 1 & 3 & 5 & 7
            & Starts at 1, goes up by 2
            \\ & & & & \\
            $a_n = 3n + 5$  & 5 & 8 & 11 & 14 
            & Starts at 5, goes up by 3
            \\ & & & & \\
            $a_n = -2n + 1$ & 1 & -1 & -3 & -5
            & Starts at 1, goes down by 2
        \end{tabular}
        \end{center}
        
        ~\\
        Starting with the index of $0$, and given the form
        $a_n = mn + b$
        
        \begin{itemize}
            \item   The starting value of the first element at position 0 is $b$.
            \item   The increase/decrease of the sequence is given by $m$.
        \end{itemize}
        
        ~\\ If we began with $n=1$ as the first term, this relationship between
        $b$ and the starting position would be hidden; the first term would be
        $b + m$ instead of just $b$.


        \newpage
        \subsubsection{Quadratic formulas}
        
        We will also run into sequences that are quadratic.
        We think of quadratic functions in algebra like this:
        
        \begin{center}        
        \begin{tikzpicture}
            \draw[gray] (2, 0) -- (-2, 0);
            \draw[gray] (0, -2) -- (0, 2);
            \node at (2.5, 0) {$x$};
            \node at (0, 2.5) {$y$};

            \draw (0,0) parabola (1.5,2.25) ;
            \draw (0,0) parabola (-1.5,2.25) ;
        \end{tikzpicture}
        ~\\
        $y = x^2$
        \end{center}
        
        But since we're working with sequences, we tend to just care about
        the positive positions.
        
        \begin{center}        
        \begin{tikzpicture}
            \draw[gray] (4, 0) -- (0, 0);
            \draw[gray] (0, 0) -- (0, 4);
            \node at (4.5, 0) {$x$};
            \node at (0, 4.5) {$y$};

            \draw (0,0) parabola (2,4);
            
            \draw[fill=black] (0,0) circle (1pt) node[right] { \tiny (0,0) };
            \draw[fill=black] (1, 1) circle (1pt) node[right] { \tiny (1,1) };
            \draw[fill=black] (2, 4) circle (1pt) node[right] { \tiny (2,4) };
        \end{tikzpicture}
        ~\\
        $y = x^2$
        \end{center}
        
        
        \begin{center}
        \begin{tabular}{p{3cm} | c | c | c | c | p{5cm}}
            Formula         & $a_0$ & $a_1$ & $a_2$ & $a_3$ & Notes
            \\ \hline  & & & & \\
            
            $a_n = n^2$       & 0 & 1 & 4 & 9
            & Starts at 0, goes up by $n^2$
            \\ & & & & \\ 
            $a_n = n^2 + 1$  & 1 & 2 & 5 & 10
            & Starts at 1, goes up by $n^2$
            \\ & & & & \\
            $a_n = 2n^2$      & 0 & 2 & 8 & 18
            & Starts at 0, goes up by $2n^2$
        \end{tabular}
        \end{center}
        
        ~\\
        Again, adding/subtracting a number affects the offset,
        and multiplying/dividing from $n$ affects the speed of increase/decrease.

        \newpage
        \subsubsection{Exponential formulas}

        And finally, we will sometimes find exponential formulas
        in this section, so we need to see what the pattern of numbers
        tends to look like so we can better identify it later.

        \begin{center}       
            \pgfplotsset{compat=1.11}
            \begin{tikzpicture}
            \begin{axis}[grid=none,
                      xmax=4,ymax=10,
                      axis lines=middle,
                      restrict y to domain=-7:12,
                      enlargelimits]
            \addplot[black]  {pow(2,x)} node[above]{};
            
            
            \draw[fill=black] (0, 1) circle (1pt) node[right] { \tiny (0,1) };
            \draw[fill=black] (1, 2) circle (1pt) node[right] { \tiny (1,2) };
            \draw[fill=black] (2, 4) circle (1pt) node[right] { \tiny (2,4) };
            \draw[fill=black] (3, 8) circle (1pt) node[right] { \tiny (3,8) };
            \end{axis}
            \end{tikzpicture}
        ~\\
        $y = 2^x$
        \end{center}
        
        There are, of course, different types of exponential functions.
        In particular, $2^x$ is an important function in computer science,
        since everything is built on top of binary - but more on that another time.
        
        \begin{center}
        \begin{tabular}{p{3cm} | c | c | c | c | c | c | c | c }
            Formula         & $a_0$ & $a_1$ & $a_2$ & $a_3$ & $a_4$ & $a_5$ & $a_6$ & $a_7$
            \\ \hline  & & & & & & & & \\
            $a_n = 2^n$         & 1 & 2 & 4 & 8 & 16 & 32 & 64 & 128
            \\ & & & & & & & & \\ 
            $a_n = 2^n + 1$     & 2 & 3 & 5 & 9 & 17 & 33 & 65 & 129
            \\ & & & & & & & & \\ 
            $a_n = 3^n$         & 1 & 3 & 9 & 27 & 81 & 243 & 729 & 2187
        \end{tabular}
        \end{center}
        
        \subsubsection{Linear vs. Quadratic vs. Exponential}
        
        So overall, make sure you're able to recognize the kind of patterns
        that emerge when looking at a linear sequence, a quadratic sequence, and an exponential sequence...
        
        \begin{center}
            1, 3, 5, 7, 9, ... \tab 1, 2, 4, 9, 16, 25, ... \tab 1, 2, 4, 8, 16, 32, 64, ...
        \end{center}


        \newpage
        \subsection{Building recursive sequences}
        
        We should also spend a bit of time looking at building recursive sequences,
        to see what kind of patterns we get out of these.
        Remember that we need to specify at least one starting value, such as $a_1 = 1$,
        and then future elements $a_n$ are based on one or more previous elements $a_{n-1}$.
        So what kind of formulas can we come up with?
        
        \begin{center}
            \begin{tabular}{p{7cm} | c | c | c | c | c | c | }
                Formula and initial                     & $a_2$ & $a_3$ & $a_4$ & $a_5$ & $a_6$ & $a_7$
                \\ \hline & & & & & & \\
                $a_n = a_{n-1}+1$,                      & 2     & 3     & 4     & 5     & 6     & 7
                \\ $a_1 = 1$ & & & & & &
                \\ Starts at 1, goes up linearly    & & & & & &
                \\ (Closed formula: $a_n = n$)      & & & & & &
                \\ & & & & & & \\
                
                $a_n = 2a_{n-1}$,                       & 2     & 4     & 8     & 16    & 32    & 64
                \\ $a_1 = 1$ & & & & & &
                \\ Starts at 1, goes up quadratically   & & & & & &
                \\ (Closed formula: $a_n = 2^{n-1})$        & & & & & &
                \\ & & & & & & \\
                
                $a_n = n + a_{n-1}$,                    & 3     & 6     & 10    & 15    & 21    & 28
                \\ $a_1 = 1$ & & & & & &
                \\ Starts at 1, goes up by an increasing amount each time. (+2, +3, +4, +5) & & & & & &
                \\ & & & & & & \\
                
                $a_n = n \cdot a_{n-1}$,                & 2     & 6     & 24    & 120   & 720   & 5040
                \\ $a_1 = 1$ & & & & & &
                \\ Starts at 1, increasing greatly.  & & & & & &
                \\ (Closed formula: $a_n = n!$)     & & & & & &
            \end{tabular}
        \end{center}
        
        ~\\ Just be familiar with the look of recursive sequences
        using different patterns: adding/subtracting constant values and variable values, 
        multiplying by constant values and variable values, and so on.
        
        \newpage
        Any of the recursive functions, we can build an equation for $a_n$ at some specific $n$,
        similarly to how we'd step through a recursive function when programming...
        
        
        ~\\
        ``What is $a_4$ for $a_n = n \cdot a_n-1$, with $a_1 = 1$?''
        
        ~\\
        \begin{tabular}{l l}
            $a_4 = 4 \cdot a_3$     & But what is $a_3$? \\
            $a_3 = 3 \cdot a_2$     & But what is $a_2$? \\
            $a_2 = 2 \cdot a_1$     & But what is $a_1$? \\
            $a_1 = 1$               & OK, now we can plug these in...
            \\
            \\
            $a_4 = 4 \cdot a_3$                                 & Plug in $a_3$ equation... \\
            $a_4 = 4 \cdot (3 \cdot a_2)$                       & Plug in $a_2$ equation... \\
            $a_4 = 4 \cdot (3 \cdot (2 \cdot a_1))$             & Plug in $a_1$ equation... \\
            $a_4 = 4 \cdot (3 \cdot (2 \cdot (1)))$             & The result for $a_4$ \\
        \end{tabular}
        
        ~\\~\\
        Looking at this, you can see that the result of $a_4$ ends up being $4!$,
        so the closed formula version of $a_n = n \cdot a_{n-1}$ with $a_1 = 1$ is $a_n = 4!$
        
        
        
        

        \newpage
        \subsection{Figuring out formulas from sequences}

        The main gist of this section is to figure out formulas based on
        sequences of numbers. Sometimes, the pattern can be easily
        viewed based on our prior experience with strings of numbers,
        like ``2, 4, 6, 8, 10'', while others may have their patterns
        hidden deeper within them. Let's look at some strategies.
        
        \subsubsection{What's the difference?}
        
        One way to analyze the sequence is to look at what the difference
        is between each element of the sequence and then asking:
        is this difference the same every time? Is the \textit{difference}
        changing each term?
        
        ~\\
        With a sequence like 0, 2, 4, 6, 8...
        
        \begin{center}
            \begin{tabular}{l | c c c c c c c c c}
                Index $n$       & 0 & & 1 & & 2 & & 3 & & 4
                \\ \hline
                Element $a_n$   & 0 & & 2 & & 4 & & 6 & & 8
                \\ \hline
                Difference      & & +2 & & +2 & & +2 & & +2
            \end{tabular}
        \end{center}
        
        We can see that the difference is +2 each time,
        and we can see that $a_1 = 2$.
        
        \paragraph{Closed formula, $a_n = mn + b$?} ~\\
        
        Given that the difference is +2 each time, this corresponds
        to our ``slope'' (if we're thinking of $y = mx + b$).
        By having our sequence start at the index 0, we can see that
        there is no offset $b$ here.
        
        ~\\
        The closed formula will be $a_n = 2n$.
        
        \paragraph{Recursive formula?} ~\\
        
        Here, we can see the first value already: $a_0 = 0$. And,
        breaking down the difference, we can see that each term
        is +2 more than the previous term.
        
        ~\\
        This gives us the recursive formula $a_n = a_{n-1} + 2$, with $a_0 = 0$.
        
        \newpage
        \paragraph{Checking yourself} ~\\
        Want to verify your work? You could plug in values for $n$ to make sure
        the sequence matches up, or you could write some code...

~\\ C++ code:
\begin{lstlisting}[style=code]
int Formula1( int n ) { // closed formula
    return 2 * n;
}

int Formula2( int n ) { // recursive formula
    if ( n == 0 ) 
        return 0;
    else 
        return Formula2( n - 1 ) + 2;
}

int main()
{
    for ( int n = 0; n < 5; n++ )
    {
        cout << "n = " << n << "\t"
            << "Closed: " << Formula1( n ) << "\t"
            << "Recursive: " << Formula2( n ) << endl;
    }
    return 0;
}
\end{lstlisting}

~\\ Program output:
\begin{lstlisting}[style=output]
n = 0	Closed: 0	Recursive: 0
n = 1	Closed: 2	Recursive: 2
n = 2	Closed: 4	Recursive: 4
n = 3	Closed: 6	Recursive: 6
n = 4	Closed: 8	Recursive: 8
\end{lstlisting}
        
        \newpage
        \subsubsection{Difference of the differences?}
        
        Sometimes the changes between each element changes,
        but there might be an underlying pattern in the differences themselves.
        
        ~\\ Lets look at the sequence 6, 11, 19, 30, 44
        
        \begin{center}
            \begin{tabular}{p{5cm} | c c c c c c c c c}
                Index $n$                   & 0 & & 1 & & 2 & & 3 & & 4
                \\ \hline
                Element $a_n$               & 6 & & 11 & & 19 & & 30 & & 44
                \\ \hline
                Difference of elements      & & +5 & & +8 & & +11 & & +14
                \\ \hline
                Difference of differences   & & & +3 & & +3 & & +3
            \end{tabular}
        \end{center}
        
        ~\\
        Here the difference between each element grows each time,
        but the difference between \textit{those differences} is constant,
        changing by +3 each time.
        
        ~\\
        So from the element 6, how do we get to 11, and then to 19?
        
        \begin{center}
        \begin{tabular}{c c l l}
            Index & Element         & Relationship to previous  & Mathmematically
            \\ \hline
            1   & 11                & 6 + 5                       & $a_{n-1} + 5 \cdot 3(n-1)$
            \\  &                   &                             & $a_0 + 5 \cdot 3(0)$
            \\ \\
            2   & 19                & 11 + 5 + 3                  & $a_{n-1} + 5 \cdot 3(n-1)$
            \\  &                   &                             & $a_1 + 5 \cdot 3(1)$
            \\ \\
            3   & 30                & 19 + 5 + 3 + 3              & $a_{n-1} + 5 \cdot 3(n-1)$
            \\  &                   &                             & $a_2 + 5 \cdot 3(2)$
            \\ \\
            4   & 44                & 30 + 5 + 3 + 3 + 3          & $a_{n-1} + 5 \cdot 3(n-1)$
            \\  &                   &                             & $a_3 + 5 \cdot 3(3)$
        \end{tabular}
        \end{center}
        
        Looking at it recursively, each element references the previous value,
        plus five, and then plus some amount of 3's.
        
        ~\\
        We will go over the specific technique to solve recursive sequences
        in a bit, but I'm just trying to illustrate the relationship
        in this sequence here.
        
        \newpage
        \subsubsection{Building the element from the index}
        
        Another way to look for patterns in the values is to write out
        each index and element given, and try to figure out how to
        create the element value given $n$.
        
        ~\\ Let's take 2, 5, 8, 11, 14.
        
        ~\\ For each element given, I'll try to write a basic equation that
        results in each element. ``How do I build 2 from the index 0?'',
        ``How do I build 5 from the index 1?'' and so on.
        
        \begin{center}
            \begin{tabular}{l l}
            Index and element   & Building the element from $n$
            \\ \hline
            $a_0 = 2$           & 2 = 0 + 2
            \\
            $a_1 = 5$           & 5 = 1 + 4
            \\
            $a_2 = 8$           & 8 = 2 + 6
            \\
            $a_3 = 11$          & 11 = 3 + 8
            \\
            $a_4 = 14$          & 14 = 4 + 10
            \end{tabular}
        \end{center}
        
        From here, we can start to see a pattern: We have the index, plus 2, then plus 4, then plus 6, and so on.
        
        \paragraph{Closed formula:}
        From this pattern, we can build out the equation $a_n = n + 2( n+1 )$.
        
        ~\\ We're adding the value of $n$, plus a value from the sequence ``2, 4, 6, 8''.
        We start with 2 when we have index 0, so we can represent it with $2(n+1)$.
        
        ~\\ But this equation can be simplified throuhg basic algebra as well...
        
        $a_n = n + 2(n+1)$ \\
        \tab $    = n + 2n + 2$ \\
        \tab $    = 3n + 2 $
        
        ~\\
        So a simplified version of this closed formula would be $ a_n = 3n + 2 $.
        
        \newpage
        \paragraph{Recursive formula:}
        
        We can also do the same thing, but building each element with respect to the previous element.
        We know that $a_0 = 2$ in this case, so we'll keep that out of the table.

        \begin{center}
            \begin{tabular}{l l}
            Index and element   & Building the element from $a_{n-1}$
            \\ \hline
            $a_1 = 5$           & 5 = 2 + 3
            \\
            $a_2 = 8$           & 8 = 5 + 3
            \\
            $a_3 = 11$          & 11 = 8 + 3
            \\
            $a_4 = 14$          & 14 = 11 + 3
            \end{tabular}
        \end{center}
        
        ~\\
        From here we can see that each element is built off the previous
        element plus 3, so we get the formula: $a_n = a_{n-1} + 3$, with $a_0 = 2$.



