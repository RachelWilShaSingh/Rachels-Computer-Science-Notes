    
    \newpage
    %---%
    \section{Solving Recurrence Relations}
    
        \begin{hint}{Note}
        These notes are based off chapter 5.6 of the Ensley and Crawley Discrete Math book.
        For context, we are solving formulas for sequences in chapter 1.2 of this book,
        and then this information on how to \textit{actually solve for recurrence relations}
        comes in at chapter 5.6, which was originally part of Discrete Math 2 curriculum.
        ~\\~\\
        As such, this information can be really handy, but, for Discrete Math 1 classes,
        this is not going to be tested over or part of homework.
        \end{hint}
    
        % Chapter 5.6
        There is an actual method to solving recurrence relations
        that doesn't require investigating and guessing formulas,
        so let's look at these.
        
        \subsection{Difference tables}
        
        Given a simple sequence like 2, 5, 8, 11, 14, we can
        begin to solve it with a difference table, like how I
        illustrated in the previous section.
        Here, we're going to label the index $n$, the element $s_n$,
        and the difference $\Delta_n$.
        
        \begin{center}
        \begin{tabular}{ | l | c | c | c | c | c | }
            \hline
            
            \rowcolor{colorblind_light_blue}
            $n$             & 0 & 1 & 2 & 3 & 4
            \\ \hline
            
            \rowcolor{colorblind_light_orange}
            $s_n$           & 2 & 5 & 8 & 11 & 14
            \\ \hline
            
            \rowcolor{colorblind_light_gray}
            $\Delta_n$      & 3 & 3 & 3 & 3 & ...
            
            \\ \hline
        \end{tabular}
        \end{center}
        
        We can build any element $s_n$ based on these values like this:
        
        \begin{center}
        \begin{tabular}{c | c | l}
            $n$ & $s_n$ & Equation \\ \hline
            1 & 5 & $s_0$ + $\Delta_0$
            \\
            2 & 8 & $s_0$ + $\Delta_0$ + $\Delta_1$
            \\ 
            3 & 11 & $s_0$ + $\Delta_0$ + $\Delta_1$ + $\Delta_2$
            \\
            4 & 14 & $s_0$ + $\Delta_0$ + $\Delta_1$ + $\Delta_2$ + $\Delta_3$
        \end{tabular}
        \end{center}
        
        which can be further simplified to...
        
        $$ s_n = s_0 + \sum_{k=0}^{n} \Delta_k $$
        
        And since the difference is always 3, if we take $\sum_{k=0}^{n} 3$ we end up with $3n$, so...
        
        $$ s_n = s_0 + 3n $$

        ...and...

        $$ s_n = 2 + 3n $$
        
        ... which is our formula.

        
        \subsection{Complex sequences}
        
        I tried illustrating finding the \textit{difference of differences} last time,
        but let's look at it more mathematically. We will again use a difference table,
        but will need to go deeper.
        
        ~\\ Let's take the sequence 6, 11, 19, 30, 44, and we'll build out the difference table:
        
        
        \begin{center}
        \begin{tabular}{ | l | c | c | c | c | c | }
            \hline
            
            \rowcolor{colorblind_light_blue}
            $n$             & 0 & 1 & 2 & 3 & 4
            \\ \hline
            
            \rowcolor{colorblind_light_orange}
            $s_n$           & 6 & 11 & 19 & 30 & 44
            \\ \hline
            
            \rowcolor{colorblind_light_gray}
            $\Delta_n$      & 5 & 8 & 11 & 14 & ??
            
            \\ \hline
        \end{tabular}
        \end{center}
        
        This isn't enough information to find the pattern, so we check the
        difference of the $\Delta$. So we will use $\Delta_n^1$ as the first
        level difference (difference of elements), and $\Delta_n^2$ as the
        second level difference (difference of $\Delta^1$s):
        
        
        \begin{center}
        \begin{tabular}{ | l | c | c | c | c | c | }
            \hline
            
            \rowcolor{colorblind_light_blue}
            $n$             & 0 & 1 & 2 & 3 & 4
            \\ \hline
            
            \rowcolor{colorblind_light_orange}
            $s_n$           & 6 & 11 & 19 & 30 & 44
            \\ \hline
            
            \rowcolor{colorblind_light_gray}
            $\Delta_n^1$      & 5 & 8 & 11 & 14 & ??
            
            \\ \hline
            
            \rowcolor{pink}
            $\Delta_n^2$      & 3 & 3 & 3 & ... & ...
            
            \\ \hline
        \end{tabular}
        \end{center}
        
        Here, $\Delta^2$ is constant; if it weren't, we would go down another level
        until we found a constant change (if there were any).
        
        \newpage
        ~\\ Instead of illustrating the formula for $s_n$, now we do this for $\Delta^1$.
        We know $\Delta^1_0 = 5$, so...
        
        \begin{center}
        \large
        \begin{tabular}{c | c | l}
            $n$ & $\Delta^1_n$ & Equation \\ \hline
            1 & 5 & \color{umber} $\Delta^1_0$ + \color{purple} $\Delta^2_0$
            \\
            2 & 8 & \color{umber} $\Delta^1_0$ + \color{purple} $\Delta^2_0$ + $\Delta^2_1$
            \\ 
            3 & 11 & \color{umber} $\Delta^1_0$ + \color{purple} $\Delta^2_0$ + $\Delta^2_1$ + $\Delta^2_2$
            \\
            4 & 14 & \color{umber} $\Delta^1_0$ + \color{purple} $\Delta^2_0$ + $\Delta^2_1$ + $\Delta^2_2$ + $\Delta^2_3$
        \end{tabular}
        \normalsize
        \end{center}
        
        ~\\ And we build a formula for $\Delta^1_n$ from this...
        
        $$ \Delta^1_n = \Delta^1_0 + \sum_{k=0}^{n-1} \Delta^2_k $$
        
        ~\\ ... Which simplifies to...
        
        $$ \Delta^1_n = 5 + 3n $$
        
        ~\\ But this is just the formula to get the first-level difference; we
        still need the recursive formula for $s_n$.
        
        \begin{intro}{Fundamental Theorem of Sums and Differences\footnote{From Discrete Mathematics, Ensley \& Crawley, pg 425}}
            For any sequence $\{s_n\}$ with first differences $\Delta_k = s_{k+1} - s_k$, and any $n \geq 1$,
            $$ s_n - s_0 = \sum_{k=0}^{n-1} \Delta_k $$
            
            Or in other words...
            
            $$ s_n = \sum_{k=0}^{n-1} \Delta_k + s_0 $$
        \end{intro}
        
        \newpage
        Starting with the theorem...
        $$ s_n = \sum_{k=0}^{n-1} \Delta_k^1 + s_0 $$
        ... We begin plugging in values. We have the formula for our differences, $\Delta^1$,
        which is $ \Delta^1_n = 5 + 3n $...
        
        $$ s_n = \sum_{k=0}^{n-1} ( 5 + 3k ) + s_0 $$
        
        We can then split out the sum to solve...
        
        $$ s_n = \sum_{k=0}^{n-1} ( 5 ) + 3 \cdot \sum_{k=0}^{n-1} ( k ) + s_0 $$
        
        $ \sum_{k=0}^{n-1} ( 5 ) $ is another sum with a constant, giving us $5n$ as the result.
        Also, the first value of the sequence, $s_0$ is 6, so we will sub that out as well...
        
        $$ s_n = 5n + 3 \cdot \sum_{k=0}^{n-1} ( k ) + 6 $$
        
        So then, how do we solve the sum? We're going to cheat, I'm just going to give you the
        proposition buried somewhere random in the Ensley and Crawley book...
        
        \begin{intro}{Proposition from chapter 2.3 \footnote{From Discrete Mathematics, Ensley \& Crawley, pg 114} }
            $$ \sum_{i=1}^n i = \frac{n(n+1)}{2} $$
            
            Which can be adjusted to work with a starting value of 0 as...
            
            $$ \sum_{k=0}^(n-1) k = \frac{n(n-1)}{2} $$
        \end{intro}
        
        ... And we plug \textit{that} in...
        
        $$ s_n = 5n + 3 \cdot \frac{3n(n-1)}{2} + 6 $$
        
        ...And, sigh, after more simlifying...
        
        $$ s_n = \frac{3n^2 + 7n + 12}{2} $$
        
        There, we did it. Now you know it can be done. Let's move on.
        
        
        
        
        
        
        
