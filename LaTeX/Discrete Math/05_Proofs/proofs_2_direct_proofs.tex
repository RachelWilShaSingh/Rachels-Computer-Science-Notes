

\subsection{Implications and counter-examples}

Remember with propositional logic where we would build
\textbf{implications}, which consisted of a \textbf{hypothesis}
on the left-hand-side of the $\to$, and a \textbf{conclusion}
on the right-hand-side of the $\to$. The truth table for
an implication looks like this:

\begin{center}
    \begin{tabular}{| c | c || c |}
        \hline{}
        $p$     & $q$     & $p \to q$   \\ \hline
        \true   & \true   & \true       \\ \hline
        \true   & \false  & \false      \\ \hline
        \false  & \true   & \true       \\ \hline
        \false  & \false  & \true       \\ \hline
    \end{tabular}
\end{center}

where our implication could only be disproved (A false result for $p \to q$)
\textbf{if the hypothesis was true, and the conclusion was false.}

~\\
We can use this to help us with our proofs.

\newpage

\paragraph{Example:} ``If $n$ is odd, then $n+1$ is odd''

\begin{itemize}
    \item   \textbf{Hypothesis:} $n$ is odd
    \item   \textbf{Conclusion:} $n+1$ is odd
\end{itemize}

If we wanted to disprove this, we would only need a single case
where the \textbf{hypothesis is true, but the conclusion is false}...

\begin{center}
    $n = 3$, 3 is odd...
    ~\\
    if $3$ is odd, then $3+1$ is odd...
    ~\\
    if $3$ is odd, then $4$ is odd... \xmark NOPE!
\end{center}

~\\
This doesn't work out, since 4 is not odd; the conclusion here
is false, even though the hypothesis is true.

The scenario where we get a true hypothesis and a false conclusion,
therefore disproving the implication, is called a \textbf{counter-example}.

\paragraph{Example:} ``If $n$ is prime, then $n$ is odd.''

~\\
Maybe some examples check out: 3 is prime, 5 is prime, 7 is prime...

~\\
But we only have to find \textbf{one example} that makes it false
to disprove the entire implication.

\begin{center}
    $n = 2$ is prime...
    ~\\
    2 is odd? \xmark NOPE!!
\end{center}

\subsection{Direct proofs}

To disprove an implication, we only need one counter-example.
However, to \textbf{prove} the implication, we can't just plug in
numbers to show it's true for \textit{every integer} or \textit{every number}.
We will have to prove it mathematically.
~\\

With a \textbf{direct proof}, we will swap out any numbers that
are part of the hypothesis with definitions...
if a number is \textit{even}, swap it with $2k$, if a number is
odd, swap it with $2j+1$, if a number is divisible by 5,
swap it with $5i$. Then we simplify as much as possible,
and if our definitions still hold, we have a direct proof.

\paragraph{Example:} ``If $n$ is even, then $n+1$ is odd''.

~\\
We are operating on a variable $n$, and it's stated that $n$ is even.
Since this is the hypothesis, this \textbf{must be true} for this
implication to mean anything.
We are going to replace $n$ with the definition of an even number,
$$n = 2k$$
So now we have:

\begin{center}
    ``If $2k$ is even, then $2k + 1$ is odd.''
\end{center}

We don't have to simplify at all here - we can see that the hypothesis
is an even number, and if we add 1 to our even number, we get $2k+1$,
which is the \textbf{definition of an odd number}. So this checks out. \checkmark

\newpage
\paragraph{Example:} ``The result of summing any odd integer with any even integer is an odd integer.''

~\\
First, let's identify the hypothesis and conclusion:

\begin{itemize}
    \item   \textbf{Hypothesis:} one integer is odd, one integer is even
    \item   \textbf{Conclusion:} adding them together gives us an odd integer.
\end{itemize}

In our statement, we're working with two separate integers: an even one, and an odd one.
Therefore, we need different \textbf{aliases} for each:

\begin{center}
    \begin{tabular}{l | c | c}
        Defining our integers   & $n$ is even   & $m$ is odd
        \\ \hline
        Giving them aliases     & $n = 2k$      & $m = 2j+1$
    \end{tabular}
\end{center}

Notice that we have \textbf{two different variables} $n$ and $m$,
and each variable \textbf{gets a different alias variable:} $k$ and $j$.

\begin{center}
    \underline{\textbf{Do not re-use the same alias variable!}}~\\
    (This is a common oversight!)
\end{center}

Next, we build out the conclusion:

\begin{enumerate}
    \item   $n + m$ results in an odd integer. ~\\
            Replace with the aliases!
            
    \item   $2k + 2j+1$ results in an odd integer. ~\\
            Begin simplifying!
            
    \item   $2k + 2j + 1 = 2( k + j ) + 1 $ ~\\
            Factored out the 2
    
    \item   $2( k + j ) + 1$... ~\\ Acknowledge that, due to the
            Closure Property of Integers, $ k + j $ itself
            is \textit{some integer} AND that ``2 times \textit{some integer} plus 1''
            is \textbf{the definition of an odd integer.}
            
    \item   $n + m = 2( k+j ) + 1$ \checkmark ~\\
            This implication checks out! ~\\
            Some even integer $n$ plus some odd integer $m$
            results in some odd integer $2 (k+j) + 1$.
\end{enumerate}

\newpage
\paragraph{Example:} ``If $n$ is even and $m$ is odd, then $n \cdot m$ is even.''

\begin{enumerate}
    \item   Hypothesis: $n$ is even, $m$ is odd.
    \item   Conclusion: $n \cdot m$ is even.
    \item   Aliases: ~\\
            $n = 2k$ ~\\
            $m = 2j + 1$
    \item   $n \cdot m$ change to $2k \cdot (2j+1)$
    \item   Simplify:
            \begin{enumerate}
                \item   $2k(2j+1)$
                \item   $4kj + 2k$
                \item   Factor out the 2 to build the definition of an even integer...
                \item   $2(2kj + k)$
            \end{enumerate}
    \item   2 times \textit{some integer} is the \textbf{definition of an even integer}. ~\\
            ($2kj + k$ counts as \textit{``some integer''}!)
    \item   \checkmark this checks out!
\end{enumerate}


