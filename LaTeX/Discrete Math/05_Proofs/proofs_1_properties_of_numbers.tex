
%---%

When you make a \textbf{proposition} (recall: a statement that
is unambiguously \textit{true} or \textit{false}, like ``4 is even'',
or ``the sky is green''), how are you certain that what you've
posited is true for all cases? (Example, ``2 times any integer is always even'')

If you wrote a function in a program that was always supposed to return
a positive integer, \textit{how would you check} that it's true for all cases?
Sure, you could test the function against a bunch of numbers,
but you can't test \textit{every} number.

This is where proofs come in, and in this section we will be
looking at different types of proofs that cover different types
of things we want to validate.

\subsection{The Closure Property of Integers}

One thing we have to acknowledge first is the \textbf{closure property of integers}:
The fact that, given any two integers $n$ and $m$...

\begin{itemize}
    \item   $n + m$ is still an integer,
    \item   $n - m$ is still an integer, and
    \item   $n \cdot m$ is still an integer.
\end{itemize}

So, for example, if some function were supposed to \textit{always}
return an integer result, but for some reason it returned $\frac{n}{m}$,
then the truth that it ``always returns an integer'' would be disproven.

\subsection{Definition of Even and Odd Integers}

Given some integer $k$, an even integer can be defined as $2k$:
two times some integer is always an integer, and will always be even.

~\\
And, given some integer $k$, an odd integer can be defined as $2k + 1$:
an even integer ($2k$) plus 1 will always result in an odd integer.

~\\
So, while working on proofs later, we can say that $n$ is an even integer
by representing it with $$n = 2k$$ and we can say that $m$ is an odd integer
by represneting it with $$m = 2j + 1$$ More on this later on!

\subsection{Division and Modulus}

Likewise, we can define when a number is \textit{divisible by} some other number.
$n = 2k$ states that $n$ is evenly-divisibly by 2; no matter what $n$ is,
it can be represented as $2$ times some number...

\begin{center}
    \begin{tabular}{c c}
        $n$ value & Represented as 2 times something
        \\ \hline
        2   & = $2 \cdot 1$
        \\
        4   & = $2 \cdot 2$
        \\
        6   & = $2 \cdot 3$
        \\
        8   & = $2 \cdot 4$
    \end{tabular}
\end{center}

We can do the same thing if we wanted to say that $n$ is divisible by 5:
we define $n = 5k$, $n$ is going to be the result of 5 times some other integer...

\begin{center}
    \begin{tabular}{c c}
        $n$ value & Represented as 5 times something
        \\ \hline
        5   & = $5 \cdot 1$
        \\
        10   & = $5 \cdot 2$
        \\
        15   & = $5 \cdot 3$
        \\
        20   & = $5 \cdot 4$
    \end{tabular}
\end{center}

\newpage

\paragraph{Definition of divisibility:}\footnote{ From Discrete Mathematics, Ensley \& Crawley, pg 98 }
\begin{quote}
    ``An integer $n$ is \textit{divisible} by a nonzero integer $k$
    if there is an integer $q$ (called the quotient) such that $n = k \cdot q$.''
\end{quote}

\paragraph{Example:} Write 12 as the definition of an integer divisible by 3.

~\\
Using the form $n = k \cdot q$, 12 is the result, so $n = 12$. The quotient,
what it's divisible by, is 3, so $q = 3$. So we can write:

$$ 12 = k \cdot 3 $$

and, solving for $k$, we can write 12 in the form of \textbf{the definition of an integer that is divisible by 3:}

$$ 12 = 4 \cdot 3 $$


~\\
\paragraph{Definition of a rational number:}
\footnote{ From Discrete Mathematics, Ensley \& Crawley, pg 100 }
\begin{quote}
    ``A real number $r$ is \textit{rational} if there exists integers $a$ and $b$ ($b \neq 0$)
    with $r = \frac{a}{b}$.''
\end{quote}

So, given any integers for $a$ and $b$ (except 0 for $b$), whatever we build as $\frac{a}{b}$
will be a rational number. (Examples of an \textit{irrational number} would be numbers
with infinitely repeating decimals, $\pi$ and so on; if it's not rational, it's irrational!)


\newpage
\paragraph{The Division Theorem}
\footnote{ From Discrete Mathematics, Ensley \& Crawley, pg 103 }
\begin{quote}
    For all integers $a$ and $b$ (with $b > 0$) there is an integer $q$
    (called ``the quotient when $a$ is divided by $b$'') and an integer
    $r$ (called ``the remainder when $a$ is divided by $b$''), such that:
    
    \begin{enumerate}
        \item   $a = b \cdot q + r$, and
        \item   $0 \leq r < b$.
    \end{enumerate}
    
    Furthermore, $q$ and $r$ are the only two integers satisfying both these conditions.            
\end{quote}

~\\
You might not be used to thinking of division in this way, but how about this?

\begin{center}
    \begin{tikzpicture}
        \node at (0,0) {)};
        \draw (0,0.2) -- (1,0.2);
        
        \node at (-0.5, 0) { $b$ };
        \node at (0.5, 0) { $a$ };
        
        \node at (1.8, 0.5) { $q$ remainder $r$ };
    \end{tikzpicture}
    ~\\
    \tiny{ (Just don't make me do long division, I forgot that long ago.) }
\end{center}


\paragraph{Example:} Given the division problem $6 \div 2$, write it in the form of ~\\$a = b \cdot q + r$.

~\\
Here, we are dividing $a$ by $b$, so $a = 6$, $b = 2$.

$$ 6 = 2 \cdot q + r $$

~\\
To get 6, we can multiply $2 \cdot 3$, which gives us 6 evenly: the quotient is $q = 3$.

~\\
In this case, the remainder $r$ is 0 (because it divides evenly). If there \textit{were} a remainder,
it must be less than $b$ (or 2). So our final form here is:

$$ 6 = 2 \cdot 3 + 0 $$

~\\ If we wrote this like grade-school math, we would have:

\begin{center}
    \begin{tikzpicture}
        \node at (0,0) {)};
        \draw (0,0.2) -- (1,0.2);
        
        \node at (-0.5, 0) { 2 };
        \node at (0.5, 0) { 6 };
        
        \node at (1.8, 0.5) { 3 remainder 0 };
        
        \node at (0.5, -0.5) { 6 };
        \node at (0, -0.5) { - };
        \draw (0, -0.8) -- (1, -0.8);
        \node at (0.5, -1.0) { 0 };
    \end{tikzpicture}
\end{center}

\newpage
\paragraph{Example:} Given the division problem $7 \div 2$, write it in the form of  ~\\$a = b \cdot q + r$.

~\\
Solving it the grade-school way:

\begin{center}
    \begin{tikzpicture}
        \node at (0,0) {)};
        \draw (0,0.2) -- (1,0.2);
        
        \node at (-0.5, 0) { 2 };
        \node at (0.5, 0) { 7 };
        
        \node at (1, 0.5) { 3  r1 };
        
        \node at (0.5, -0.5) { 6 };
        \node at (0, -0.5) { - };
        \draw (0, -0.8) -- (1, -0.8);
        \node at (0.5, -1.0) { 1 };
    \end{tikzpicture}
\end{center}

~\\
$a = 7$, $b = 2$, and there are only two integers $q$ and $r$ that will
satisfy the division theorem rules.

~\\
To solve this, first we find the largest integer $q$ that, given $b \cdot q$, will get us
as close as possible to $a$ without going over...

$$ 2 \cdot 3 = 6 $$

~\\
Then, to get our remainder $r$, we have to figure out what needs to be added on
to get us to our final value of $a$ - in this case, $r = 1$, giving us the form:

$$ 7 = 2 \cdot 3 + 1 $$

\newpage
\subsubsection{Calculating Quotient and Remainder separately}

A common task you may end up doing as a programmer is needing to get
\textit{only the quotient}, or \textit{only the remainder},
or otherwise calculating each of these separate to store in two different variables. ~\\

In many programming languages, if you do division between two integers
the result will just be $q$; this is known as \textbf{integer division}:

\begin{lstlisting}[style=code]
int a = 7;
int b = 2;
int c = a / b;  // This will be 3, NOT 3.5
\end{lstlisting}

~\\

If we wanted a decimal value, we could divide them as floats or doubles:

\begin{lstlisting}[style=code]
float a = 7.0;
float b = 2.0;
float c = a / b;  // Returns 3.5
\end{lstlisting}

~\\

BUT, if we wanted $q$ and $r$, we would have to calculate it this way,
using the \textbf{modulus operator} \%:

\begin{lstlisting}[style=code]
int a = 7;
int b = 2;
int q = a / b;  // has 3
int r = a % b;  // has 1

cout << a << " = " << b << " * " << q << " + " << r;
// prints   7 = 2 * 3 + 1
\end{lstlisting}  


\newpage
\subsection{Primes and Composites}

A number is \textbf{prime} if it is only divisible by 1 and itself...

\begin{center}
    \begin{tabular}{c l c}
        Number & Factors & Is prime?
        \\ \hline
        2 & 1, 2            & yes
        \\
        3 & 1, 3            & yes
        \\
        4 & 1, 2, 4         & no
        \\
        5 & 1, 5            & yes
        \\
        6 & 1, 2, 3, 6      & no
        \\
        7 & 1, 7            & yes
    \end{tabular}
\end{center}

~\\
If a positive integer has factors other than 1 and itself, then it is \textbf{composite}.

~\\
A non-prime number can be boiled down to an equation using prime numbers smaller than itself.
This is known as the \textbf{prime factorization} of the number.

\begin{center}
    \begin{tabular}{c l c}
        Number & Prime factorization
        \\ \hline
        4 & $2 \cdot 2$
        \\
        6 & $2 \cdot 3$
        \\
        20 & $2 \cdot 2 \cdot 5$
        \\
        30 & $2 \cdot 3 \cdot 5$
    \end{tabular}
\end{center}















