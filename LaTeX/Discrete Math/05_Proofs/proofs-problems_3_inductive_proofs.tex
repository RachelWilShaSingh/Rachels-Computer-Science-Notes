\setcounter{question}{0}

    \section{Inductive Proofs}
    
        Make sure to use the \textbf{N6B} notes while going through this assignment for reference.
        
        \newpage
        \subsection{Equivalence of a Closed Formula and a Recursive Formula}
        
        \stepcounter{question} \question{\thequestion}{ Closed/Recursive Proof }{ 20\% }
        Prove that the two formulas result in the same sequence:
        
        \begin{center}
            \begin{tabular}{p{6cm} p{6cm}}
                Closed formula  & Recursive formula \\
                $a_n = 2n$      & $a_k = a_{k-1} + 2; \tab a_1 = 2$
            \end{tabular}
        \end{center}
        
        \begin{enumerate}
            \item   Basis step: Check that $a_1$ matches for both.
                    \solution{
                        ~\\ Closed formula:     $a_1 = 2(1) = 2$
                        ~\\ Recursive formula:  $a_1 = 2$ \checkmark
                    }{ \vspace{2cm} }
                    
            \item   Inductive step 1: Use the closed formula to find an equation for $a_{k-1}$
                    \solution{
                        $a_n = 2n$ ~\\
                        $a_{k-1} = 2(k-1) = 2k - 2$
                    }{ \vspace{4cm} }
                    
            \item   Inductive step 2: Apply the formula for $a_{k-1}$ into the recursive formula and simplify.
                    \solution{
                        $a_k = a_{k-1} + 2$ ~\\
                        $a_k = 2k - 2 + 2$ ~\\
                        $a_k = 2k$ \checkmark
                    }{ \vspace{6cm} }
        \end{enumerate}
        
        \newpage
        \stepcounter{question} \question{\thequestion}{ Closed/Recursive Proof }{ 20\% }
        Prove that the two formulas result in the same sequence:
        
        \begin{center}
            \begin{tabular}{p{6cm} p{6cm}}
                Closed formula  & Recursive formula \\
                $a_n = 3n + 1$      & $a_k = a_{k-1} + 3; \tab a_1 = 4$
            \end{tabular}
        \end{center}
        
        \solution{
        \begin{enumerate}
            \item   Basis step: Check that $a_1$ matches for both.

                        ~\\ Closed formula:     $a_1 = 3(1) + 1 = 4$
                        ~\\ Recursive formula:  $a_1 = 4$ \checkmark
                    
            \item   Inductive step 1: Use the closed formula to find an equation for $a_{k-1}$

                        $a_n = 3n+1$ ~\\
                        $a_{k-1} = 3(k-1) + 1 = 3k - 3 + 1 = 3k - 2$
                    
            \item   Inductive step 2: Apply the formula for $a_{k-1}$ into the recursive formula and simplify.

                        $a_k = a_{k-1} + 3$ ~\\
                        $a_k = 3k - 2 + 3$ ~\\
                        $a_k = 3k + 1$ \checkmark
        \end{enumerate}
        }{}
        \newpage
        \stepcounter{question} \question{\thequestion}{ Closed/Recursive Proof }{ 20\% }
        Prove that the two formulas result in the same sequence:
        
        \begin{center}
            \begin{tabular}{p{6cm} p{6cm}}
                Closed formula  & Recursive formula \\
                $a_n = 2^n$      & $a_k = 2 \cdot a_{k-1}; \tab a_1 = 2$
            \end{tabular}
        \end{center}
        
        \begin{hint}{Exponential properties \footnote{From https://www.mathsisfun.com/algebra/exponent-laws.html}} 
            \begin{tabular}{p{6cm} p{6cm}}
                $b^{m+n} = b^m \cdot b^n$
                &
                $b^{m-n} = \frac{b^m}{b^n}$
            \end{tabular}
        \end{hint}
        
        \solution{
        \begin{enumerate}
            \item   Basis step: Check that $a_1$ matches for both.

                        ~\\ Closed formula:     $a_1 = 2^1 = 2$
                        ~\\ Recursive formula:  $a_1 = 2$ \checkmark
                    
            \item   Inductive step 1: Use the closed formula to find an equation for $a_{k-1}$

                        $a_n = 2^n$ ~\\
                        $a_{k-1} = 2^{k-1}$
                    
            \item   Inductive step 2: Apply the formula for $a_{k-1}$ into the recursive formula and simplify.

                        $a_k = 2 \cdot a_{k-1}$ ~\\
                        $a_k = 2 \cdot 2^{k-1}$ ~\\
                        $a_k = 2^1 \cdot 2^{k} \cdot 2^{-1}$ ~\\
                        $a_k = 2^{k}$ \checkmark
        \end{enumerate}
        }{}
        
        \newpage
        \subsection{Representing the result of a Summation as a Closed Formula}
        
        \stepcounter{question} \question{\thequestion}{ Summation/Closed Proof }{ 14\% }
        Prove that the two formulas result in the same result for each $n \geq 1$:
        \footnote{From Discrete Mathematics, Ensley and Crawley, pg 122, question 8a}
        
        $$ \sum_{i=1}^{n} (2i - 1) = n^2 $$
        
        \begin{enumerate}
            \item   Basis step: Check that the results match for $n=1$, $n=2$, and $n=3$.
                    \solution{ ~\\
                    \begin{itemize}
                        \item   $n = 1$
                        \begin{itemize}
                            \item   Closed: $1^2 = 1$
                            \item   Summation: $ \sum_{i=1}^{1} (2i - 1) = (2 \cdot 1 - 1) = 1 $
                        \end{itemize}
                        
                        \item   $n = 2$
                        \begin{itemize}
                            \item   Closed: $2^2 = 4$
                            \item   Summation: $ \sum_{i=1}^{2} (2i - 1) = (2 \cdot 1 - 1) + (2 \cdot 2 - 1) = 1 + 3 = 4 $
                        \end{itemize}
                        
                        \item   $n = 3$
                        \begin{itemize}
                            \item   Closed: $3^2 = 9$
                            \item   Summation: $ \sum_{i=1}^{3} (2i - 1) = (2 \cdot 1 - 1) + (2 \cdot 2 - 1) + (2 \cdot 3 + 1) = 1 + 3 + 5 = 9 $
                        \end{itemize}
                        
                    \end{itemize}
                    }{ \vspace{2cm} }
                    
            \item   Inductive step 1: Expand the sum til $n$; it is equivalent to the sum til $n-1$, plus the final term.
                    \solution{ ~\\
                    $$ \sum_{i=1}^{n} (2i - 1) = \sum_{i=1}^{n-1} (2i - 1) + 2n - 1 $$
                    }{ \vspace{1cm} }
                    
            \item   Inductive step 2: Use the proposition to find a formula for the sum til $n-1$.
                    \solution{ ~\\
                    $$ \sum_{i=1}^{n} (2i - 1) = n^2 $$
                    $$ \sum_{i=1}^{n-1} (2i - 1) = (n-1)^2 $$
                    
                    }{ \vspace{3cm} }
            \item   Inductive step 3: Use the formula for the sum til $n-1$ in the expanded sum formula, and simplify.
                    \solution{ ~\\
                    $$ \sum_{i=1}^{n} (2i - 1) = \sum_{i=1}^{n-1} (2i - 1) + 2n - 1 $$
                    $$ \sum_{i=1}^{n} (2i - 1) = (n-1)^2 + 2n - 1 $$
                    $$ \sum_{i=1}^{n} (2i - 1) = n^2 - 2n + 1 + 2n - 1 $$
                    $$ \sum_{i=1}^{n} (2i - 1) = n^2 $$
                    
                    } {\vspace{3cm}}
        \end{enumerate}
        
        
        
        \newpage
        \stepcounter{question} \question{\thequestion}{ Summation/Closed Proof }{ 14\% }
        Prove that the two formulas result in the same result for each $n \geq 1$:
        \footnote{From Discrete Mathematics, Ensley and Crawley, pg 122, question 8b}
        
        $$ \sum_{i=1}^{n} (2i + 4) = n^2 + 5n $$
        
        \solution{
            1.  
                \begin{itemize}
                    \item   $n = 1$
                    \begin{itemize}
                        \item   Closed: $n^2 + 5n$ = $1^2 + 5(1) = 1 + 5 = 6$
                        \item   Summation: $ \sum_{i=1}^{n} (2i + 4) $ = $ 2 \cdot 1 + 4 = 6 $
                    \end{itemize}
                    
                    \item   $n = 2$
                    \begin{itemize}
                        \item   Closed: $n^2 + 5n$ = $2^2 + 5(2) = 4 + 10 = 14$
                        \item   Summation: $ \sum_{i=1}^{n} (2i + 4) $ = $ ( 2 \cdot 1 + 4 ) + ( 2 \cdot 2 + 4 ) = 6 + 8 = 14 $
                    \end{itemize}
                    
                    \item   $n = 3$
                    \begin{itemize}
                        \item   Closed: $n^2 + 5n$ = $3^2 + 5(3) = 9 + 15 = 24$
                        \item   Summation: $ \sum_{i=1}^{n} (2i + 4) $ = $ ( 2 \cdot 1 + 4 ) + ( 2 \cdot 2 + 4 ) + ( 2 \cdot 3 + 4 ) = 6 + 8 + 10 = 24$
                    \end{itemize}
                \end{itemize}
            
            2. $$ \sum_{i=1}^{n} (2i + 4) = \sum_{i=1}^{n-1} (2i + 4) + (2n + 4) $$
            
            3. $$ \sum_{i=1}^{n-1} (2i + 4) = (n-1)^2 + 5(n-1) $$
            
            4.  $$ \sum_{i=1}^{n} (2i + 4) = \sum_{i=1}^{n-1} (2i + 4) + (2n + 4) $$
                $$ \sum_{i=1}^{n} (2i + 4) = (n-1)^2 + 5(n-1) + (2n + 4) $$
                $$ \sum_{i=1}^{n} (2i + 4) = n^2 - 2n + 1 + 5n - 5 + 2n + 4 $$
                $$ \sum_{i=1}^{n} (2i + 4) = n^2 + 5n  $$
        }{}
        
        
        
        \newpage
        \stepcounter{question} \question{\thequestion}{ Summation/Closed Proof }{ 12\% }
        Prove that the two formulas result in the same result for each $n \geq 1$:
        \footnote{From Discrete Mathematics, Ensley and Crawley, pg 122, question 8c}
        
        $$ \sum_{i=1}^{n} (2^i - 1) = 2^{n+1} - n - 2 $$
        
        \solution{
            1.  
                \begin{itemize}
                    \item   $n = 1$
                    \begin{itemize}
                        \item   Closed: $2^{n+1} - n - 2$ = $ 2^{1+1} - 1 - 2 = 2^2 - 3 = 4 - 3 = 1$
                        \item   Summation: $ \sum_{i=1}^{n} (2^i - 1) $ = $2^1 - 1 = 1$
                    \end{itemize}
                    
                    \item   $n = 2$
                    \begin{itemize}
                        \item   Closed: $2^{n+1} - n - 2$ = $ 2^{2+1} - 2 - 2 = 2^3 - 4 = 8 - 4 = 4$
                        \item   Summation: $ \sum_{i=1}^{n} (2^i - 1) $ = $ (2^1 - 1) + (2^2 - 1) = 1 + 3 = 4$
                    \end{itemize}
                    
                    \item   $n = 3$
                    \begin{itemize}
                        \item   Closed: $2^{n+1} - n - 2$ = $ 2^{3+1} - 3 - 2 = 2^4 - 5 = 16 - 5 = 11$
                        \item   Summation: $ \sum_{i=1}^{n} (2^i - 1) $ = $ (2^1 - 1) + (2^2 - 1) + (2^3 - 1) = 1 + 3 + 7 = 11$
                    \end{itemize}
                \end{itemize}
            
            2.  $$ \sum_{i=1}^{n} (2^i - 1) = \sum_{i=1}^{n-1} (2^i - 1) + (2^n - 1) $$
            
            3.  $$ \sum_{i=1}^{n-1} (2^i - 1) =  2^{n-1+1} - (n-1) - 2 $$
                $$ \sum_{i=1}^{n-1} (2^i - 1) =  2^{n} - n + 1 - 2 $$ 
                $$ \sum_{i=1}^{n-1} (2^i - 1) =  2^{n} - n - 1 $$ 
            
            4.  $$ \sum_{i=1}^{n} (2^i - 1) = \sum_{i=1}^{n-1} (2^i - 1) + (2^n - 1) $$
                $$ \sum_{i=1}^{n} (2^i - 1) = 2^{n} - n - 1 + 2^n - 1 $$ 
                $$ \sum_{i=1}^{n} (2^i - 1) = 2^{n} + 2^n - n - 2 $$ 
                $$ \sum_{i=1}^{n} (2^i - 1) = 2 ( 2^{n} ) - n - 2 $$ 
                $$ \sum_{i=1}^{n} (2^i - 1) = ( 2^1 \cdot 2^{n} ) - n - 2 $$ 
                $$ \sum_{i=1}^{n} (2^i - 1) = 2^{n+1} - n - 2 $$ 
        }{}
        
        
        
        \begin{hint}{Exponential properties \footnote{From https://www.mathsisfun.com/algebra/exponent-laws.html}} 
            \begin{tabular}{p{6cm} p{6cm}}
                $b^{m+n} = b^m \cdot b^n$
                &
                $b^{m-n} = \frac{b^m}{b^n}$
            \end{tabular} ~\\~\\
            ( Note: $b^n + b^n = 2b^n$, just like $a + a = 2a$. )
        \end{hint}
