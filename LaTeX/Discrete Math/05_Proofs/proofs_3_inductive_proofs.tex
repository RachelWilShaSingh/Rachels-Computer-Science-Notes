
    The core idea behind inductive proofs is that we can illustrate that some statement
    is true for a \textbf{base case} (the first step, or first value),
    and then use an \textbf{induction step} to show that the statement holds
    for any value $n$.

    There are several types of inductive proofs we will be learning about,
    so I will be separating them up and illustrating how to do each.
    
    \subsection{Equivalence of a Closed Formula and a Recursive Formula}
    
        \paragraph{Example:} Show that the sequence given by the \textbf{recursive formula}:
        
        $$ a_k = a_{k-1} + 1; \tab a_1 = 1 $$
        
        is also given by the \textbf{closed formula}, $k \geq 2$:
        
        $$ a_n = n $$
        
        \begin{enumerate}
            \item   \textbf{Basis Step: Check $a_1$ for both formulas first}
            \begin{enumerate}
                \item   Recursive formula (given): $a_1 = 1$
                \item   Closed formula: $a_1 = 1$ ~\\ \checkmark OK
            \end{enumerate}
            
            \item   \textbf{Inductive Step: Show that this is true for all values up through $n-1$:}
            \begin{enumerate}
                \item   Use the closed formula, $a_n = n$, to find a value for $a_{k-1}$, which is used in the recursive formula.
                \begin{enumerate}
                    \item   $a_n = n$
                    \item   $a_{k-1} = k-1$
                \end{enumerate}
                
                \item   In the recursive formula, plug in our newly found formula for $a_{k-1}$ in, and simplify.
                \begin{enumerate}
                    \item   $a_k = a_{k-1} + 1$
                    \item   $a_k = k-1 + 1 $
                    \item   \framebox[1.1\width]{$a_k = k$}
                \end{enumerate}
                
                \item   $a_k = k$ is equivalent to $a_n = n$ (just changing the variable name).
                        By using the closed formula to find a value for $a_{k-1}$ and plugging
                        it back in, we can see that the two formulas are equivalent.
            \end{enumerate}
        \end{enumerate}
        
        \newpage
        \paragraph{Example:} Show that the sequence given by the \textbf{recursive formula}:
        
            $$ a_k = 2a_{k-1}; \tab a_1 = 1 $$
            
            is also given by the \textbf{closed formula}, 
            
            $$a_n = 2^{n-1}$$
            
        \begin{enumerate}
            \item   \textbf{Basis Step: Check $a_1$ for both formulas first}
            \begin{enumerate}
                \item   Recursive formula (given): $a_1 = 1$
                \item   Closed formula: $a_1 = 2^{1-1} = 2^0 = 1$ ~\\ \checkmark OK
            \end{enumerate}
            
            \item   \textbf{Inductive Step: Show that this is true for all values up through $n-1$:}
            \begin{enumerate}
                \item   Use the closed formula, $a_n = n$, to find a value for $a_{k-1}$, which is used in the recursive formula.
                \begin{enumerate}
                    \item   $a_n = 2^{n-1}$
                    \item   $a_{k-1} = 2^(k-1-1)$ ~\\
                            $= 2^{k-2} $
                \end{enumerate}
                
                \item   Plug back into the recursive formula:
                \begin{enumerate}
                    \item   $a_k = 2a_{k-1}$
                    \item   $a_k = 2( 2^{k-2} ) $  ~\\
                            $= 2^1 \cdot 2^k \cdot 2^{-2} $ ~\\
                            $= 2^k \cdot 2^{-1} $ (Combining 1 and -2) ~\\
                            $= 2^{k-1}$
                    \item   \framebox[1.1\width]{$a_k = 2^{k-1}$} \checkmark
                \end{enumerate}
            \end{enumerate}
        \end{enumerate}
        
        \begin{hint}{Exponential properties \footnote{From https://www.mathsisfun.com/algebra/exponent-laws.html}} 
            ~\\
            \begin{tabular}{p{6cm} p{6cm}}
                $b^{m+n} = b^m \cdot b^n$
                &
                $b^{m-n} = \frac{b^m}{b^n}$
                \\
                $b^{-n} = \frac{1}{b^n}$
                &
                $(b^m)^n = b^{m \cdot n}$
            \end{tabular}
        \end{hint}
        
    \newpage
    \subsection{Representing the result of a Summation as a Closed Formula}
    
        \paragraph{Example:}  \footnote{From Discrete Mathematics, Ensley and Crawley, pg 122 question 8a}
        Show that the result of the \textbf{summation} given by:
        
        $$ \sum_{i=1}^n (2i - 1) $$
    
        is the same as the $n$th item in the sequence given by the \textbf{closed formula}:
        
        $$ a_n = n^2 $$
        
        For each $n \geq 1$.
        
        ~\\
        
        In other words, the proposition is...
        
        $$ \sum_{i=1}^n (2i - 1) = n^2 $$        
        
        \begin{enumerate}
            \item   \textbf{Basis step: Show that this is true for $n=1$, $n=2$, and $n=3$.}
            \begin{enumerate}
                \item   For $i = 1$... ~\\
                        Summation: \tab $\sum_{i=1}^1 (2i - 1) = (2 \cdot 1 - 1) = 1$ ~\\
                        Closed formula: \tab[0.3cm] $a_1 = 1^2 = 1$ \checkmark
                \item   For $i = 2$... ~\\
                        Summation: \tab $\sum_{i=1}^2 (2i - 1) = (2 \cdot 1 - 1) + (2 \cdot 2 - 1) = 1 + 3 = 4$ ~\\
                        Closed formula: \tab[0.3cm] $a_1 = 2^2 = 4$ \checkmark
                \item   For $i = 2$... ~\\
                        Summation: \tab $\sum_{i=1}^3 (2i - 1) = (2 \cdot 1 - 1) + (2 \cdot 2 - 1) + (2 \cdot 3 - 1) $~\\
                                   \tab[4cm] $= 1 + 3 + 5 = 9$ ~\\
                        Closed formula: \tab[0.3cm] $a_1 = 3^2 = 9$ \checkmark
            \end{enumerate}
            
            \newpage
            \item   \textbf{Inductive step:}
            \begin{enumerate}
                \item   Recall that $\sum_{i=1}^{n}$ is equivalent to  $\sum_{i=1}^{n-1}$ plus the \textbf{final term} at $i=n$ ($2n-1$).
                        First, we need to rewrite our sum into this form: ~\\
                        $\sum_{i=1}^{n} (2i-1) = \sum_{i=1}^{n-1} (2i-1) + (2n-1)$
                
                \item   Next, we need to find an equation for $\sum_{i=1}^{n-1} (2i-1)$ using the \textbf{closed formula:}
                \begin{enumerate}
                    \item   $ \sum_{i=1}^n (2i - 1) = n^2 $
                    \item   $ \sum_{i=1}^{n-1} (2i - 1) = (n-1)^2 $
                    \item   $ \sum_{i=1}^{n-1} (2i - 1) = (n-1)(n-1) $
                    \item   $ \sum_{i=1}^{n-1} (2i - 1) = n^2 - 2n + 1 $
                \end{enumerate}
                
                \item   Finally, we plug in our formula for $\sum_{i=1}^{n-1} (2i - 1)$ into the extended equation and simplify:
                \begin{enumerate}
                    \item   $ \sum_{i=1}^{n} (2i-1) = \sum_{i=1}^{n-1} (2i-1) + (2n-1) $
                    \item   $ \sum_{i=1}^{n} (2i-1) = n^2 - 2n + 1 + (2n-1) $
                    \item   $ \sum_{i=1}^{n} (2i-1) = n^2 - 2n + 2n + 1 - 1 $
                    \item   \framebox[1.1\width]{$ \sum_{i=1}^{n} (2i-1) = n^2 $}
                \end{enumerate}
            \end{enumerate}
            
            \item   Since we end up with the original proposition, we have proved that they are equivalent.
        \end{enumerate}
        
        \newpage
        \paragraph{Example:} \footnote{From Discrete Mathematics, Ensley and Crawley, pg 122 question 8b}
            Use induction to prove the following:
            
            $$ \sum_{i=1}^n (2i + 4) = n^2 + 5n $$
            
            For each $n \geq 1$.
        
        \begin{enumerate}
            \item   \textbf{Basis step: Show that this is true for $n=1$, $n=2$, and $n=3$.} ~\\
                    \begin{tabular}{l | l | l}
                        $n$ value   & Summation                                     & Closed formula 
                        \\ \hline
                        $n = 1$     & $\sum_{i=1}^1 (2i + 4) = 2 \cdot 1 + 4 = 6$   & $1^2 + 5(1) = 1+5 = 6$ \checkmark
                        \\ \hline
                        $n = 2$     & $\sum_{i=1}^2 (2i + 4)$                       & $2^2 + 5(2) $
                        \\          & $ = (2 \cdot 1 + 4) + (2 \cdot 2 + 4)$        & $= 4 + 10$
                        \\          & $ = 6 + 8 = 14 $                              & $= 14$ \checkmark
                        \\ \hline
                        $n = 3$     & $\sum_{i=1}^2 (2i + 4)$                       & $3^2 + 5(3) $
                        \\          & $= 6 + 8 + 10 $                               & $=9 + 15$
                        \\          & $= 24$                                        & $= 24$ \checkmark
                    \end{tabular}
            
            \item   \textbf{Inductive step:}
            \begin{enumerate}
                \item   Rewrite \textbf{summation} into an expanded formula: ~\\
                        $ \sum_{i=1}^n (2i + 4) = \sum_{i=1}^{n-1} (2i + 4) + (2n+4)$ ~\\
                        \footnotesize (The summation's formula is $2i+4$, so the final term at $i=n$ is $2n+4$)
                        \normalsize
                
                \item   Find a formula for $\sum_{i=1}^{n-1} (2i + 4)$ using the original proposition: ~\\
                        $ \sum_{i=1}^n (2i + 4) = n^2 + 5n $ ~\\
                        $ \sum_{i=1}^{n-1} (2i + 4) = n(n-1)^2 + 5(n-1) $ ~\\
                        $ \tab[2.5cm] = (n-1)(n-1) + 5(n-1) $ ~\\
                        $ \tab[2.5cm] = (n^2 - 2n + 1) + 5n - 5 $ ~\\
                        $ \tab[2.5cm] = n^2 - 2n + 5n + 1 - 5$ ~\\
                        $ \tab[2.5cm] = n^2 + 3n - 4$ ~\\
                        
                \item   Plug formula for $\sum_{i=1}^{n-1} (2i + 4)$ into the expanded formula: ~\\
                        \begin{tabular}{l l l}
                            $ \sum_{i=1}^n (2i + 4) = $ & $\sum_{i=1}^{n-1} (2i + 4)$ & $+ (2n+4)$ \\
                            $ \sum_{i=1}^n (2i + 4) = $ & $n^2 + 3n - 4$ & $+ (2n+4)$  \\
                            $ \sum_{i=1}^n (2i + 4) = $ & \multicolumn{2}{l}{$n^2 + 3n + 2n - 4 + 4$}  \\
                        \end{tabular} ~\\
                        \framebox[1.1\width]{$ \sum_{i=1}^n (2i + 4) = n^2 + 5n $}
            \end{enumerate}
        \end{enumerate}

    \newpage
    \subsection{Representing the result of a Summation as a Recursive Formula}

        We can also use inductive thinking to convert a summation into a recursive formula
        that gives the same resulting number for each value of $n$.
        
        \paragraph{Example:} \footnote{From Discrete Mathematics, Ensley and Crawley, pg 123}
        
        Rewrite the summation $$ \sum_{i=1}^{n} (2i-1) $$ as a recursive formula...
        
        \begin{enumerate}
            \item   Find the first value, for $n=1$: ~\\
                    $ \sum_{i=1}^{1} (2i-1) = 2(1) - 1 = 1$ ~\\
                    
            \item   Expand the summation to use the sum to $n-1$ plus the final term: ~\\
                    $\sum_{i=1}^{n} (2i-1) = \sum_{i=1}^{n-1} (2i-1) + 2n - 1$
                    
            \item   Use $s_n$ to denote the sum to $n$, and $s_{n-1}$ to denote the sum to $n-1$. ~\\
                    $s_n = s_{n-1} + 2n - 1$
                    
            \item   The first value, $s_1$ will be from step (1): $s_1 = 1$.
        \end{enumerate}
        
        We can illustrate that they give the same results by plugging in different values for $n$ for each one...
        
        \begin{center}
            \begin{tabular}{l | l | l}
                $n = ...$   & Summation                                                     & Recursive \\ \hline
                $n = 2$     & $\sum_{i=1}^{2} (2i-1) = (2 \cdot 1 - 1) + (2 \cdot 2 - 1)$   & $s_2 = s_1 + 2n - 1$
                \\          & $= 1 + 3$                                                     & $= 1 + 2(2) - 1$
                \\          & $= 4$                                                         & $= 4$
                \\ \hline
                $n = 3$     & $\sum_{i=1}^{2} (2i-1)$                                       & $s_3 = s_2 + 2n - 1$
                \\          & $= 1 + 3 + 5$                                                 & $= 4 + 2(3) - 1$
                \\          & $= 9$                                                         & $= 9$
                \\ \hline
            \end{tabular}
        \end{center}
        
        ~\\ And so on... 








