def Implication( p, q ):
    # p to q
    return not NegImplication( p, q )

def NegImplication( p, q ):
    # p and neg q
    return p and not q

print( "p\tq\tp -> q" )
print( "------------------------" )
for p in [ True, False ]:
    for q in [ True, False ]:
        print( str( p ) + "\t" +  str( q ) + "\t" + str( Implication( p, q ) ) )

print( "" )
print( "p\tq\t!(p -> q) Negation" )
print( "------------------------" )
for p in [ True, False ]:
    for q in [ True, False ]:
        print( str( p ) + "\t" +  str( q ) + "\t" + str( NegImplication( p, q ) ) )


print( "" )
print( "p\tq\tq -> p Converse" )
print( "------------------------" )
for p in [ True, False ]:
    for q in [ True, False ]:
        print( str( p ) + "\t" +  str( q ) + "\t" + str( Implication( q, p ) ) )
        
print( "" )
print( "p\tq\t!p -> !q Inverse" )
print( "------------------------" )
for p in [ True, False ]:
    for q in [ True, False ]:
        print( str( p ) + "\t" +  str( q ) + "\t" + str( Implication( not p, not q ) ) )
        
print( "" )
print( "p\tq\t!q -> !p Contrapositive" )
print( "------------------------" )
for p in [ True, False ]:
    for q in [ True, False ]:
        print( str( p ) + "\t" +  str( q ) + "\t" + str( Implication( not q, not p ) ) )
#