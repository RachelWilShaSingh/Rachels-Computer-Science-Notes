\begin{intro}{Proposition}
  A proposition is a statement that is $true$ or $false$.
\end{intro}

While ``2 is an even number'' is a proposition, 
``$x$ is an even number'' is \textbf{not} a proposition, 
since on its own it is not unambiguously $true$ or $false$.
However, being able to ``plug in'' values to a structure
like this can be useful. That's where \textbf{Predicates} come in.

\begin{intro}{Predicate}
  Given a predicate $P(x)$, the predicate takes in
  some input $x$ and results in either a $true$ or $false$ proposition.
\end{intro}

Think of Predicates like \textbf{functions}, both in algebra
and in programming. It takes some input (or inputs), and returns
a proposition, which then evaluates to a boolean value.

In Discrete Math, it could look like this:

\begin{center}
  $P(x)$ is the predicate ``$x$ is a positive number''.
  
  $P(5)$ is ``5 is a positive number'', which is \textbf{true}.
  
  $P(-1)$ is ``-1 is a positive number'', which is \textbf{false}.
\end{center}

If we were thinking about this in terms of code, it might look like this:

\begin{lstlisting}[style=code]
bool IsPositive( int x )
{
  return ( x > 0 );
}
\end{lstlisting}

While propositions are usually written with lower-case letters, such as $p$ and $q$,
predicates are written with upper-case letters, like $P$ or $Q$, with some kind of input like $x$.

We can use our logic operators $\land$, $\lor$, and $\neg$ to build compound statements
out of these propositions as well.

~\\ Given:

\begin{itemize}  
  \item   $E(x)$ is the predicate ``$x$ is even''
  \item   $O(x)$ is the predicate ``$x$ ends in 0''
\end{itemize}

~\\ We can build some predicates, like:

\begin{itemize}  
  \item   $E(x) \land O(x)$ is $x$ is even AND $x$ ends in 0.
  \item   $E(x) \lor O(x)$ is $x$ is even OR $x$ ends in 0.
  \item   $\neg E(x)$ is $x$ is not even.
  \item   $\neg O(x)$ is $x$ does not end in 0.
\end{itemize}

~\\ So using these predicates would look like:

\begin{itemize}
  \item   $E(2) \land O(2)$: ``2 is even and 2 ends in 0'' - false.
  \item   $E(2) \lor O(2)$: ``2 is even or 2 ends in 0'' - true.
\end{itemize}


\newpage
Something that I use often in programming is checking if an inputted value is out of range:

\begin{lstlisting}[style=code]
bool InvalidChoice( int x )
{
  // 0 is the min value, 10 is the max value.
  return ( x < 0 || x > 10 );
}
\end{lstlisting}

In Discrete Math, it could look like this:

~\\ Given:

\begin{itemize}
    \item   $i$ is the \textbf{proposition} ``input is invalid'',
    \item   $v$ is the \textbf{proposition} ``input is valid'',
    \item   $L(x)$ is the \textbf{predicate} ``$x < 0$'',
    \item   $M(x)$ is the \textbf{predicate} ``$x > 10$''.
\end{itemize}

~\\ We can build the \textbf{implications}:

\begin{itemize}
    \item   \hypothesis{$( L(x) \lor M(x) )$} $\to$ \conclusion{$i$} ~\\
            If \hypothesis{$x < 10$ OR $x > 10$} then \conclusion{the input is invalid}.
    \item   \hypothesis{$( \neg L(x) \land \neg M(x) )$} $\to$ \conclusion{$v$} ~\\
            If \hypothesis{$x \geq 10$ AND $x \leq 10$} then \conclusion{the input is valid}.
\end{itemize}

\newpage

\subsection{Domains}

\begin{intro}{Domain}
In mathematics, the domain of a function is the set of inputs accepted by the function.
\footnote{From https://en.wikipedia.org/wiki/Domain\_of\_a\_function}
\end{intro}

When we're specifying predicates with inputs, we may want to specify a
\textbf{domain} of values the input could be.

We can specify a domain set explicitly as a \textit{discrete set of values}, such as:

$$D = \{ 1, 2, 3, 4 \}$$

or we can rely on sets of classes of numbers, such as:

\begin{center}
    \begin{tabular}{ l p{12cm} }
        $\mathbb{Z}$    & The set of all \textbf{integers} \\
                        & Integers are whole numbers, including positive, negative, and zero. \\
                        & Example: $\{ ..., -2, -1, 0, 1, 2, ... \}$ \\ \\
        $\mathbb{N}$    & The set of all \textbf{natural numbers} \\
                        & Natural numbers are ``counting numbers'', whole numbers 0 and up. \\ 
                        & Example: $\{0, 1, 2, 3, ...\}$ \\ \\
        $\mathbb{Q}$    & The set of all \textbf{rational numbers} \\ 
                        & Rational numbers are numbers that can be represented as a ratio. \\
                        & Example values: $\frac{1}{2}, \frac{5}{1}, \frac{5}{3}$ \\ \\
        $\mathbb{R}$    & The set of all \textbf{real numbers} \\
                        & Real numbers, which include the sets above, plus numbers with unending strings of digits after a decimal point. \\
                        & Example values: $1, \frac{1}{2}, \pi, \sqrt{3}$
    \end{tabular}
\end{center}

\begin{intro}{$\in$ In the set, $\not\in$ not in the set}
  The symbol $\in$ means ``exists in the set'', and the symbol $\not\in$ means ``does not exist in the set''.
\end{intro}

So given $D = \{1, 2, 3\}$, we could say that $1 \in D$ (1 is in the set $D$), 
and we can say $6 \not\in D$ (6 is not in the set $D$).

\newpage
\subsection{Universal Quantifier $\forall$}

Sometimes, no matter what you plug into a
predicate, it will always come out to the
same result (either always \textbf{true} or always \textbf{false}).

\begin{intro}{Universal quantifier $\forall$}
  Using universal quantifier $\forall$ with $x$ specifies that,
  \textit{for all inputs $x$}, the result of the predicate will
  always evaluate to the same result.
  
  $\forall x \in D, P(x)$ is read as ``For all inputs $x$ in the set $D$,
  $P(x)$ is true.''
\end{intro}

\paragraph{Example 1:}

~\\ Given:

\begin{itemize}
  \item   The set $C = \{$ red, yellow, blue $\}$,
  \item   The predicate $P(x)$ is ``$x$ is a primary color''.
\end{itemize}

~\\ Then we can write:

\begin{itemize}
  \item   $\forall x \in C, P(x)$ ~\\
          ``For all inputs $x$ in the set $C$, $x$ is a primary color'', ~\\
          or ``All colors in $C$ are primary colors''.
\end{itemize}

\begin{center}
\includegraphics[height=0.5cm]{01_Propositional_Logic/images/colors.png} 
\end{center}

\paragraph{Example 2:}

~\\ Given:

\begin{itemize}
  \item   The set $ D = \{2, 4, 6, 8\}$,
  \item   The predicate $E(x)$ is ``$x$ is even''.
\end{itemize}

~\\ Then we can write:

\begin{itemize}
  \item   $\forall x \in D, E(x)$ ~\\
          ``For all inputs $x$ in the set $D$, $x$ is even'', ~\\
          or ``All values in $D$ are even''.
\end{itemize}



\newpage
\subsection{Existential Quantifier $\exists$}

On the other hand, some inputs in the domain may cause the predicate to result in
\textbf{true}, and others may cause it to result in \textbf{false}.

\begin{intro}{Existential Quantifier $\exists$}
  Using the existential quantifier $\exists$ with $x$ specifies that,
  \textit{there exists some inputs $x$} that will
  have the predicate $P(x)$ result in true (or $\neg P(x)$ for resulting in false).
  
  $\exists x \in D, P(x)$ is read as ``There exists some input $x$ in the set $D$,
  such that $P(x)$ is true.''
\end{intro}

\paragraph{Example 1:}

~\\ Given:

\begin{itemize}
  \item   The set $C = \{$ red, orange, yellow, green, blue, purple $\}$,
  \item   The predicate $P(x)$ is ``$x$ is a primary color''.
\end{itemize}

~\\ Then we can write:

\begin{itemize}
  \item   $\exists x \in C, P(x)$ ~\\
          ``There exists at least one input $x$ in the set $C$, such that $x$ is a primary color'', ~\\
          or ``Some colors in $C$ are primary colors''.
  \item   $\exists x \in C, \neg P(x)$ ~\\
          ``There exists at least one input $x$ in the set $C$, such that $x$ is NOT a primary color'', ~\\
          or ``Some colors in $C$ are not primary colors''.
\end{itemize}

\begin{center}
\includegraphics[height=0.5cm]{01_Propositional_Logic/images/colors2.png} 
\end{center}

\newpage
\paragraph{Example 2:}

~\\ Given:

\begin{itemize}
  \item   The set $ D = \{1, 2, 3, 4, 5, 6, 7, 8\}$,
  \item   The predicate $E(x)$ is ``$x$ is even''.
\end{itemize}

~\\ Then we can write:

\begin{itemize}
  \item   $\exists x \in D, E(x)$ ~\\
          ``There exists at least one input $x$ in the set $D$, such that $x$ is even'', ~\\
          or ``Some values in $D$ are even''.
          
  \item   $\exists x \in D, \neg E(x)$ ~\\
          ``There exists at least one input $x$ in the set $D$, such that $x$ is NOT even'', ~\\
          or ``Some values in $D$ are NOT even''.
\end{itemize}


\subsection{Quantified statement}

\begin{intro}{Quantified statement}
  A statement written with a \textbf{quantifier} ($\forall$ or $\exists$), a domain specified,
  and a predicate, is known as a \textbf{Quantified Statement}.
  
  $$ \forall x \in D, P(x) $$
  
  $$ \exists x \in D, P(x) $$
\end{intro}


\newpage
\subsection{Counterexamples}

If we have a quantified statement of the form
$$ \forall x \in D, P(x) $$
if we can find \textit{at least one input $x$} that makes
$P(x)$ false, then we can disprove this entire quantified statement.
This value of $x$ is the \textbf{counterexample}.

\paragraph{Example:}

~\\ Given:

\begin{itemize}
  \item   The set $D = \{ 1, 3, 5, 7, 9, 10 \}$
  \item   The predicate $P(x)$ is ``$x$ is odd''
  \item   The quantified statement $\forall x \in D, P(x)$
\end{itemize}

Our quantified statement says ``All inputs $x$ from the set $D$ are odd numbers''.
However, looking at the set $D$ we can see that $10 \in D$ (10 is in the set $D$),
which is an even number. Thus, the quantified statement $\forall x \in D, P(x)$ is invalid, or \textbf{false}.

\subsection{Negations of predicates}

If we come across a quantified statement that is false, we can say that the \textbf{negation}
of that statement is \textbf{true}.

\paragraph{Example:}

~\\ Given:

\begin{itemize}
  \item   The set $D = \{ 1, 3, 5, 7, 9, 10 \}$
  \item   The predicate $P(x)$ is ``$x$ is odd''
  \item   The quantified statement $\forall x \in D, P(x)$
\end{itemize}

~\\ $\forall x \in D, P(x)$ is false, so $\neg ( \forall x \in D, P(x) ) $ will be true:
~\\ $\exists x \in D, \neg P(x)$ -- ``There exists some input $x$ in the set $D$ such that $x$ is NOT odd''.


\newpage
\begin{intro}{Negating a quantified statement}
  When negating a quantified statement, the \textbf{Universal Quantifier $\forall$} changes
  to a \textbf{Existential Quantifier $\exists$} and vice versa. Additionally,
  the predicate is also negated.
  
  \begin{center} \large
    \begin{tabular}{| c | c |} \hline
      \textbf{Original} & \textbf{Negation} \\ \hline
      $\forall x \in D, P(x)$ & $\exists x \in D, \neg P(x)$ \\ \hline
      $\exists x \in D, P(x)$ & $\forall x \in D, \neg P(x)$ \\ \hline
    \end{tabular}
  \end{center}
\end{intro}


\paragraph{Example 1:}

~\\ Given:

\begin{itemize}
  \item   The set $D = \{ 2, 4, 6, 8 \}$
  \item   The predicate $O(x)$ is ``$x$ is odd''
  \item   The quantified statement $\exists x \in D, O(x)$
\end{itemize}

~\\ The quantified statement is ``There exists some input $x$ from the set $D$ such that $x$ is an odd number'',
but when we look at the set $D$, none of those numbers are odd. Therefore, $\exists x \in D, O(x)$ is false, or:

\begin{enumerate}
  \item   $\exists x \in D, O(x)$ is false
  \item   $\equiv \neg ( \exists x \in D, O(x) )$
  \item   $\equiv \forall x \in D, \neg O(x)$ ~\\
          ``For all inputs $x$ from $D$, $x$ is NOT odd.''
\end{enumerate}

\newpage
\paragraph{Example 2:}

~\\ Given:

\begin{itemize}
  \item   The set $D = \{$ apple, banana, carrot $\}$
  \item   The predicate $F(x)$ is ``$x$ is a fruit''
  \item   The quantified statement $\forall x \in D, F(x)$
\end{itemize}

~\\ The quantified statement is 
``For all inputs $x$ from the set $D$, $x$ is a fruit'',
but when we look at the set $D$, the \textbf{carrot} is not a fruit.
Therefore, $\forall x \in D, F(x)$ is false, or:

\begin{enumerate}
  \item   $\forall x \in D, F(x)$ is false
  \item   $\equiv \neg ( \forall x \in D, F(x) )$
  \item   $\equiv \exists x \in D, \neg F(x)$ ~\\
          ``There exists some input $x$ from $D$ such that $x$ is NOT a fruit.''
\end{enumerate}

\newpage
\subsection{Quantified statements with multiple variables}

We can also write predicates with multiple input variables, such as:

\begin{center}
  $P(x,y)$ is the predicate, ``$y + 1 = x$''.
\end{center}

But with this additional varaible, we need to make sure to add another
quantifier for it, and also specify its domain (even if the domain is
the same for both inputs):

$$ \forall x \in \mathbb{Z}, \exists y \in \mathbb{Z}, P(x,y) $$

This quantified statement is read as ``For all integers\footnote{Remember that $\mathbb{Z}$ is the set of all integers.} 
$x$,
there exists some integer $y$, such that $y + 1 = x$.''
Basically, if we take any integer $x$, we can find some other variable
$y$ where we can get $x$ from taking $y + 1$, like this:

\begin{center} \large
  \begin{tabular}{l l l}
    \textbf{Inputs} & \textbf{Predicate}  & \textbf{Result} \\ \hline
    $P(2, 1)$       & $1 + 1 = 2$         & true            \\
    $P(5, 4)$       & $4 + 1 = 5$         & true            \\
    $P(5, 6)$       & $6 + 1 = 5$         & false           \\
    $P(-1, 0)$      & $0 + 1 = -1$        & false           \\
  \end{tabular}
\end{center}


\newpage
\subsubsection{Negating quantified statements with multiple variables}

When we have multiple variables and quantifiers in a quantified statement,
we again negate each \textbf{quantifier} (flip from $\forall$ to $\exists$ or vice versa),
and negate the predicate, so:

\begin{center} \large
  \begin{tabular}{c c c | l}
    \multicolumn{3}{c |}{ $\neg( \forall x \in \mathbb{Z}, \exists y \in \mathbb{Z}, P(x, y))$ }         & Statement to negate \\
    $\neg ( \forall x \in \mathbb{Z} ) $  & $\neg ( \exists y \in \mathbb{Z} )$   & $\neg ( P(x,y) )$   & Each section gets negated \\
    $\exists x \in \mathbb{Z}$            & $\forall y \in \mathbb{Z}$            & $\neg P(x,y)$       & Simplified \\
    \multicolumn{3}{c |}{ $\exists x \in \mathbb{Z}, \forall y \in \mathbb{Z}, \neg P(x, y)$ }         & Result \\
  \end{tabular}
\end{center}

\paragraph{Example:}

~\\ Given

\begin{itemize}
  \item   The predicate $P(a, b)$ is ``$a + b \in \mathbb{Z}$'' 
  \item   The quantified statement $\forall a \in \mathbb{Z}, \forall b \in \mathbb{Z}, P(a, b)$ ~\\
          (The sum of the integers $a$ and $b$ is also an integer)
\end{itemize}

~\\ What is the negation? Is the original or the negation true?

\begin{enumerate}
  \item   $\neg ( \forall a \in \mathbb{Z}, \forall b \in \mathbb{Z}, P(a, b) )$
  \item   $\equiv \exists a \in \mathbb{Z}, \exists b \in \mathbb{Z}, \neg P(a, b)$ ~\\
          (There exists some integers $a$ and $b$ such that $a + b$ is NOT an integer.)
\end{enumerate}



%If this is the case, we can use a symbol that means
%``for all $x$'', to specify that for every input value possible,
%the predicate will evaluate to the same result.

%On the other hand, if a predicate may be \textbf{true} for some values
%of $x$ and \textbf{false} for other values of $x$, we can use a symbol
%to say ``there exists some input value $x$'' such that
%the predicate will give some result.












